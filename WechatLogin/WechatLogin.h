//
//  WechatLogin.h
//  OpenSDK
//
//  Created by 涂俊 on 2020/2/11.
//  Copyright © 2020 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_SdkProtrol.h"
#import "OpenSDK_LoginProtocol.h"
#import "OpenSDK_BaseClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface WechatLogin : OpenSDK_BaseClass <OpenSDK_SdkProtrol, OpenSDK_LoginProtocol>

@end

NS_ASSUME_NONNULL_END
