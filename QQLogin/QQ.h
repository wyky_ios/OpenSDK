//
//  QQLogin.h
//  Pods-Unity-iPhone
//
//  Created by ycgame on 2020/9/29.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_SdkProtrol.h"
#import "OpenSDK_LoginProtocol.h"
#import "OpenSDK_BaseClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface QQ : OpenSDK_BaseClass <OpenSDK_SdkProtrol, OpenSDK_LoginProtocol>

@end

NS_ASSUME_NONNULL_END
