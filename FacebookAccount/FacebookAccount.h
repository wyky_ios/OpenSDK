//
//  FacebookAccount.h
//  FacebookAccount
//
//  Created by wyht－ios－dev on 16/11/16.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AccountKit/AccountKit.h>
#import <Foundation/Foundation.h>
#import "OpenSDK_BaseProtrol.h"
#import "OpenSDK_BaseClass.h"

//! Project version number for FacebookAccount.
FOUNDATION_EXPORT double FacebookAccountVersionNumber;

//! Project version string for FacebookAccount.
FOUNDATION_EXPORT const unsigned char FacebookAccountVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FacebookAccount/PublicHeader.h>

@interface FacebookAccount : OpenSDK_BaseClass <AKFViewControllerDelegate,OpenSDK_BaseProtrol>

@end
