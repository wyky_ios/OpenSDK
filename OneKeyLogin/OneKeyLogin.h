//
//  OneKeyLogin.h
//  Pods-Unity-iPhone
//
//  Created by MacPro on 2020/12/17.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_SdkProtrol.h"
#import "OpenSDK_LoginProtocol.h"
#import "OpenSDK_BaseClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface OneKeyLogin : OpenSDK_BaseClass <OpenSDK_SdkProtrol, OpenSDK_LoginProtocol>

@end

NS_ASSUME_NONNULL_END
