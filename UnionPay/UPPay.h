//
//  UPPay.h
//  
//
//  Created by 涂俊 on 2018/3/21.
//  Copyright © 2018年 wyht. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_SdkProtrol.h"
#import "OpenSDK_PayProtocol.h"
#import "OpenSDK_BaseClass.h"

@interface UPPay : OpenSDK_BaseClass <OpenSDK_SdkProtrol, OpenSDK_PayProtocol>

@end
