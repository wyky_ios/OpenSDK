//
//  WcXyz.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/10/25.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_SdkProtrol.h"
#import "OpenSDK_PayProtocol.h"
#import "OpenSDK_BaseClass.h"

@interface WcXyz : OpenSDK_BaseClass <OpenSDK_SdkProtrol, OpenSDK_PayProtocol>

@end
