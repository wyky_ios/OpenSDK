//
//  OPKTracking.m
//  YCOpenSDK
//
//  Created by ycgame on 2020/12/8.
//

#import "OPKTracking.h"
#import "JANALYTICSService.h"
#import "OPKCoreConfiguration.h"
#import "OpenSDK_DataManager.h"
#import "OPKDevice.h"
#import "OpenSDK_DataKeys.h"
#import <Y2Common/Y2Common.h>

static OPKTracking *instance = nil;

@interface OPKTracking()
@property(nonatomic, strong)NSString *lastPage;
@property(nonatomic, strong)NSString *currentPage;
@property(nonatomic, assign)BOOL isEnabled;
@end
@implementation OPKTracking

+ (instancetype)shared {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)config {
    NSString *appKey = [[[[NSBundle mainBundle] infoDictionary] objectForKey:OpenSDK_CONFIG] objectForKey:OpenSDK_JAPP_KEY];
    _isEnabled = appKey ? YES : NO;
    JANALYTICSLaunchConfig * config = [[JANALYTICSLaunchConfig alloc] init];
    config.appKey = appKey;
    config.channel = [OpenSDK_DataManager getInstance].channelID;
    [JANALYTICSService setupWithConfig:config];
//    [JANALYTICSService setDebug:YES];
}

- (void)pageView:(NSString *)pageName {
    if (!_isEnabled) { return; }
    if (_currentPage) {
        _lastPage = [_currentPage copy];
    }
    _currentPage = [pageName copy];
    
    JANALYTICSBrowseEvent *obj = [[JANALYTICSBrowseEvent alloc] init];
    obj.name = pageName;
    obj.contentID = pageName;
    obj.type = OPKEventPageView;
    obj.extra = [self defaultExtra];
    [JANALYTICSService eventRecord:obj];
}

- (void)event:(NSString *)eventName {
    if (!_isEnabled) { return; }
    [self event:eventName parameters:@{}];
}

- (void)event:(NSString *)eventName parameters:(NSDictionary *)param {
    if (!_isEnabled) { return; }
    NSMutableDictionary *mutableDict = [self defaultExtra].mutableCopy;
    for (NSString *key in param) {
        [mutableDict setObject:param[key] forKey:key];
    }
    JANALYTICSCountEvent *obj = [[JANALYTICSCountEvent alloc] init];
    obj.eventID = eventName;
    obj.extra = mutableDict.copy;
    [JANALYTICSService eventRecord:obj];
}

- (NSString *)currentTime {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    NSString *time = [format stringFromDate:[NSDate date]];
    return time;
}

- (NSDictionary *)defaultExtra {
    NSString *deviceId = [Y2DeviceInfo getDeviceID] ?: @"";
    NSString *deviceName = [OPKDevice deviceModel] ?: @"";
    NSString *time = [self currentTime] ?: @"";
    NSString *mobile = [OpenSDK_DataManager getInstance].currentAccount.mobile ?: @"";
    NSString *openId = [OpenSDK_DataManager getInstance].currentAccount.openId ?: @"";
    NSString *lastPage = _lastPage ?: _currentPage ?: @"";
    
    NSDictionary *extra = @{
        OPKJReportKeyDeviceId: deviceId,
        OPKJReportKeyDeviceType: deviceName,
        OPKJReportKeyCurrentTime: time,
        OPKJReportKeyPhone: mobile,
        OPKJReportKeyOpenId: openId,
        OPKJReportKeyLastPage: lastPage
    };
    return extra;
}

@end
