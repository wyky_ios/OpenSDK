//
//  OpenSDK_ReqDataGenerator.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/18.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_PayInfo.h"

@interface OpenSDK_ReqDataGenerator : NSObject


+(instancetype) getInstance;
typedef NS_ENUM(NSInteger,ReqDataSignEncryption){
    ReqDataSignEncryptionRSA,
    ReqDataSignEncryptionMD5,
};

+(NSString *) generateRegistrationDataWithName:(NSString *) userName
                                      password:(NSString *) password;

+(NSString *) generateUserCheckDataWithName:(NSString *) userName
                                     password:(NSString *) password;

+(NSString *) generateUserCheckDataWithToken:(NSString *) token
                                      openID:(NSString *) openID;

+(NSString *) generateCreateOrderDataWithPayInfo:(OpenSDK_PayInfo *) payInfo;

+(NSString *) generateLogoutDataWithToken:(NSString *) token
                                      uid:(NSString *) uid;

+(NSString *) generateQueryActiveCodeData:(NSString *) affect;

+(NSString *) generateBindActiveCodeData:(NSString *) code;

// DELETE
+(NSString *) generateQuickLoginData;

+(NSString *) generateTmpLoginData:(NSString *)tmpId;

+(NSString *) generateBindingDataWithName:(NSString *)userName password:(NSString *)password;

+(NSString *) generateBindingDataWithOtherSDK;

+(NSString *) generateFindPasswordData:(NSString *) email;

+(NSString *) generateUserCheckDataWithLoginToken:(NSString *)token;

+(NSString *) generateUserCheckDataWithPhoneNumber:(NSString *)phoneNumber
                                          password:(NSString *)password;

+(NSString *) generateUserCheckDataWithPhoneNumber:(NSString *)phoneNumber
                                    randomPassword:(NSString *)randomPassword;

+(NSString *) generateRegistrationDataWithPhone:(NSString *)phoneNumber
                                       password:(NSString *)password
                                 randomPassword:(NSString *)randomPassword;

+(NSString *) generateResetPasswordDataWithPhone:(NSString *)phoneNumber
                                        password:(NSString *)password
                                  randomPassword:(NSString *)randomPassword;

+(NSString *) generateFindPasswordDataWithPhoneNumber:(NSString *) phoneNumber;

+(NSString *) generateBindingDataPhoneNumber:(NSString *)phoneNumber
                                    password:(NSString *)password
                              randomPassword:(NSString *)randomPassword
                                      openId:(NSString *)openId
                                       token:(NSString *)token
                                    platform:(NSString *)platform;

//临时保存OtherSDK的
-(void) restoreSDKParameters:(NSMutableDictionary *) sdkParameter;

//获取OtherSDK返回的SDK参数
-(NSMutableDictionary *) getSDKParameters;

//产生初始化需要发送的数据
+(NSString *) generateInitData;

//产生设备信息
+(NSMutableDictionary *) generateDeviceInfo;

//产生初始化需要发送的数据+设备信息
+(NSMutableDictionary *) generateInitDataWithInfo;

//产生最终的生成数据 q字段必须urlEncode，默认加密方式RSA
+(NSString *) generateDataByDictionary:(NSMutableDictionary *) data;

/// 产生最终数据，
/// data 为请求参数
/// encryption 为加密方式，RSA/MD5两种方式
+(NSString *) generateDataByDictionary:(NSMutableDictionary *) data
                        signEncryption:(ReqDataSignEncryption)encryption;

//产生验证码请求数据
+(NSString *) generateSMSDataWithPhoneNumber:(NSString *)phoneNumber
                                     smsType:(NSString *)smsType;

//产生验证是否已经实名的请求数据
+(NSString *) generateCheckRealNameDataWithOpenID:(NSString *)openID
                                            token:(NSString *)token;
//产生实名认证的请求数据
+(NSString *) generateRealNameDataWithOpenID:(NSString *)openID
                                       token:(NSString *)token
                                    realName:(NSString *)realName
                                  cardNumber:(NSString *)cardNumber
                                 phoneNumber:(NSString *)phoneNumber
                                    security:(NSString *)security;

//产生实名制web请求的数据
+(NSString *) generateWebRealNameData;

//产生绑定web请求的数据
+(NSString *) generateBindData;

@end
