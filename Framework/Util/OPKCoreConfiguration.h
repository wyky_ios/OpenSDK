//
//  OPKTrackingConfiguration.h
//  YCOpenSDK
//
//  Created by ycgame on 2020/12/8.
//

@import Foundation;

#pragma mark - Page Name
extern NSString *const OPKPageLoginQuick;
extern NSString *const OPKPageLoginMobile;
extern NSString *const OPKPageLoginOther;
extern NSString *const OPKPageLoginOneKey;
extern NSString *const OPKPageLoginPassword;
extern NSString *const OPKPageLoginCaptcha;


#pragma mark - Event Name
extern NSString *const OPKEventPageView;
extern NSString *const OPKEventLoginFinish;
extern NSString *const OPKEventLoginOther;
extern NSString *const OPKEventLoginMobile;
extern NSString *const OPKEventLoginMobileNext;
extern NSString *const OPKEventLoginPassword;
extern NSString *const OPKEventLoginPasswordNext;
extern NSString *const OPKEventLoginApple;
extern NSString *const OPKEventLoginQQ;
extern NSString *const OPKEventLoginWeChat;
extern NSString *const OPKEventLoginOneKey;
extern NSString *const OPKEventLoginOnekeyNext;
extern NSString *const OPKEventLoginMobileEnd;
extern NSString *const OPKEventLoginQuickNext;

#pragma mark - J Report Key
extern NSString *const OPKJReportKeyDeviceId;
extern NSString *const OPKJReportKeyCurrentTime;
extern NSString *const OPKJReportKeyOpenId;
extern NSString *const OPKJReportKeyPhone;
extern NSString *const OPKJReportKeyDeviceType;
extern NSString *const OPKJReportKeyLastPage;
extern NSString *const OPKJReportKeyLoginType;
