//
//  OPKTrackingConfiguration.m
//  YCOpenSDK
//
//  Created by ycgame on 2020/12/8.
//

#import "OPKCoreConfiguration.h"

#pragma mark - Page Name
NSString *const OPKPageLoginQuick = @"快捷登录";
NSString *const OPKPageLoginMobile = @"手机验证码登录";
NSString *const OPKPageLoginOther = @"其他登录";
NSString *const OPKPageLoginOneKey = @"一键登录";
NSString *const OPKPageLoginPassword = @"账号密码登录";
NSString *const OPKPageLoginCaptcha = @"输入验证码";


#pragma mark - Event Name
NSString *const OPKEventPageView = @"page_view";
NSString *const OPKEventLoginFinish = @"login_finish";
NSString *const OPKEventLoginOther = @"login_other";
NSString *const OPKEventLoginMobile = @"login_phone";
NSString *const OPKEventLoginOneKey = @"login_onekey";
NSString *const OPKEventLoginMobileNext = @"login_phone_next";
NSString *const OPKEventLoginPassword = @"login_pwd";
NSString *const OPKEventLoginPasswordNext = @"login_pwd_next";
NSString *const OPKEventLoginApple = @"login_apple";
NSString *const OPKEventLoginQQ = @"login_qq";
NSString *const OPKEventLoginWeChat = @"login_wechat";
NSString *const OPKEventLoginOnekey = @"login_onekey";
NSString *const OPKEventLoginOnekeyNext = @"login_onekey_next";
NSString *const OPKEventLoginMobileEnd = @"login_phone_end";
NSString *const OPKEventLoginQuickNext = @"login_quick_next";


#pragma mark - J Report Key
NSString *const OPKJReportKeyDeviceId = @"device_id";
NSString *const OPKJReportKeyCurrentTime = @"cur_time";
NSString *const OPKJReportKeyOpenId = @"open_id";
NSString *const OPKJReportKeyPhone = @"phone";
NSString *const OPKJReportKeyDeviceType = @"device_type";
NSString *const OPKJReportKeyLastPage = @"last_page";
NSString *const OPKJReportKeyLoginType = @"login_type";
