//
//  OPKDevice.h
//  YCOpenSDK
//
//  Created by MacPro on 2020/12/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OPKDevice : NSObject
+ (NSString *)deviceModel;
@end

NS_ASSUME_NONNULL_END
