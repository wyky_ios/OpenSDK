//
//  SSDKEncryptUtil.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 15/10/7.
//  Copyright © 2015年 WYHT. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface OpenSDK_EncryptUtil : NSObject

+ (NSString *) generateSignWithDictionary : (NSMutableDictionary *) dictionary
                               useSignKey : (NSString *) key
                              useSignValue:(NSString *)value;

//通过公钥签名数据
+(NSString *) generateRSAResultByData:(NSMutableDictionary *) data;

+(NSString *) generateMD5ResultByData:(NSMutableDictionary *) data;


//+(NSString *) oldGenerateRSAResultByData:(NSMutableDictionary *)data;
//+(NSString *) newGenerateRSAResultByData:(NSMutableDictionary *)data;

@end
