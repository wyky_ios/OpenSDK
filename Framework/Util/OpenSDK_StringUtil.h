//
//  OpenSDK_StringUtil.h
//  YCOpenSDK
//
//  Created by Mac on 2021/5/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenSDK_StringUtil : NSString

/// 身份证验证
+(BOOL)validateIDCardNumber:(NSString *)value;


@end

NS_ASSUME_NONNULL_END
