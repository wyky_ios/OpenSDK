//
//  SSDKJsonUtil.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 15/10/7.
//  Copyright © 2015年 WYHT. All rights reserved.
//

#import "OpenSDK_JsonUtil.h"
#import "OpenSDK_BaseResData.h"

@implementation OpenSDK_JsonUtil

+ (NSString *) toJsonWithObject:(id)obj
{
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:obj
                                                       options:0
                                                         error:&writeError];
    
    if (jsonData) {
        NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonStr;
    }
    NSLog(@"%@", writeError);
    
    return @"";
}

+ (OpenSDK_BaseResData *) toObjectWithJson:(NSString *)json {
    NSError *readError = nil;
    
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    OpenSDK_BaseResData *obj = [NSJSONSerialization JSONObjectWithData:data
                                               options:0
                                                 error:&readError];
    if (!obj) {
        NSLog(@"%@", readError);
    }
    return obj;
}

@end
