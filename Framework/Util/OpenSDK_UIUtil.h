//
//  OpenSDK_UIUtil.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/2/1.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenSDK_UIUtil : NSObject

//获取当前Window
+ (UIWindow *)getCurrentWindow;

//获取当前viewController
+ (UIViewController *)getCurrentVC;

// 获取当前最外层的VC 新
+ (UIViewController *)currentViewController;

//获取第一个parentViewController。如果没有，返回传入VC
+ (UIViewController *)getFirstParentVC:(UIViewController *)vc;

//获取系统适配缩放比
+ (CGFloat)getSystemScale;

//计算系统适配缩放后大小
+ (CGFloat)getUISize:(CGFloat) value;

@end
