//
//  OpenSDK_ImageUtil.h
//  OpenSDK
//
//  Created by 涂俊 on 2020/3/6.
//  Copyright © 2020 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenSDK_ImageUtil : NSObject

//补全图片的路径名，用于加载bundle中的图片
+ (NSString *)getImageNameByBoundleName:(NSString *)boundleName
                              imageName:(NSString *)imageName;

//动态拉伸
+ (UIImage *)resizeWithImage:(UIImage *) image;

//动态拉伸
+ (UIImage *)resizeWithImage:(UIImage *)image leftMargin:(CGFloat)lm rightMargin:(CGFloat)rm topMargin:(CGFloat)tm bottomMargin:(CGFloat)bm;

//横向平铺拉伸
+ (UIImage *)resizeTileWithImage:(UIImage *)image;

//横向平铺拉伸
+ (UIImage *)resizeTileWithImage:(UIImage *)image leftMargin:(CGFloat)lm rightMargin:(CGFloat)rm topMargin:(CGFloat)tm bottomMargin:(CGFloat)bm;

//等比尺寸缩放图片(@deprecated)
+ (UIImage *)imageCompress:(UIImage*)image scale:(float)scale;

//缩放图片到指定尺寸
+ (UIImage *)scaleWithImage:(UIImage*)image targetSize:(CGSize)size;

//先压缩图片质量，再压缩图片尺寸，直到图片小于指定大小
+ (UIImage *)compressWithImage:(UIImage*)image maxSize:(CGFloat)maxSize;

//合成两张图片
+ (UIImage *)mixWithBgImage:(UIImage *)bgImage coverImage:(UIImage *)coverImage coverRect:(CGRect)coverRect;


@end

NS_ASSUME_NONNULL_END
