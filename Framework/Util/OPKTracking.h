//
//  OPKTracking.h
//  YCOpenSDK
//
//  Created by ycgame on 2020/12/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OPKTracking : NSObject

@property(nonatomic, strong, readonly)NSString *lastPage;
@property(nonatomic, strong, readonly)NSString *currentPage;

+ (instancetype)shared;

- (void)config;

// 浏览
- (void)pageView:(NSString *)pageName;

// 事件
- (void)event:(NSString *)eventName;

- (void)event:(NSString *)eventName parameters:(NSDictionary *)param;

@end

NS_ASSUME_NONNULL_END
