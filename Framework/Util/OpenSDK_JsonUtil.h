//
//  SSDKJsonUtil.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 15/10/7.
//  Copyright © 2015年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenSDK_JsonUtil : NSObject

+ (NSString *) toJsonWithObject : (id) obj;

+ (id) toObjectWithJson : (NSString *) json;

@end
