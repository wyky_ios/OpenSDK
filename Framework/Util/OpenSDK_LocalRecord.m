//
//  OpenSDK_LocalRecord.m
//  
//
//  Created by wyht－ios－dev on 16/3/10.
//
//

#import "OpenSDK_LocalRecord.h"
#import "OpenSDK_DataKeys.h"

@implementation OpenSDK_LocalRecord

static OpenSDK_LocalRecord *instance = nil;

+(instancetype) getInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

-(void) saveData:(NSString *)key value:(NSString *)value
{
    if (key == nil || value == nil) { return; }
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
}

-(void) saveLocalDateByUserID:(NSString *)userID
                        token:(NSString *)token {
    [self saveData:OpenSDK_L_OPENID value:userID];
    [self saveData:OpenSDK_L_USERTOKEN value:token];
    [self saveData:OpenSDK_L_AUTOLOGINFLAG value:@"true"];
}

-(void) delLocalData
{
    //不管什么情况都不删除数据，目的是为了方便游客登陆
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:OpenSDK_L_OPENID];
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:OpenSDK_L_USERTOKEN];
    //只清除自动登陆的标志位
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:OpenSDK_L_AUTOLOGINFLAG];
}

-(void) delLocalDataForKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
}

-(NSString *) getLocalValueForKey:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

@end
