//
//  OpenSDK_LocalRecord.h
//  
//
//  Created by wyht－ios－dev on 16/3/10.
//
//

#import <Foundation/Foundation.h>

@interface OpenSDK_LocalRecord : NSObject

+(instancetype) getInstance;

-(void) saveData:(NSString *) key
           value:(NSString *) value;

//删除本地OpenSDK的所有数据(不包括AppsFlyer的数据)
-(void) delLocalData;

//保存本地的id和token
-(void) saveLocalDateByUserID:(NSString *)userID
                        token:(NSString *)token;

-(void) delLocalDataForKey:(NSString *) key;

-(NSString *) getLocalValueForKey:(NSString *) key;

@end
