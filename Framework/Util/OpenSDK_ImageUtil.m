//
//  OpenSDK_ImageUtil.m
//  OpenSDK
//
//  Created by 涂俊 on 2020/3/6.
//  Copyright © 2020 WYHT. All rights reserved.
//

#import "OpenSDK_ImageUtil.h"

@implementation OpenSDK_ImageUtil

+ (NSString *)getImageNameByBoundleName:(NSString *)boundleName
                              imageName:(NSString *)imageName {
    return [[boundleName stringByAppendingString:@".bundle/"] stringByAppendingString:imageName];
}

+ (UIImage *)resizeWithImage:(UIImage *)image {
    CGFloat top = image.size.height/2.0;
    CGFloat left = image.size.width/2.0;
    CGFloat bottom = image.size.height/2.0 - 1;
    CGFloat right = image.size.width/2.0 - 1;
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(top, left, bottom, right)];
}

+ (UIImage *)resizeWithImage:(UIImage *)image leftMargin:(CGFloat)lm rightMargin:(CGFloat)rm topMargin:(CGFloat)tm bottomMargin:(CGFloat)bm {
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(tm, lm, bm, rm) resizingMode:UIImageResizingModeStretch];
}

+ (UIImage *)resizeTileWithImage:(UIImage *)image {
    return [image resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeTile];
}

+ (UIImage *)resizeTileWithImage:(UIImage *)image leftMargin:(CGFloat)lm rightMargin:(CGFloat)rm topMargin:(CGFloat)tm bottomMargin:(CGFloat)bm {
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(tm, lm, bm, rm) resizingMode:UIImageResizingModeTile];
}

+ (UIImage *)imageCompress:(UIImage*)image scale:(float)scale {
    if (image == nil) {
        return nil;
    }
    CGSize size = image.size;
    CGFloat width = size.width;
    CGFloat height = size.height;
    CGFloat scaledWidth = width * scale;
    CGFloat scaledHeight = height * scale;
    UIGraphicsBeginImageContext(CGSizeMake(scaledWidth, scaledHeight));
    [image drawInRect:CGRectMake(0, 0, scaledWidth, scaledHeight)];
    UIImage* newImage= UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)scaleWithImage:(UIImage*)image targetSize:(CGSize)size {
    if (image == nil) {
        return nil;
    }
    if ([UIScreen mainScreen].scale > 0) {
        UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage* newImage= UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)compressWithImage:(UIImage*)image maxSize:(CGFloat)maxSize {
    // Compress by quality
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    if (data.length < maxSize) return image;
    
    CGFloat max = 1;
    CGFloat min = 0;
    UIImage *resultImage = nil;
    BOOL qualityCompressOK = NO;
    for (int i = 0; i < 8; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(image, compression);
        resultImage = [UIImage imageWithData:data];
        if (UIImageJPEGRepresentation(resultImage, 1).length < maxSize * 0.8) {
            min = compression;
        } else if (UIImageJPEGRepresentation(resultImage, 1).length > maxSize) {
            max = compression;
        } else {
            qualityCompressOK = YES;
            break;
        }
    }
    if (qualityCompressOK)
        return resultImage;
    
    // Compress by size
    NSUInteger lastDataLength = 0;
    data = UIImageJPEGRepresentation(resultImage, 1);
    while (data.length > maxSize && data.length != lastDataLength) {
        lastDataLength = data.length;
        CGFloat ratio = (CGFloat)maxSize / data.length;
        CGSize size = CGSizeMake((NSUInteger)(resultImage.size.width * sqrtf(ratio)),
                                 (NSUInteger)(resultImage.size.height * sqrtf(ratio))); // Use NSUInteger to prevent white blank
        UIGraphicsBeginImageContext(size);
        [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(resultImage, 1);
    }
    return resultImage;
}


+ (UIImage *)mixWithBgImage:(UIImage *)bgImage coverImage:(UIImage *)coverImage coverRect:(CGRect)coverRect {
    if (bgImage == nil) {
        return nil;
    }
    if (coverImage == nil) {
        return bgImage;
    }
    UIGraphicsBeginImageContext(bgImage.size);
    [bgImage drawInRect:CGRectMake(0, 0, bgImage.size.width, bgImage.size.height)];
    [coverImage drawInRect:coverRect];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}

@end
