//
//  SSDKHttpUtil.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 15/10/6.
//  Copyright © 2015年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_BaseResData.h"
#import <UIKit/UIControl.h>
#import <UIKit/UIAlert.h>
#import "OpenSDK_PayInfo.h"

@interface OpenSDK_HttpUtil : NSObject

+(instancetype) getInstance;

+(void) postWithUrl : (NSString *) url
               data : (NSString *) data
       httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

+(void) getWithUrl:(NSString *) url
              data:(NSString *) data
     httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

+(void) delWithUrl:(NSString *) url
              data:(NSString *) data
     httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

+ (void) postWithUrl : (NSString *)url
                data : (NSString *)data
              timeout: (NSUInteger)time
        httpDelegate : (void (^)(OpenSDK_BaseResData* resData))gotInfo;

//初始化接口
+(void) initWithDelegate:(void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//游客登录(老版) DELETE
+(void) quickLogin:(void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//激活状态查询接口
+(void) queryAppActiveStatus:(NSString *)affect
               httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//绑定激活码
+(void) bindActiveCode:(NSString *)code
         httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//使用手机号码注册
+(void) registerWithPhoneNumber:(NSString *)phoneNumber
                         passwd:(NSString *)passwd
                 randomPassword:(NSString *)randomPassword
                   httpDelegate:(void (^)(OpenSDK_BaseResData *))gotInfo;

//用户注册接口
+(void) registerWithUserName:(NSString*) userName
                     passwd : (NSString *) passwd
               httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//游客登录
+(void) loginWithTmpId:(NSString *)tmpId
          httpDelegate:(void (^)(OpenSDK_BaseResData* resData)) gotInfo;

+(void) loginWithOneKeyToken:(NSString *)token httpDelegate:(void (^)(OpenSDK_BaseResData *))gotInfo;

//邮箱+密码登录接口
+(void) loginWithUserName:(NSString*) userName
                  passwd : (NSString *) passwd
            httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//手机号＋密码登录
+(void) loginWithPhoneNumber:(NSString *)phoneNumber
                      passwd:(NSString *)passwd
                httpDelegate:(void (^)(OpenSDK_BaseResData *))gotInfo;

//手机号＋验证码登录
+(void) loginWithPhoneNumber:(NSString *)phoneNumber
                randomPasswd:(NSString *)randomPasswd
                httpDelegate:(void (^)(OpenSDK_BaseResData *))gotInfo;

//发送验证码
+(void) sendSMSWithPhoneNumber:(NSString *)phoneNumber
                       smsType:(NSString *)smsType
                  httpDelegate:(void (^)(OpenSDK_BaseResData *))gotInfo;

//使用用户名 密码绑定
+(void) bindingWithUserName:(NSString*) userName
                    passwd : (NSString *) passwd
              httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//其它sdk绑定
+(void) bindingWithOtherSDK:(void (^)(OpenSDK_BaseResData *))gotInfo;

//登出
+(void) logoutWithToken:(NSString *) token
                    UID:(NSString *) UID
          httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//第三方SDK登录
+(void) loginWithOtherSDK : (NSDictionary *) params
                  platform:(NSString *)platform
           platformVersion:(NSString *)platformVersion
             httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//根据token验证用户登录状态
+(void) checkUserWithToken:(NSString *) token
                    openID:(NSString *) openID
             httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//支付创建订单接口
+(void) createOrderWithPayInfo:(OpenSDK_PayInfo *) payInfo
                 httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//查询订单的接口
+(void) checkOrderWithOrderInfo:(NSString *) orderInfo
                        orderID:(NSString *) orderID
                  httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

// 查询苹果支付订单
+ (void)checkAppleOrderWithParams:(NSString *)params
                     httpDelegate:(void (^)(OpenSDK_BaseResData *))gotInfo;

// check苹果兑换商品订单
+ (void)checkApplePromoOrderWithParams:(NSString *)params
                          httpDelegate:(void (^)(OpenSDK_BaseResData *))gotInfo;

//找回密码接口
+(void) findPasswordWithEmail:(NSString *) email
                 httpDelegate: (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//通话手机号码找回密码
+(void) findPasswordWithPhone:(NSString *) phoneNumber
                 httpDelegate:(void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//重置密码接口
+(void) resetPasswordWithPhoneNumber:(NSString *)phoneNumber
                              passwd:(NSString *)passwd
                      randomPassword:(NSString *)randomPassword
                        httpDelegate:(void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//绑定手机号码
+(void) bindingWithPhoneNumber:(NSString *)phoneNumber
                        passwd:(NSString *)passwd
                randomPassword:(NSString *)randomPassword
                        openId:(NSString *)openId
                         token:(NSString *)token
                      platform:(NSString *)platform
                  httpDelegate:(void (^)(OpenSDK_BaseResData *))gotInfo;

//检查用户是否实名制
+(void) checkRealNameWithOpenID:(NSString *)openID
                          token:(NSString *)token
                   httpDelegate:(void (^)(OpenSDK_BaseResData *))gotInfo;

//进行实名认证
+(void) realNameWithOpenID:(NSString *)openID
                     token:(NSString *)token
                  realName:(NSString *)name
                cardNumber:(NSString *)cardNumber
               phoneNumber:(NSString *)phoneNumber
                  security:(NSString *)security
              httpDelegate:(void (^)(OpenSDK_BaseResData *))gotInfo;

+(void) getGameProtrolWithDelegate:(void (^)(OpenSDK_BaseResData* resData)) gotInfo;

//获取商品id列表
+(void) getProductIds:(void (^)(OpenSDK_BaseResData* resData)) gotInfo;

+(NSString *) urlEncode:(NSString *) urlStr;

@end



