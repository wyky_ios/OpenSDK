//
//  SSDKMD5.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 15/10/11.
//  Copyright © 2015年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenSDK_MD5 : NSObject

+ (NSString *) md5 : (NSString *) encStr;

@end
