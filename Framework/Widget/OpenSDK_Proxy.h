//
//  OpenSDK_Proxy.h
//  Pods-Unity-iPhone
//
//  Created by ycgame on 2020/4/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 弱引用方法代理
@interface OpenSDK_Proxy : NSObject
@property(nonatomic, weak)id targat;

+ (instancetype)proxyWithTarget:(id)target;
@end

NS_ASSUME_NONNULL_END
