/*

iToast.h

MIT LICENSE

Copyright (c) 2012 Guru Software

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum OpenSDK_iToastGravity {
	OpenSDK_iToastGravityTop = 1000001,
	OpenSDK_iToastGravityBottom,
	OpenSDK_iToastGravityCenter
}OpenSDK_iToastGravity;

typedef enum OpenSDK_iToastDuration {
	OpenSDK_iToastDurationLong = 10000,
	OpenSDK_iToastDurationShort = 1000,
	OpenSDK_iToastDurationNormal = 3000
}OpenSDK_iToastDuration;

typedef enum OpenSDK_iToastType {
	OpenSDK_iToastTypeInfo = -100000,
	OpenSDK_iToastTypeNotice,
	OpenSDK_iToastTypeWarning,
	OpenSDK_iToastTypeError,
	OpenSDK_iToastTypeNone // For internal use only (to force no image)
}OpenSDK_iToastType;

typedef enum {
    OpenSDK_iToastImageLocationTop,
    OpenSDK_iToastImageLocationLeft
} OpenSDK_iToastImageLocation;


@class OpenSDK_iToastSettings;

@interface OpenSDK_iToast : NSObject {
	OpenSDK_iToastSettings *_settings;
	
	NSTimer *timer;
	
	UIView *view;
	NSString *text;
}

- (void) show;
- (void) show:(OpenSDK_iToastType) type;
- (OpenSDK_iToast *) setDuration:(NSInteger ) duration;
- (OpenSDK_iToast *) setGravity:(OpenSDK_iToastGravity) gravity 
			 offsetLeft:(NSInteger) left
			 offsetTop:(NSInteger) top;
- (OpenSDK_iToast *) setGravity:(OpenSDK_iToastGravity) gravity;
- (OpenSDK_iToast *) setPostion:(CGPoint) position;
- (OpenSDK_iToast *) setFontSize:(CGFloat) fontSize;
- (OpenSDK_iToast *) setUseShadow:(BOOL) useShadow;
- (OpenSDK_iToast *) setCornerRadius:(CGFloat) cornerRadius;
- (OpenSDK_iToast *) setBgRed:(CGFloat) bgRed;
- (OpenSDK_iToast *) setBgGreen:(CGFloat) bgGreen;
- (OpenSDK_iToast *) setBgBlue:(CGFloat) bgBlue;
- (OpenSDK_iToast *) setBgAlpha:(CGFloat) bgAlpha;
- (OpenSDK_iToast *) setMaxWidth:(CGFloat) maxWidth;
- (OpenSDK_iToast *) setMaxHeight:(CGFloat) maxHeight;

+ (OpenSDK_iToast *) makeText:(NSString *) text;

-(OpenSDK_iToastSettings *) theSettings;

@end



@interface OpenSDK_iToastSettings : NSObject<NSCopying>{
	NSInteger duration;
	OpenSDK_iToastGravity gravity;
	CGPoint postition;
    CGFloat maxWidth;
    CGFloat maxHeight;
	OpenSDK_iToastType toastType;
	CGFloat fontSize;
	BOOL useShadow;
	CGFloat cornerRadius;
	CGFloat bgRed;
	CGFloat bgGreen;
	CGFloat bgBlue;
	CGFloat bgAlpha;
	NSInteger offsetLeft;
	NSInteger offsetTop;

	NSDictionary *images;
	
	BOOL positionIsSet;
}


@property(assign) NSInteger duration;
@property(assign) OpenSDK_iToastGravity gravity;
@property(assign) CGPoint postition;
@property(assign) CGFloat maxWidth;
@property(assign) CGFloat maxHeight;
@property(assign) CGFloat fontSize;
@property(assign) BOOL useShadow;
@property(assign) CGFloat cornerRadius;
@property(assign) CGFloat bgRed;
@property(assign) CGFloat bgGreen;
@property(assign) CGFloat bgBlue;
@property(assign) CGFloat bgAlpha;
@property(assign) NSInteger offsetLeft;
@property(assign) NSInteger offsetTop;
@property(readonly) NSDictionary *images;
@property(assign) OpenSDK_iToastImageLocation imageLocation;


- (void) setImage:(UIImage *)img forType:(OpenSDK_iToastType) type;
- (void) setImage:(UIImage *)img withLocation:(OpenSDK_iToastImageLocation)location forType:(OpenSDK_iToastType)type;
+ (OpenSDK_iToastSettings *) getSharedSettings;
						  
@end
