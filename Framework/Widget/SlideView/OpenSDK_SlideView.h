//
//  OpenSDK_SlideView.h
//  code
//
//  Created by 涂俊 on 2018/8/27.
//  Copyright © 2018年 Wicky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DWSlideCaptchaView.h"


@interface OpenSDK_SlideView : UIView<DWSlideCaptchaDelegate>

typedef BOOL (^SlideView_IndentifyBeginBlock)(OpenSDK_SlideView* view);
typedef void (^SlideView_IndentifyCompletionBlock)(OpenSDK_SlideView* view, BOOL success);
typedef void (^SlideView_LoadNewImageBlock)(OpenSDK_SlideView* view);
typedef void (^SlideView_DisplayHideBlock)(BOOL display);

@property (nonatomic ,readonly) BOOL isSuccess;

// 默认滑块
@property (nonatomic ,strong) UISlider * slider;
// 底部轨道按键图
@property (nonatomic ,strong) UIImage * sliderThumbImage;
// 底部轨道背景图
@property (nonatomic ,strong) UIImage * sliderTrackBg;
// 底部轨道提示文字
@property (nonatomic ,strong) NSString * sliderTipText;
// 底部轨道验证成功文字
@property (nonatomic ,strong) NSString * sliderSuccessText;

// 验证视图
@property (nonatomic ,strong) DWSlideCaptchaView * captchaView;

// 开始滑块回调（返回值如果为true，则此次拖动有效，否则无效）
@property (nonatomic ,copy) SlideView_IndentifyBeginBlock indentifyBegin;

// 验证回调
@property (nonatomic ,copy) SlideView_IndentifyCompletionBlock indentifyCompletion;

// 加载新图片的回调
@property (nonatomic ,copy) SlideView_LoadNewImageBlock loadNewImage;

// 是否可以滑动拼图
@property (nonatomic) BOOL dragable;

// 错误次数达到此值时，会触发加载新图片的回调，否则只是重置拼图(默认为0，表示不会加载新图片)
@property (nonatomic) NSUInteger errorResetCount;

// 加载新的图片，生成拖动的拼图滑块
-(void) setImage:(UIImage *)image;

// 异步加载新的图片，生成拖动的拼图滑块
-(void) setImageWithUrl:(NSString *)url;

// 设置隐藏模式：改模式下图片一开始隐藏，滑动过程中显示，验证成功也隐藏
-(void) setHideMode:(SlideView_DisplayHideBlock) callback;

//// 显示图片
//-(void) showImageWithAnimation:(BOOL) animate;
//
//// 隐藏图片
//-(void) hideImageWithAnimation:(BOOL) animate;

@end
