//
//  OpenSDK_Loading.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/9/13.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSDK_Loading : UIViewController

-(void) dismissView;

@end
