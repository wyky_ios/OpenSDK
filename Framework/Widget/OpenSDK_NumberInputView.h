//
//  OpenSDK_NumberInputView.h
//
//  Created by 涂俊 on 2018/8/10.
//

#import <UIKit/UIKit.h>

//selected
typedef void(^BlockInputComplete)(NSString *text);

@interface OpenSDK_NumberInputView : UIView

/**文字颜色*/
@property (nonatomic,assign) UIColor *textColor;

/**文字颜色*/
@property (nonatomic,assign) UIFont *textFont;

/**占位横线颜色*/
@property (nonatomic,assign) UIColor *lineColor;

/**位数*/
@property (nonatomic,assign) NSInteger numberLength;

/**否密文显示*/
@property (nonatomic,assign) BOOL secureTextEntry;

/**否密文显示*/
@property (nonatomic,assign) id<UITextFieldDelegate> delegate;

/**
 * 完成的回调
 */
@property (nonatomic, copy) BlockInputComplete completeCallback;

- (void)becomeFirstResponder;

- (void)clearText;

@end
