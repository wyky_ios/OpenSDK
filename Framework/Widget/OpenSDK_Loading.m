//
//  OpenSDK_Loading.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/9/13.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import "OpenSDK_Loading.h"

@implementation OpenSDK_Loading {
    UIView *contentView;
    UIView *rootView;
    UIActivityIndicatorView *indicator;
}

-(void) viewDidLoad{
    [self initRootView];
    [self initIndiacator];
}

-(void) initRootView {
    //透明背景view
    contentView=[[UIView alloc] init];
    [contentView setBackgroundColor:[UIColor clearColor]];
    self.view=contentView;
    
    //实际显示的区域
    rootView=[[UIView alloc] init];
    [rootView setBackgroundColor:[UIColor clearColor]];
    rootView.layer.shadowOffset = CGSizeMake(2, 2);
    rootView.layer.shadowOpacity = 0.6;
    rootView.layer.shadowColor = [UIColor whiteColor].CGColor;
    [rootView setTranslatesAutoresizingMaskIntoConstraints:NO];
    //宽度
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:rootView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
    //高度
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:rootView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0]];
    //居中
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:rootView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:rootView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    
    [contentView addSubview:rootView];
}

-(void) initIndiacator{
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicator startAnimating];
    [indicator setTranslatesAutoresizingMaskIntoConstraints:NO];
    [indicator setColor:[UIColor whiteColor]];
    //水平居中
    [rootView addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:rootView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    [rootView addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:rootView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    [rootView addSubview:indicator];
}

-(void) dismissView{
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
