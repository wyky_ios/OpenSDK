//
//  下拉列表
//  OpenSDK_PullDownView.h
//
//  Created by 涂俊 on 2018/7/17.
//  Copyright © 2018年 wang_ziwu. All rights reserved.
//

#import <UIKit/UIKit.h>

//selected
typedef void(^BlockSelectedMenu)(NSInteger menuRow);
typedef void(^BlockClose)(void);

//NS_ENUM，定义状态等普通枚举
typedef NS_ENUM(NSUInteger, AnchorPlace) {
    AnchorPlaceMiddle,
    AnchorPlaceLeft,
    AnchorPlaceRight
};

@interface OpenSDK_PullDownView : UIView
/**
 *  蒙层背景color
 */
@property (nonatomic, strong) UIColor *coverBgColor;
/**
 *  主样式color
 */
@property (nonatomic, strong) UIColor *menuBgColor;
/**
 *  cel高度
 */
@property (nonatomic, assign) CGFloat menuCellHeight;
/**
 *  table最大高度限制
 *  默认：3 * cellHeight
 */
@property (nonatomic, assign) CGFloat menuMaxHeight;
/**
 * table的宽度，如果不指定，则自动计算宽度
 */
@property (nonatomic, assign) CGFloat menuWidth;

/**
 * 选择内容的回调
 */
@property (nonatomic, copy) BlockSelectedMenu blockSelectedMenu;

@property (nonatomic, copy) BlockClose blockClose;

/**
 * 下拉菜单的位置:居中，靠左，靠右
 */
@property (nonatomic) AnchorPlace place;

/** list*/
@property (nonatomic, strong) UITableView *mTable;

// 底部附加视图
@property (nonatomic, strong)UIView *additionalView;

@property (nonatomic, assign)CGFloat additionalViewHeight;

/**
 *  anchorView：下拉依赖视图[推荐初始化]
 *  箭头指向依赖视图
 *  titleArray:文字
 *  imageArray:icon
 *  menuArray:图文Model
 */
+ (instancetype)pullMenuAnchorView:(UIView *)anchorView
                     tableDelegate:(id<UITableViewDataSource>)delegate
                         menuWidth:(CGFloat)menuWidth
                       cellNibName:(NSString *)cellNibName
               cellReuseIdentifier:(NSString *)cellReuseIdentifier;

+ (instancetype)pullMenuAnchorView:(UIView *)anchorView
                     tableDelegate:(id<UITableViewDataSource>)delegate
                         menuWidth:(CGFloat)menuWidth
                         cellClass:(Class)cellClass
               cellReuseIdentifier:(NSString *)cellReuseIdentifier;

/**
 * 展示下拉列表
 */
- (void)show:(BOOL) rotateAnchorView;

/**
 * 刷新下拉列表
 */
- (void)reloadMenu;

/**
 * 收起下拉列表
 */
- (void)animateRemoveView;

@end
