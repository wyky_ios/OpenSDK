//
//  OpenSDK_TextField.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/4/5.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import "OpenSDK_TextField.h"

@implementation OpenSDK_TextField

static CGFloat ICON_TOTAL_WIDTH = 36;
static CGFloat ICON_VERTICAL_PADDING = 6;

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = [UIColor colorWithRed:202/255.0 green:202/255.0 blue:202/255.0 alpha:1].CGColor;
    self.layer.cornerRadius = 4;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [super editingRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

// 设置左边icon图片
- (void) setIconImage:(UIImage *)iconImage {
    _iconImage = iconImage;
    
    UIImageView *iconView = [[UIImageView alloc] init];
    iconView.image = iconImage;
    iconView.contentMode = UIViewContentModeScaleAspectFit;
    [self setEdgeView:iconView width:ICON_TOTAL_WIDTH horizontalPadding:ICON_VERTICAL_PADDING verticalPadding:ICON_VERTICAL_PADDING isLeft:YES];
}

// 设置view到最左或最右
- (void) setEdgeView:(UIView *)view
               width:(CGFloat)width
     horizontalPadding:(CGFloat)horizontalPadding
     verticalPadding:(CGFloat)verticalPadding
              isLeft:(BOOL)isLeft {
    
    UIView *bgView = [[UIView alloc]initWithFrame: CGRectMake(0, 0, width, self.frame.size.height)];
    // 传入view(居中)
//    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    CGFloat viewWidth = width - 2 * horizontalPadding;
    CGFloat viewHeight = self.frame.size.height - 2 * verticalPadding;
    view.frame = CGRectMake(horizontalPadding, verticalPadding, viewWidth, viewHeight);
    [bgView addSubview:view];
    
//    [bgView addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1.0 constant:viewWidth]];
//    [bgView addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:viewHeight]];
//    [bgView addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:bgView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
//    [bgView addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:bgView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    
    //分割线(最左或最右)
    CGFloat lineX;
    if (isLeft) {
        lineX = width - 1;
    } else {
        lineX = 0;
    }
    UIImageView *lineView = [[UIImageView alloc] initWithFrame:CGRectMake(lineX, 0, 1, bgView.frame.size.height)];
//    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    lineView.image = [UIImage imageNamed:@"op_icon_line"];
    lineView.contentMode = UIViewContentModeScaleAspectFit;
    [bgView addSubview:lineView];
//    [bgView addConstraint:[NSLayoutConstraint constraintWithItem:lineView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1.0 constant:1]];
//    [bgView addConstraint:[NSLayoutConstraint constraintWithItem:lineView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:self.frame.size.height]];
//    [bgView addConstraint:[NSLayoutConstraint constraintWithItem:lineView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:bgView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
//    [bgView addConstraint:[NSLayoutConstraint constraintWithItem:lineView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:bgView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
//    if (isLeft) {
//        [bgView addConstraint:[NSLayoutConstraint constraintWithItem:lineView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:bgView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0]];
//    } else {
//        [bgView addConstraint:[NSLayoutConstraint constraintWithItem:lineView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:bgView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0]];
//    }
    
    if (isLeft) {
        //设置左边icon图片
        [self setLeftView:bgView];
        //设置显示模式为永远显示(默认不显示 必须设置 否则没有效果)
        [self setLeftViewMode:UITextFieldViewModeAlways];
    } else {
        //设置左边icon图片
        [self setRightView:bgView];
        //设置显示模式为永远显示(默认不显示 必须设置 否则没有效果)
        [self setRightViewMode:UITextFieldViewModeAlways];
    }
}

@end
