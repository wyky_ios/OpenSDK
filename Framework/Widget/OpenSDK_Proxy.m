//
//  OpenSDK_Proxy.m
//  Pods-Unity-iPhone
//
//  Created by ycgame on 2020/4/7.
//

#import "OpenSDK_Proxy.h"

@implementation OpenSDK_Proxy

+ (instancetype)proxyWithTarget:(id)target {
    OpenSDK_Proxy *proxy = [[OpenSDK_Proxy alloc] init];
    proxy.targat = target;
    return proxy;
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
    return [self.targat methodSignatureForSelector:aSelector];
}

- (void)forwardInvocation:(NSInvocation *)anInvocation {
    [anInvocation invokeWithTarget:self.targat];
}

@end
