//
//  PayNowH5View.h
//
//  Created by 涂俊 on 2018/1/31.
//  Copyright © 2018年 wyht. All rights reserved.
//

#import <UIKit/UIKit.h>

// H5页面ViewController回调
@protocol H5ViewController_Delegate <NSObject>

@required
#pragma warning - 由于内部webView从UIWebView替换为WKWebView，所以此处数据格式可能会修改，使用时请于前端再次联调数据格式
/// web端调用native
/// @param name WKScriptMessage.name
/// @param obj WKScriptMessage.body
- (void) callbackWithName:(NSString *) name args:(id) obj;

- (void) close;

@end

// H5页面ViewController
@interface OpenSDK_H5ViewController : UIViewController

@property (nonatomic, readwrite) BOOL hasCloseBtn;

@property (nonatomic, readwrite, weak) id<H5ViewController_Delegate> delegate;

-(id) openWithGetUrl:(NSString *) url
           jsMethods:(NSArray *) methods;

-(id) openWithPostUrl:(NSString *) url
                 body:(NSString *) body
            jsMethods:(NSArray *) methods;

@end
