//
//  OPKWebViewScriptMessageObject.h
//  YCOpenSDK
//
//  Created by ycgame on 2020/9/23.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol OPKWebViewScriptMessageDelegate <NSObject>
- (void)userContentController:(nonnull WKUserContentController *)userContentController didReceiveScriptMessage:(nonnull WKScriptMessage *)message;
@end

@interface OPKWebViewScriptMessageObject : NSObject<WKScriptMessageHandler>
@property(nonatomic, weak)id<OPKWebViewScriptMessageDelegate> delegate;
- (instancetype)initWithDelegate:(id<OPKWebViewScriptMessageDelegate>)delegate;
@end

NS_ASSUME_NONNULL_END
