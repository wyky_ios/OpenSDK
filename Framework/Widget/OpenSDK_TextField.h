//
//  OpenSDK_TextField.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/4/5.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSDK_TextField : UITextField

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@property (nonatomic, assign) UIImage *iconImage;

- (void) setEdgeView:(UIView *)view
               width:(CGFloat)width
   horizontalPadding:(CGFloat)horizontalPadding
     verticalPadding:(CGFloat)verticalPadding
              isLeft:(BOOL)isLeft;

@end
