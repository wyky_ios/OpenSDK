//
//  OpenSDK_WebView.h
//  Pods-Unity-iPhone
//
//  Created by 马乾征 on 2020/3/26.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol OpenSDK_WebView_Delegate <NSObject>

@required

- (void) callbackWithName:(NSString *) name withArgs:(id) obj;

@optional

- (void) loadFinish:(BOOL) success;

@end

@interface OpenSDK_WebView : WKWebView

-(instancetype) initWithGetUrl:(NSString *) urlStr
           jsMethods:(NSArray *) methods
            callback:(id<OpenSDK_WebView_Delegate>) callback;

-(instancetype) initWithPostUrl:(NSString *) urlStr
             postBody:(NSString *) body
            jsMethods:(NSArray *) methods
             callback:(id<OpenSDK_WebView_Delegate>) callback;

@end

NS_ASSUME_NONNULL_END
