//
//  OPKMach.h
//  YCOpenSDK
//
//  Created by ycgame on 2020/12/3.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OPKMach : NSObject
#pragma 获取总内存大小
+ (NSString *)getTotalMemorySize;

#pragma 获取当前可用内存
+ (NSString *)getAvailableMemorySize;

#pragma 获取总磁盘容量
+ (NSString *)getTotalDiskSize;

#pragma 获取可用磁盘容量  f_bavail 已经减去了系统所占用的大小比 f_bfree 更准确
+ (NSString *)getAvailableDiskSize;

@end

NS_ASSUME_NONNULL_END
