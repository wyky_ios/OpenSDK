//
//  OPKWebViewScriptMessageObject.m
//  YCOpenSDK
//
//  Created by ycgame on 2020/9/23.
//

#import "OPKWebViewScriptMessageObject.h"

@implementation OPKWebViewScriptMessageObject
- (instancetype)initWithDelegate:(id<OPKWebViewScriptMessageDelegate>)delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}


- (void)userContentController:(nonnull WKUserContentController *)userContentController didReceiveScriptMessage:(nonnull WKScriptMessage *)message {
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(OPKWebViewScriptMessageDelegate)]) {
        [self.delegate userContentController:userContentController didReceiveScriptMessage:message];
    }
}

@end
