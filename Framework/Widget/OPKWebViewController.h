//
//  OPKWebViewController.h
//  YCOpenSDK
//
//  Created by ycgame on 2020/9/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OPKWebViewController : UIViewController
@property(nonatomic, copy)NSString *url;
@end

NS_ASSUME_NONNULL_END
