//
//  PayNowH5View.m
//
//  Created by wyht－ios－dev on 17/3/2.
//  Copyright © 2017年 WYHT. All rights reserved.
//

//#import "Widget_WebView.h"
#import "OpenSDK_WebView.h"
#import "OpenSDK_H5ViewController.h"
#import "OpenSDK_UIUtil.h"

@interface OpenSDK_H5ViewController () <OpenSDK_WebView_Delegate>

@end

@implementation OpenSDK_H5ViewController

{
    UIView *contentView;
//    Widget_WebView *webView;
    OpenSDK_WebView *webView;
    UIActivityIndicatorView *indicator;// loading
    UIButton *closeBtn;
}

- (void)viewDidLoad {
    //透明背景view
    contentView = [[UIView alloc] init];
    [contentView setBackgroundColor:[UIColor clearColor]];
    self.view = contentView;
    
    if(webView == nil)
        return;
    
    //webView[全屏]
    [webView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [contentView addSubview:webView];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:webView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:webView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:webView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:webView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
    
    //关闭按钮[右边上角]
    closeBtn = [[UIButton alloc] init];
    [closeBtn setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:0.5]];
    [closeBtn setTitle:@"X" forState:UIControlStateNormal];
    closeBtn.titleLabel.font = [UIFont systemFontOfSize:[OpenSDK_UIUtil getUISize:40]];
    [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [closeBtn setHidden:YES];
    [contentView addSubview:closeBtn];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:closeBtn attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:closeBtn attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:closeBtn attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1.0 constant:[OpenSDK_UIUtil getUISize:60]]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:closeBtn attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:[OpenSDK_UIUtil getUISize:60]]];
    
    //loading圈圈[居中]
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicator startAnimating];
    [indicator setTranslatesAutoresizingMaskIntoConstraints:NO];
    [indicator setColor:[UIColor blueColor]];
    [contentView addSubview:indicator];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
}

//隐藏状态栏
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)deviceOrientationDidChange:(NSNotification *)notification {
    CGRect rect = [[UIScreen mainScreen] bounds];
    CGSize size = rect.size;
    CGFloat width = size.width;
    CGFloat height = size.height;
    [contentView setCenter:CGPointMake(width/2, height/2)];
    contentView.frame = rect;
}

-(id) openWithGetUrl:(NSString *) url
             jsMethods:(NSArray *) methods {
    if (webView != nil) {
        return self;
    }
    webView = [[OpenSDK_WebView alloc] initWithGetUrl:url jsMethods:methods callback:self];
    return self;
}

-(id) openWithPostUrl:(NSString *) url
                   body:(NSString *) body
              jsMethods:(NSArray *) methods {
    if (webView != nil) {
        return self;
    }
    webView = [[OpenSDK_WebView alloc] initWithPostUrl:url postBody:body jsMethods:methods callback:self];
    return self;
}


- (void) loadFinish:(BOOL) success {
    if (_hasCloseBtn) {
        [closeBtn setHidden:NO];
    }
    [indicator removeFromSuperview];
}

- (void) callbackWithName:(NSString *) name withArgs:(id) obj {
    if (_delegate) {
        [_delegate callbackWithName:name args:obj];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void) close:(id) sender {
    if (_delegate) {
        [_delegate close];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
