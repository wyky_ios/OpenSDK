//
//  Localization_EN.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/30.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import "Localization_EN.h"
#import "LocalizationManager.h"

@implementation Localization_EN

-(id) init
{
    self=[super init];
    localizationInfo=[[NSMutableDictionary alloc] init];
    
    [localizationInfo setObject:@"Please enter your e-mail address" forKey:OpenSDK_LOCALIZATION_EMAIL];
    [localizationInfo setObject:@"Please enter your password" forKey:OpenSDK_LOCALIZATION_PASSWORD];
    [localizationInfo setObject:@"Login" forKey:OpenSDK_LOCALIZATION_SIGNIN];
    [localizationInfo setObject:@"Register" forKey:OpenSDK_LOCALIZATION_SIGNUP];
    [localizationInfo setObject:@"Guest Login" forKey:OpenSDK_LOCALIZATION_QUICKLOGIN];
    [localizationInfo setObject:@"  More:" forKey:OpenSDK_LOCALIZATION_SIGNINWITH];
    [localizationInfo setObject:@"Fail to login" forKey:OpenSDK_LOCALIZATION_SIGNINERROR];
    [localizationInfo setObject:@"The account or password can not be empty" forKey:OpenSDK_LOCALIZATION_WARN1];
    [localizationInfo setObject:@"Please enter your password again" forKey:OpenSDK_LOCALIZATION_CONFIRMPASSWORK];
    [localizationInfo setObject:@"Register failed" forKey:OpenSDK_LOCALIZATION_SIGNUPERROR];
    [localizationInfo setObject:@"The format of E-mail address is wrong" forKey:OpenSDK_LOCALIZATION_EMAILERROR];
    [localizationInfo setObject:@"Forget Password" forKey:OpenSDK_LOCALIZATION_FINDPASSWORD];
    [localizationInfo setObject:@"Password should be more than six characters and less than 20 characters" forKey:OpenSDK_LOCALIZATION_PASSWORDILLEGAL];
    [localizationInfo setObject:@"Confirm" forKey:OpenSDK_LOCALIZATION_OK];
    [localizationInfo setObject:@"The passwords you entered should be the same" forKey:OpenSDK_LOCALIZATION_PASSWORDCONFIRMERROR];
    [localizationInfo setObject:@"data format returned by servers is wrong" forKey:OpenSDK_LOCALIZATION_SERVERERROR1];
    [localizationInfo setObject:@"network connection timeout" forKey:OpenSDK_LOCALIZATION_SERVERERROR2];
    [localizationInfo setObject:@"can not detect the internet" forKey:OpenSDK_LOCALIZATION_NETWORKERROR];
    [localizationInfo setObject:@"Login Tips" forKey:OpenSDK_LOCALIZATION_ALERTTITLE];
    [localizationInfo setObject:@"Please bind your account in case of losing the game records!" forKey:OpenSDK_LOCALIZATION_ALERTMSG];
    [localizationInfo setObject:@"Back" forKey:OpenSDK_LOCALIZATION_GOBACK];
    [localizationInfo setObject:@"player " forKey:OpenSDK_LOCALIZATION_PLAYER];
    [localizationInfo setObject:@"guest " forKey:OpenSDK_LOCALIZATION_TMPPLAYER];
    [localizationInfo setObject:@"player %@ ,Welcome!" forKey:OpenSDK_LOCALIZATION_WELCOMETOGAME_PLAYER];
    [localizationInfo setObject:@"guest %@ ,Welcome!" forKey:OpenSDK_LOCALIZATION_WELCOMETOGAME_TMPPLAYER];
    [localizationInfo setObject:@"Please enter your e-mail address" forKey:OpenSDK_LOCALIZATION_EMAILREGISTER];
    [localizationInfo setObject:@"Service Agreement" forKey:OpenSDK_LOCALIZATION_GAMEPROTROL];
    [localizationInfo setObject:@"" forKey:OpenSDK_LOCALIZATION_GAMEPROTROLCONTENT];
    [localizationInfo setObject:@"Current Account：" forKey:OpenSDK_LOCALIZATION_CURACCOUNT];
    [localizationInfo setObject:@"Official Website：" forKey:OpenSDK_LOCALIZATION_WEB];
    [localizationInfo setObject:@"Contact Us：" forKey:OpenSDK_LOCALIZATION_PHONE];
    [localizationInfo setObject:@"Bind The Eamil Account" forKey:OpenSDK_LOCALIZATION_BINDBTN];
    [localizationInfo setObject:@"Switch The Account" forKey:OpenSDK_LOCALIZATION_CHANGEACCOUNTBTN];
    [localizationInfo setObject:@"Countine To Login" forKey:OpenSDK_LOCALIZATION_TMPPLAYERLOGIN];
    [localizationInfo setObject:@"Please accept the game agreement" forKey:OpenSDK_LOCALIZATION_APPECTPROTROL];
    [localizationInfo setObject:@"Register And Bind" forKey:OpenSDK_LOCALIZATION_BINDINAUTOVIEW];
    [localizationInfo setObject:@"Notice" forKey:OpenSDK_LOCALIZATION_WARN2];
    [localizationInfo setObject:@"player cancels to login" forKey:OpenSDK_LOCALIZATION_WARN3];
    [localizationInfo setObject:@"Register And Login" forKey:OpenSDK_LOCALIZATION_SIGNUP_AND_SIGNIN];
    [localizationInfo setObject:@"Please enter your e-mail address and the system will send the e-mail to it.Please open the link in the e-mail and reset the password according to the tips." forKey:OpenSDK_LOCALIZATION_FINDPASSWORD_CONTENT];
    [localizationInfo setObject:@"Please enter the activation code to activate your device" forKey:OpenSDK_LOCALIZATION_ACTIVECODE_TITLE];
    [localizationInfo setObject:@"Activate And Bind" forKey:OpenSDK_LOCALIZATION_BIND_AND_ACTIVE];
    [localizationInfo setObject:@"the activate code can not be empty" forKey:OpenSDK_LOCALIZATION_BIND_AND_ACTIVE_ERROR1];
    [localizationInfo setObject:@"Please enter the activation code of 18 characters" forKey:OpenSDK_LOCALIZATION_BIND_AND_ACTIVE_INPUT_PLACEHOLDER];
    [localizationInfo setObject:@"Send The E-Mail" forKey:OpenSDK_LOCALIZATION_SEND_EMAIL];
    [localizationInfo setObject:@"Please enter the e-mail address" forKey:OpenSDK_LOCALIZATION_FIND_PASSWORD_EMAIL];
    [localizationInfo setObject:@"E-mail is sent. Please get back the password according to the tips" forKey:OpenSDK_LOCALIZATION_FINDPASSWORD_ALERT_MSG];
    [localizationInfo setObject:@"User center servers have no response" forKey:OpenSDK_LOCALIZATION_USERCENTER_ERROR_FROM_SERVER];
    [localizationInfo setObject:@"Select Login Method" forKey:OpenSDK_LOCALIZATION_LOGIN_SELECT];
    [localizationInfo setObject:@"Smartphone Account" forKey:OpenSDK_LOCALIZATION_PHONE_LOGIN];
    [localizationInfo setObject:@"E-mail Account" forKey:OpenSDK_LOCALIZATION_EMAIL_LOGIN];
    [localizationInfo setObject:@"Fast Account" forKey:OpenSDK_LOCALIZATION_TOURIST_LOGIN];
    [localizationInfo setObject:@"Facebook Login" forKey:OpenSDK_LOCALIZATION_FACEBOOK_ACCOUNT_LOGIN];
    [localizationInfo setObject:@"Fb E-mail" forKey:OpenSDK_LOCALIZATION_FACEBOOK_EMAIL_LOGIN];
    [localizationInfo setObject:@"Fb Smartphone" forKey:OpenSDK_LOCALIZATION_FACEBOOK_PHONE_LOGIN];
    [localizationInfo setObject:@"Verification Code Login" forKey:OpenSDK_LOCALIZATION_PHONE_YZM_LOGIN];
    [localizationInfo setObject:@"Password Login" forKey:OpenSDK_LOCALIZATION_PHONE_MM_LOGIN];
    [localizationInfo setObject:@"Please enter your phone number" forKey:OpenSDK_LOCALIZATION_PHONE_NUMBER_INPUT];
    [localizationInfo setObject:@"Please enter verification code" forKey:OpenSDK_LOCALIZATION_PHONE_YZM_INPUT];
    [localizationInfo setObject:@"Send" forKey:OpenSDK_LOCALIZATION_PHONE_YZM_BTN];
    [localizationInfo setObject:@"Change Password" forKey:OpenSDK_LOCALIZATION_PHONE_MODIFY_PASSWORD_BTN];
    [localizationInfo setObject:@"Please enter new password" forKey:OpenSDK_LOCALIZATION_NEW_PASSWORD_INPUT];
    [localizationInfo setObject:@"Reset Password" forKey:OpenSDK_LOCALIZATION_NEW_PASSWORD_BTN];
    [localizationInfo setObject:@"Mobile phone number can't be empty" forKey:OpenSDK_LOCALIZATION_WARN4];
    [localizationInfo setObject:@"Phone number is wrong" forKey:OpenSDK_LOCALIZATION_PHONEERROR];
    [localizationInfo setObject:@"Phone number or verification code can't be empty" forKey:OpenSDK_LOCALIZATION_WARN5];
    [localizationInfo setObject:@"Phone number or password can't be empty" forKey:OpenSDK_LOCALIZATION_WARN6];
    [localizationInfo setObject:@"Phone number can't be Empty" forKey:OpenSDK_LOCALIZATION_WARN7];
    [localizationInfo setObject:@"Frequent operation ,please try again later" forKey:OpenSDK_LOCALIZATION_WARN8];
    [localizationInfo setObject:@"Verification code can't be empty" forKey:OpenSDK_LOCALIZATION_WARN9];
    [localizationInfo setObject:@"Verificatoin code sent" forKey:OpenSDK_LOCALIZATION_WARN10];
    [localizationInfo setObject:@"Password reset, please log in" forKey:OpenSDK_LOCALIZATION_WARN11];
    [localizationInfo setObject:@"Bind Mobile Phone Number" forKey:OpenSDK_LOCALIZATION_BIND_PHONE];
    [localizationInfo setObject:@"Product information is null, please confirm the internet connection and try again later" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN1];
    [localizationInfo setObject:@"ApplePay purchase failed, illegal product ID" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN2];
    [localizationInfo setObject:@"ApplePay purchase failed, in-game purchase is prohibited" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN3];
    [localizationInfo setObject:@"Applepay purchase failed, null" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN4];
    [localizationInfo setObject:@"Appleplay purchase failed,apple server is wrong：SKErrorUnknown" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN5];
    [localizationInfo setObject:@"Appleplay purchase failed,apple server is wrong：SKErrorClientInvalid" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN6];
    [localizationInfo setObject:@"Appleplay purchase failed,apple server is wrong：SKErrorPaymentInvalid" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN7];
    [localizationInfo setObject:@"Appleplay purchase failed,apple server is wrong：SKErrorPaymentNotAllowed" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN8];
    [localizationInfo setObject:@"Purchase failed, unknown error with apple server" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN9];
    [localizationInfo setObject:@"Pay Tips" forKey:OpenSDK_LOCALIZATION_PAY_ALERTTITLE];
    [localizationInfo setObject:@"Visitors can not pay, please go to the user center binding account" forKey:OpenSDK_LOCALIZATION_PAY_ALERTMSG];
    [localizationInfo setObject:@"Bind" forKey:OpenSDK_LOCALIZATION_BIND_USER];
    [localizationInfo setObject:@"Cancel" forKey:OpenSDK_LOCALIZATION_CLOSE];
    [localizationInfo setObject:@"Select Areas" forKey:OpenSDK_LOCALIZATION_COUNTRY_CODE_SELECT];
    
    return self;
}

@end
