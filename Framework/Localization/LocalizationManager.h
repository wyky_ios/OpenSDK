//
//  LocalizationManager.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/30.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalizationManager : NSObject

extern NSString * const OpenSDK_LOCALIZATION_EMAIL;
extern NSString * const OpenSDK_LOCALIZATION_PASSWORD;
extern NSString * const OpenSDK_LOCALIZATION_SIGNIN;
extern NSString * const OpenSDK_LOCALIZATION_SIGNUP;
extern NSString * const OpenSDK_LOCALIZATION_BIND_AND_ACTIVE;
extern NSString * const OpenSDK_LOCALIZATION_BIND_AND_ACTIVE_INPUT_PLACEHOLDER;
extern NSString * const OpenSDK_LOCALIZATION_BIND_AND_ACTIVE_ERROR1;
extern NSString * const OpenSDK_LOCALIZATION_SIGNUP_AND_SIGNIN;
extern NSString * const OpenSDK_LOCALIZATION_QUICKLOGIN;
extern NSString * const OpenSDK_LOCALIZATION_SIGNINWITH;
extern NSString * const OpenSDK_LOCALIZATION_SIGNINERROR;
extern NSString * const OpenSDK_LOCALIZATION_WARN1;
extern NSString * const OpenSDK_LOCALIZATION_CONFIRMPASSWORK;
extern NSString * const OpenSDK_LOCALIZATION_SIGNUPERROR;
extern NSString * const OpenSDK_LOCALIZATION_EMAILERROR;
extern NSString * const OpenSDK_LOCALIZATION_PASSWORDILLEGAL;
extern NSString * const OpenSDK_LOCALIZATION_OK;
extern NSString * const OpenSDK_LOCALIZATION_SEND_EMAIL;
extern NSString * const OpenSDK_LOCALIZATION_FIND_PASSWORD_EMAIL;
extern NSString * const OpenSDK_LOCALIZATION_FINDPASSWORD_ALERT_MSG;
extern NSString * const OpenSDK_LOCALIZATION_USERCENTER_ERROR_FROM_SERVER;
extern NSString * const OpenSDK_LOCALIZATION_FINDPASSWORD;
extern NSString * const OpenSDK_LOCALIZATION_FINDPASSWORD_CONTENT;
extern NSString * const OpenSDK_LOCALIZATION_PASSWORDCONFIRMERROR;
extern NSString * const OpenSDK_LOCALIZATION_SERVERERROR1;
extern NSString * const OpenSDK_LOCALIZATION_SERVERERROR2;
extern NSString * const OpenSDK_LOCALIZATION_NETWORKERROR;
extern NSString * const OpenSDK_LOCALIZATION_ALERTTITLE;
extern NSString * const OpenSDK_LOCALIZATION_ALERTMSG;
extern NSString * const OpenSDK_LOCALIZATION_GOBACK;
extern NSString * const OpenSDK_LOCALIZATION_ACTIVECODE_TITLE;
extern NSString * const OpenSDK_LOCALIZATION_PLAYER;
extern NSString * const OpenSDK_LOCALIZATION_TMPPLAYER;
extern NSString * const OpenSDK_LOCALIZATION_WELCOMETOGAME_PLAYER;
extern NSString * const OpenSDK_LOCALIZATION_WELCOMETOGAME_TMPPLAYER;
extern NSString * const OpenSDK_LOCALIZATION_EMAILREGISTER;
extern NSString * const OpenSDK_LOCALIZATION_GAMEPROTROL;
extern NSString * const OpenSDK_LOCALIZATION_GAMEPROTROLCONTENT;
extern NSString * const OpenSDK_LOCALIZATION_CURACCOUNT;
extern NSString * const OpenSDK_LOCALIZATION_WEB;
extern NSString * const OpenSDK_LOCALIZATION_PAY_ALERTTITLE;
extern NSString * const OpenSDK_LOCALIZATION_PAY_ALERTMSG;
extern NSString * const OpenSDK_LOCALIZATION_BIND_USER;
extern NSString * const OpenSDK_LOCALIZATION_CLOSE;
extern NSString * const OpenSDK_LOCALIZATION_COUNTRY_CODE_SELECT;
extern NSString * const OpenSDK_LOCALIZATION_PHONE;
extern NSString * const OpenSDK_LOCALIZATION_BINDBTN;
extern NSString * const OpenSDK_LOCALIZATION_CHANGEACCOUNTBTN;
extern NSString * const OpenSDK_LOCALIZATION_TMPPLAYERLOGIN;
extern NSString * const OpenSDK_LOCALIZATION_APPECTPROTROL;
extern NSString * const OpenSDK_LOCALIZATION_BINDINAUTOVIEW;
extern NSString * const OpenSDK_LOCALIZATION_WARN2;
extern NSString * const OpenSDK_LOCALIZATION_WARN3;
extern NSString * const OpenSDK_LOCALIZATION_LOGIN_SELECT;
extern NSString * const OpenSDK_LOCALIZATION_PHONE_LOGIN;
extern NSString * const OpenSDK_LOCALIZATION_EMAIL_LOGIN;
extern NSString * const OpenSDK_LOCALIZATION_TOURIST_LOGIN;
extern NSString * const OpenSDK_LOCALIZATION_FACEBOOK_ACCOUNT_LOGIN;
extern NSString * const OpenSDK_LOCALIZATION_FACEBOOK_EMAIL_LOGIN;
extern NSString * const OpenSDK_LOCALIZATION_FACEBOOK_PHONE_LOGIN;
extern NSString * const OpenSDK_LOCALIZATION_PHONE_YZM_LOGIN;
extern NSString * const OpenSDK_LOCALIZATION_PHONE_MM_LOGIN;
extern NSString * const OpenSDK_LOCALIZATION_PHONE_NUMBER_INPUT;
extern NSString * const OpenSDK_LOCALIZATION_PHONE_YZM_INPUT;
extern NSString * const OpenSDK_LOCALIZATION_PHONE_YZM_BTN;
extern NSString * const OpenSDK_LOCALIZATION_PHONE_MODIFY_PASSWORD_BTN;
extern NSString * const OpenSDK_LOCALIZATION_NEW_PASSWORD_INPUT;
extern NSString * const OpenSDK_LOCALIZATION_NEW_PASSWORD_BTN;
extern NSString * const OpenSDK_LOCALIZATION_WARN4;
extern NSString * const OpenSDK_LOCALIZATION_PHONEERROR;
extern NSString * const OpenSDK_LOCALIZATION_WARN5;
extern NSString * const OpenSDK_LOCALIZATION_WARN6;
extern NSString * const OpenSDK_LOCALIZATION_WARN7;
extern NSString * const OpenSDK_LOCALIZATION_WARN8;
extern NSString * const OpenSDK_LOCALIZATION_WARN9;
extern NSString * const OpenSDK_LOCALIZATION_WARN10;
extern NSString * const OpenSDK_LOCALIZATION_WARN11;
extern NSString * const OpenSDK_LOCALIZATION_BIND_PHONE;
extern NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN1;
extern NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN2;
extern NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN3;
extern NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN4;
extern NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN5;
extern NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN6;
extern NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN7;
extern NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN8;
extern NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN9;

+(NSString *) getValueByKey:(NSString *) key;

+(NSString *) getHttpLanHeaderValue;

+(NSString *) getLocalLanguageConfig;

@end
