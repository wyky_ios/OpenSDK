//
//  Localization_Base.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/30.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Localization_Base : NSObject
{
    @protected
    NSMutableDictionary *localizationInfo;
}

-(NSString *) getValueByKey:(NSString *) key;

+(instancetype) getInstance;

@end
