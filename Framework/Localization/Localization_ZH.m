//
//  Localization_ZH.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/30.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import "Localization_ZH.h"
#import "LocalizationManager.h"

@implementation Localization_ZH

-(id) init
{
    self=[super init];
    localizationInfo=[[NSMutableDictionary alloc] init];
    
    [localizationInfo setObject:@"请输入邮箱" forKey:OpenSDK_LOCALIZATION_EMAIL];
    [localizationInfo setObject:@"请输入密码" forKey:OpenSDK_LOCALIZATION_PASSWORD];
    [localizationInfo setObject:@"登录" forKey:OpenSDK_LOCALIZATION_SIGNIN];
    [localizationInfo setObject:@"注册" forKey:OpenSDK_LOCALIZATION_SIGNUP];
    [localizationInfo setObject:@"游客登录" forKey:OpenSDK_LOCALIZATION_QUICKLOGIN];
    [localizationInfo setObject:@"其它登录:" forKey:OpenSDK_LOCALIZATION_SIGNINWITH];
    [localizationInfo setObject:@"登录失败" forKey:OpenSDK_LOCALIZATION_SIGNINERROR];
    [localizationInfo setObject:@"帐号或密码不能为空" forKey:OpenSDK_LOCALIZATION_WARN1];
    [localizationInfo setObject:@"手机号不能为空" forKey:OpenSDK_LOCALIZATION_WARN4];
    [localizationInfo setObject:@"请再次输入密码" forKey:OpenSDK_LOCALIZATION_CONFIRMPASSWORK];
    [localizationInfo setObject:@"注册失败" forKey:OpenSDK_LOCALIZATION_SIGNUPERROR];
    [localizationInfo setObject:@"邮箱格式错误" forKey:OpenSDK_LOCALIZATION_EMAILERROR];
    [localizationInfo setObject:@"忘记密码" forKey:OpenSDK_LOCALIZATION_FINDPASSWORD];
    [localizationInfo setObject:@"密码长度要求大于6个字符，小于20个字符" forKey:OpenSDK_LOCALIZATION_PASSWORDILLEGAL];
    [localizationInfo setObject:@"确认" forKey:OpenSDK_LOCALIZATION_OK];
    [localizationInfo setObject:@"两次输入的密码不一致" forKey:OpenSDK_LOCALIZATION_PASSWORDCONFIRMERROR];
    [localizationInfo setObject:@"服务器返回数据格式错误" forKey:OpenSDK_LOCALIZATION_SERVERERROR1];
    [localizationInfo setObject:@"网络连接超时" forKey:OpenSDK_LOCALIZATION_SERVERERROR2];
    [localizationInfo setObject:@"检测不到网络" forKey:OpenSDK_LOCALIZATION_NETWORKERROR];
    [localizationInfo setObject:@"登录提示" forKey:OpenSDK_LOCALIZATION_ALERTTITLE];
    [localizationInfo setObject:@"游客登录后，请尽快绑定账号，以防游戏记录丢失！" forKey:OpenSDK_LOCALIZATION_ALERTMSG];
    [localizationInfo setObject:@"返回" forKey:OpenSDK_LOCALIZATION_GOBACK];
    [localizationInfo setObject:@"玩家 " forKey:OpenSDK_LOCALIZATION_PLAYER];
    [localizationInfo setObject:@"游客 " forKey:OpenSDK_LOCALIZATION_TMPPLAYER];
    [localizationInfo setObject:@"玩家 %@ ,欢迎进入游戏!" forKey:OpenSDK_LOCALIZATION_WELCOMETOGAME_PLAYER];
    [localizationInfo setObject:@"游客 %@ ,欢迎进入游戏!" forKey:OpenSDK_LOCALIZATION_WELCOMETOGAME_TMPPLAYER];
    [localizationInfo setObject:@"请输入邮箱（推荐QQ邮箱）" forKey:OpenSDK_LOCALIZATION_EMAILREGISTER];
    [localizationInfo setObject:@"游戏服务协议" forKey:OpenSDK_LOCALIZATION_GAMEPROTROL];
    [localizationInfo setObject:@"" forKey:OpenSDK_LOCALIZATION_GAMEPROTROLCONTENT];
    [localizationInfo setObject:@"当前帐号：" forKey:OpenSDK_LOCALIZATION_CURACCOUNT];
    [localizationInfo setObject:@"官网网站：" forKey:OpenSDK_LOCALIZATION_WEB];
    [localizationInfo setObject:@"客服联系方式：" forKey:OpenSDK_LOCALIZATION_PHONE];
    [localizationInfo setObject:@"绑定邮箱帐号" forKey:OpenSDK_LOCALIZATION_BINDBTN];
    [localizationInfo setObject:@"切换帐号" forKey:OpenSDK_LOCALIZATION_CHANGEACCOUNTBTN];
    [localizationInfo setObject:@"继续登录" forKey:OpenSDK_LOCALIZATION_TMPPLAYERLOGIN];
    [localizationInfo setObject:@"请接受游戏协议" forKey:OpenSDK_LOCALIZATION_APPECTPROTROL];
    [localizationInfo setObject:@"注册并绑定" forKey:OpenSDK_LOCALIZATION_BINDINAUTOVIEW];
    [localizationInfo setObject:@"注意" forKey:OpenSDK_LOCALIZATION_WARN2];
    [localizationInfo setObject:@"用户取消登录" forKey:OpenSDK_LOCALIZATION_WARN3];
    [localizationInfo setObject:@"注册并登录" forKey:OpenSDK_LOCALIZATION_SIGNUP_AND_SIGNIN];
    [localizationInfo setObject:@"请输入注册账号的邮箱，系统会发送邮件到该邮箱。\r\n请打开邮件中的链接，按链接的提示进行密码重置。" forKey:OpenSDK_LOCALIZATION_FINDPASSWORD_CONTENT];
    [localizationInfo setObject:@"请输入激活码进行设备激活" forKey:OpenSDK_LOCALIZATION_ACTIVECODE_TITLE];
    [localizationInfo setObject:@"激活并绑定" forKey:OpenSDK_LOCALIZATION_BIND_AND_ACTIVE];
    [localizationInfo setObject:@"激活码不能为空" forKey:OpenSDK_LOCALIZATION_BIND_AND_ACTIVE_ERROR1];
    [localizationInfo setObject:@"请输入18位激活码" forKey:OpenSDK_LOCALIZATION_BIND_AND_ACTIVE_INPUT_PLACEHOLDER];
    [localizationInfo setObject:@"发送邮件" forKey:OpenSDK_LOCALIZATION_SEND_EMAIL];
    [localizationInfo setObject:@"请输入邮箱" forKey:OpenSDK_LOCALIZATION_FIND_PASSWORD_EMAIL];
    [localizationInfo setObject:@"邮件发送成功，请按照邮件提示进行密码找回" forKey:OpenSDK_LOCALIZATION_FINDPASSWORD_ALERT_MSG];
    [localizationInfo setObject:@"用户中心服务器无响应" forKey:OpenSDK_LOCALIZATION_USERCENTER_ERROR_FROM_SERVER];
    [localizationInfo setObject:@"请选择登录方式" forKey:OpenSDK_LOCALIZATION_LOGIN_SELECT];
    [localizationInfo setObject:@"手机账户登录" forKey:OpenSDK_LOCALIZATION_PHONE_LOGIN];
    [localizationInfo setObject:@"邮箱账户登录" forKey:OpenSDK_LOCALIZATION_EMAIL_LOGIN];
    [localizationInfo setObject:@"游客登录" forKey:OpenSDK_LOCALIZATION_TOURIST_LOGIN];
    [localizationInfo setObject:@"Facebook登录" forKey:OpenSDK_LOCALIZATION_FACEBOOK_ACCOUNT_LOGIN];
    [localizationInfo setObject:@"Facebook邮箱登录" forKey:OpenSDK_LOCALIZATION_FACEBOOK_EMAIL_LOGIN];
    [localizationInfo setObject:@"Facebook手机登录" forKey:OpenSDK_LOCALIZATION_FACEBOOK_PHONE_LOGIN];
    [localizationInfo setObject:@"验证码登录" forKey:OpenSDK_LOCALIZATION_PHONE_YZM_LOGIN];
    [localizationInfo setObject:@"密码登录" forKey:OpenSDK_LOCALIZATION_PHONE_MM_LOGIN];
    [localizationInfo setObject:@"请输入手机号码" forKey:OpenSDK_LOCALIZATION_PHONE_NUMBER_INPUT];
    [localizationInfo setObject:@"请输入验证码" forKey:OpenSDK_LOCALIZATION_PHONE_YZM_INPUT];
    [localizationInfo setObject:@"获取验证码" forKey:OpenSDK_LOCALIZATION_PHONE_YZM_BTN];
    [localizationInfo setObject:@"修改密码" forKey:OpenSDK_LOCALIZATION_PHONE_MODIFY_PASSWORD_BTN];
    [localizationInfo setObject:@"请输入新密码" forKey:OpenSDK_LOCALIZATION_NEW_PASSWORD_INPUT];
    [localizationInfo setObject:@"重置密码" forKey:OpenSDK_LOCALIZATION_NEW_PASSWORD_BTN];
    [localizationInfo setObject:@"手机号码格式错误" forKey:OpenSDK_LOCALIZATION_PHONEERROR];
    [localizationInfo setObject:@"手机号码或验证码不能为空" forKey:OpenSDK_LOCALIZATION_WARN5];
    [localizationInfo setObject:@"手机号码或密码不能为空" forKey:OpenSDK_LOCALIZATION_WARN6];
    [localizationInfo setObject:@"手机号码不能为空" forKey:OpenSDK_LOCALIZATION_WARN7];
    [localizationInfo setObject:@"操作频繁，请稍后重试" forKey:OpenSDK_LOCALIZATION_WARN8];
    [localizationInfo setObject:@"验证码不能为空" forKey:OpenSDK_LOCALIZATION_WARN9];
    [localizationInfo setObject:@"发送验证码成功" forKey:OpenSDK_LOCALIZATION_WARN10];
    [localizationInfo setObject:@"重置密码成功，请重新登录" forKey:OpenSDK_LOCALIZATION_WARN11];
    [localizationInfo setObject:@"绑定手机号码" forKey:OpenSDK_LOCALIZATION_BIND_PHONE];
    [localizationInfo setObject:@"iTunes返回的产品信息无效，请确认网络连接正常，稍后重试" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN1];
    [localizationInfo setObject:@"applePay购买失败，非法的productID" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN2];
    [localizationInfo setObject:@"applePay购买失败，系统禁止应用内付费购买" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN3];
    [localizationInfo setObject:@"applePay支付失败 无效的receipt" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN4];
    [localizationInfo setObject:@"applePay购买失败，苹果服务器错误：SKErrorUnknown" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN5];
    [localizationInfo setObject:@"applePay购买失败，苹果服务器错误：SKErrorClientInvalid" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN6];
    [localizationInfo setObject:@"applePay购买失败，苹果服务器错误：SKErrorPaymentInvalid" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN7];
    [localizationInfo setObject:@"applePay购买失败，苹果服务器错误：SKErrorPaymentNotAllowed" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN8];
    [localizationInfo setObject:@"applePay购买失败，苹果服务器未知错误" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN9];
    [localizationInfo setObject:@"支付提示" forKey:OpenSDK_LOCALIZATION_PAY_ALERTTITLE];
    [localizationInfo setObject:@"游客无法支付，请前往用户中心绑定账户" forKey:OpenSDK_LOCALIZATION_PAY_ALERTMSG];
    [localizationInfo setObject:@"绑定" forKey:OpenSDK_LOCALIZATION_BIND_USER];
    [localizationInfo setObject:@"取消" forKey:OpenSDK_LOCALIZATION_CLOSE];
    [localizationInfo setObject:@"选择地区" forKey:OpenSDK_LOCALIZATION_COUNTRY_CODE_SELECT];
    
    return self;
}

@end
