//
//  Localization_ZH.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/30.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import "Localization_ZH_TW.h"
#import "LocalizationManager.h"

@implementation Localization_ZH_TW

-(id) init
{
    self=[super init];
    localizationInfo=[[NSMutableDictionary alloc] init];
    
    [localizationInfo setObject:@"請輸入郵箱" forKey:OpenSDK_LOCALIZATION_EMAIL];
    [localizationInfo setObject:@"請輸入密碼" forKey:OpenSDK_LOCALIZATION_PASSWORD];
    [localizationInfo setObject:@"登錄" forKey:OpenSDK_LOCALIZATION_SIGNIN];
    [localizationInfo setObject:@"註冊" forKey:OpenSDK_LOCALIZATION_SIGNUP];
    [localizationInfo setObject:@"遊客登錄" forKey:OpenSDK_LOCALIZATION_QUICKLOGIN];
    [localizationInfo setObject:@"其他登錄:" forKey:OpenSDK_LOCALIZATION_SIGNINWITH];
    [localizationInfo setObject:@"登錄失敗" forKey:OpenSDK_LOCALIZATION_SIGNINERROR];
    [localizationInfo setObject:@"賬號或密碼不能為空" forKey:OpenSDK_LOCALIZATION_WARN1];
    [localizationInfo setObject:@"請再次輸入密碼" forKey:OpenSDK_LOCALIZATION_CONFIRMPASSWORK];
    [localizationInfo setObject:@"註冊失敗" forKey:OpenSDK_LOCALIZATION_SIGNUPERROR];
    [localizationInfo setObject:@"郵箱格式錯誤" forKey:OpenSDK_LOCALIZATION_EMAILERROR];
    [localizationInfo setObject:@"忘記密碼" forKey:OpenSDK_LOCALIZATION_FINDPASSWORD];
    [localizationInfo setObject:@"密碼長度要求大於6個字符，小於20個字符" forKey:OpenSDK_LOCALIZATION_PASSWORDILLEGAL];
    [localizationInfo setObject:@"確認" forKey:OpenSDK_LOCALIZATION_OK];
    [localizationInfo setObject:@"兩次輸入的密碼不一致" forKey:OpenSDK_LOCALIZATION_PASSWORDCONFIRMERROR];
    [localizationInfo setObject:@"服務器返回數據格式錯誤" forKey:OpenSDK_LOCALIZATION_SERVERERROR1];
    [localizationInfo setObject:@"網絡連接超時" forKey:OpenSDK_LOCALIZATION_SERVERERROR2];
    [localizationInfo setObject:@"檢測不到網絡" forKey:OpenSDK_LOCALIZATION_NETWORKERROR];
    [localizationInfo setObject:@"登錄提示" forKey:OpenSDK_LOCALIZATION_ALERTTITLE];
    [localizationInfo setObject:@"遊客登錄後，請盡快綁定賬號，以防遊戲記錄丟失！" forKey:OpenSDK_LOCALIZATION_ALERTMSG];
    [localizationInfo setObject:@"返回" forKey:OpenSDK_LOCALIZATION_GOBACK];
    [localizationInfo setObject:@"玩家 " forKey:OpenSDK_LOCALIZATION_PLAYER];
    [localizationInfo setObject:@"遊客 " forKey:OpenSDK_LOCALIZATION_TMPPLAYER];
    [localizationInfo setObject:@"玩家 %@ ,歡迎進入遊戲!" forKey:OpenSDK_LOCALIZATION_WELCOMETOGAME_PLAYER];
    [localizationInfo setObject:@"游客 %@ ,歡迎進入遊戲!" forKey:OpenSDK_LOCALIZATION_WELCOMETOGAME_TMPPLAYER];
    [localizationInfo setObject:@"請輸入郵箱（推薦QQ郵箱）" forKey:OpenSDK_LOCALIZATION_EMAILREGISTER];
    [localizationInfo setObject:@"遊戲服務協議" forKey:OpenSDK_LOCALIZATION_GAMEPROTROL];
    [localizationInfo setObject:@"" forKey:OpenSDK_LOCALIZATION_GAMEPROTROLCONTENT];
    [localizationInfo setObject:@"當前帳號：" forKey:OpenSDK_LOCALIZATION_CURACCOUNT];
    [localizationInfo setObject:@"官網網站：" forKey:OpenSDK_LOCALIZATION_WEB];
    [localizationInfo setObject:@"客服聯繫方式：" forKey:OpenSDK_LOCALIZATION_PHONE];
    [localizationInfo setObject:@"綁定郵箱帳號" forKey:OpenSDK_LOCALIZATION_BINDBTN];
    [localizationInfo setObject:@"切換帳號" forKey:OpenSDK_LOCALIZATION_CHANGEACCOUNTBTN];
    [localizationInfo setObject:@"繼續登錄" forKey:OpenSDK_LOCALIZATION_TMPPLAYERLOGIN];
    [localizationInfo setObject:@"請接受遊戲協議" forKey:OpenSDK_LOCALIZATION_APPECTPROTROL];
    [localizationInfo setObject:@"註冊並綁定" forKey:OpenSDK_LOCALIZATION_BINDINAUTOVIEW];
    [localizationInfo setObject:@"注意" forKey:OpenSDK_LOCALIZATION_WARN2];
    [localizationInfo setObject:@"用戶取消登錄" forKey:OpenSDK_LOCALIZATION_WARN3];
    [localizationInfo setObject:@"註冊並登錄" forKey:OpenSDK_LOCALIZATION_SIGNUP_AND_SIGNIN];
    [localizationInfo setObject:@"請輸入註冊賬號的郵箱，系統會發送郵件到該郵箱。\r\n請打開郵件中的鏈接，按鏈接的提示進行密碼重置。" forKey:OpenSDK_LOCALIZATION_FINDPASSWORD_CONTENT];
    [localizationInfo setObject:@"請輸入激活碼進行設備激活" forKey:OpenSDK_LOCALIZATION_ACTIVECODE_TITLE];
    [localizationInfo setObject:@"激活並綁定" forKey:OpenSDK_LOCALIZATION_BIND_AND_ACTIVE];
    [localizationInfo setObject:@"激活碼不能為空" forKey:OpenSDK_LOCALIZATION_BIND_AND_ACTIVE_ERROR1];
    [localizationInfo setObject:@"請輸入18位激活碼" forKey:OpenSDK_LOCALIZATION_BIND_AND_ACTIVE_INPUT_PLACEHOLDER];
    [localizationInfo setObject:@"發送郵件" forKey:OpenSDK_LOCALIZATION_SEND_EMAIL];
    [localizationInfo setObject:@"請輸入郵箱" forKey:OpenSDK_LOCALIZATION_FIND_PASSWORD_EMAIL];
    [localizationInfo setObject:@"郵件發送成功，請按照郵件提示進行密碼找回" forKey:OpenSDK_LOCALIZATION_FINDPASSWORD_ALERT_MSG];
    [localizationInfo setObject:@"用戶中心服務器無響應" forKey:OpenSDK_LOCALIZATION_USERCENTER_ERROR_FROM_SERVER];
    [localizationInfo setObject:@"請選擇登錄方式" forKey:OpenSDK_LOCALIZATION_LOGIN_SELECT];
    [localizationInfo setObject:@"手機賬戶登錄" forKey:OpenSDK_LOCALIZATION_PHONE_LOGIN];
    [localizationInfo setObject:@"郵箱賬戶登錄" forKey:OpenSDK_LOCALIZATION_EMAIL_LOGIN];
    [localizationInfo setObject:@"遊客登錄" forKey:OpenSDK_LOCALIZATION_TOURIST_LOGIN];
    [localizationInfo setObject:@"Facebook登錄" forKey:OpenSDK_LOCALIZATION_FACEBOOK_ACCOUNT_LOGIN];
    [localizationInfo setObject:@"Facebook郵箱登錄" forKey:OpenSDK_LOCALIZATION_FACEBOOK_EMAIL_LOGIN];
    [localizationInfo setObject:@"Facebook手機登錄" forKey:OpenSDK_LOCALIZATION_FACEBOOK_PHONE_LOGIN];
    [localizationInfo setObject:@"驗證碼登錄" forKey:OpenSDK_LOCALIZATION_PHONE_YZM_LOGIN];
    [localizationInfo setObject:@"密碼登錄" forKey:OpenSDK_LOCALIZATION_PHONE_MM_LOGIN];
    [localizationInfo setObject:@"請輸入手機號碼" forKey:OpenSDK_LOCALIZATION_PHONE_NUMBER_INPUT];
    [localizationInfo setObject:@"請輸入驗證碼" forKey:OpenSDK_LOCALIZATION_PHONE_YZM_INPUT];
    [localizationInfo setObject:@"獲取驗證碼" forKey:OpenSDK_LOCALIZATION_PHONE_YZM_BTN];
    [localizationInfo setObject:@"修改密碼" forKey:OpenSDK_LOCALIZATION_PHONE_MODIFY_PASSWORD_BTN];
    [localizationInfo setObject:@"請輸入新密碼" forKey:OpenSDK_LOCALIZATION_NEW_PASSWORD_INPUT];
    [localizationInfo setObject:@"重置密碼" forKey:OpenSDK_LOCALIZATION_NEW_PASSWORD_BTN];
    [localizationInfo setObject:@"手機號不能為空" forKey:OpenSDK_LOCALIZATION_WARN4];
    [localizationInfo setObject:@"手機號碼格式錯誤" forKey:OpenSDK_LOCALIZATION_PHONEERROR];
    [localizationInfo setObject:@"手機號碼或驗證碼不能為空" forKey:OpenSDK_LOCALIZATION_WARN5];
    [localizationInfo setObject:@"手機號碼或密碼不能為空" forKey:OpenSDK_LOCALIZATION_WARN6];
    [localizationInfo setObject:@"手機號碼不能為空" forKey:OpenSDK_LOCALIZATION_WARN7];
    [localizationInfo setObject:@"操作頻繁，請稍後重試" forKey:OpenSDK_LOCALIZATION_WARN8];
    [localizationInfo setObject:@"驗證碼不能為空" forKey:OpenSDK_LOCALIZATION_WARN9];
    [localizationInfo setObject:@"發送驗證碼成功" forKey:OpenSDK_LOCALIZATION_WARN10];
    [localizationInfo setObject:@"重置密碼成功，請重新登錄" forKey:OpenSDK_LOCALIZATION_WARN11];
    [localizationInfo setObject:@"綁定手機號碼" forKey:OpenSDK_LOCALIZATION_BIND_PHONE];
    [localizationInfo setObject:@"iTunes返回的產品信息無效，請確認網絡連接正常，稍後重試" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN1];
    [localizationInfo setObject:@"applePay購買失敗，非法的productID" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN2];
    [localizationInfo setObject:@"applePay購買失敗，系統禁止應用內付費購買" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN3];
    [localizationInfo setObject:@"applePay支付失敗 無效的receipt" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN4];
    [localizationInfo setObject:@"applePay購買失敗，蘋果服務器錯誤：SKErrorUnknown" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN5];
    [localizationInfo setObject:@"applePay購買失敗，蘋果服務器錯誤：SKErrorClientInvalid" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN6];
    [localizationInfo setObject:@"applePay購買失敗，蘋果服務器錯誤：SKErrorPaymentInvalid" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN7];
    [localizationInfo setObject:@"applePay購買失敗，蘋果服務器錯誤：SKErrorPaymentNotAllowed" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN8];
    [localizationInfo setObject:@"applePay購買失敗，蘋果服務器未知錯誤" forKey:OpenSDK_LOCALIZATION_APPLEPAY_WARN9];
    [localizationInfo setObject:@"支付提示" forKey:OpenSDK_LOCALIZATION_PAY_ALERTTITLE];
    [localizationInfo setObject:@"遊客無法支付，請前往用戶中心綁定賬戶" forKey:OpenSDK_LOCALIZATION_PAY_ALERTMSG];
    [localizationInfo setObject:@"綁定" forKey:OpenSDK_LOCALIZATION_BIND_USER];
    [localizationInfo setObject:@"取消" forKey:OpenSDK_LOCALIZATION_CLOSE];
    [localizationInfo setObject:@"選擇地區" forKey:OpenSDK_LOCALIZATION_COUNTRY_CODE_SELECT];
    return self;
}

@end
