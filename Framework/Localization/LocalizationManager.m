//
//  LocalizationManager.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/30.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import "LocalizationManager.h"
#import "Localization_ZH.h"
#import "Localization_EN.h"
#import "Localization_ZH_TW.h"
#import "OpenSDK_DataKeys.h"

@implementation LocalizationManager

NSString * const OpenSDK_LOCALIZATION_EMAIL=@"email";
NSString * const OpenSDK_LOCALIZATION_BIND_AND_ACTIVE=@"bind_and_active";
NSString * const OpenSDK_LOCALIZATION_BIND_AND_ACTIVE_ERROR1=@"bind_and_active_error1";
NSString * const OpenSDK_LOCALIZATION_BIND_AND_ACTIVE_INPUT_PLACEHOLDER=@"bind_and_active_input_placeholder";
NSString * const OpenSDK_LOCALIZATION_PASSWORD=@"password";
NSString * const OpenSDK_LOCALIZATION_SIGNIN=@"signIn";
NSString * const OpenSDK_LOCALIZATION_SIGNUP=@"signUp";
NSString * const OpenSDK_LOCALIZATION_SEND_EMAIL=@"sendEmail";
NSString * const OpenSDK_LOCALIZATION_SIGNUP_AND_SIGNIN=@"signUpAndSignIn";
NSString * const OpenSDK_LOCALIZATION_QUICKLOGIN=@"quickLogin";
NSString * const OpenSDK_LOCALIZATION_FINDPASSWORD=@"findPassword";
NSString * const OpenSDK_LOCALIZATION_SIGNINWITH=@"signInWith";
NSString * const OpenSDK_LOCALIZATION_SIGNINERROR=@"signInError";
NSString * const OpenSDK_LOCALIZATION_FINDPASSWORD_ALERT_MSG=@"findPasswordAlertMsg";
NSString * const OpenSDK_LOCALIZATION_USERCENTER_ERROR_FROM_SERVER=@"userCenterErrorFromServer";
NSString * const OpenSDK_LOCALIZATION_WARN1=@"warn1";
NSString * const OpenSDK_LOCALIZATION_WARN4=@"warn4";
NSString * const OpenSDK_LOCALIZATION_CONFIRMPASSWORK=@"confirmPassword";
NSString * const OpenSDK_LOCALIZATION_SIGNUPERROR=@"signUpError";
NSString * const OpenSDK_LOCALIZATION_EMAILERROR=@"emailError";
NSString * const OpenSDK_LOCALIZATION_PHONEERROR=@"phoneError";
NSString * const OpenSDK_LOCALIZATION_PASSWORDILLEGAL=@"passwordIllegal";
NSString * const OpenSDK_LOCALIZATION_PASSWORDCONFIRMERROR=@"passwordConfirmError";
NSString * const OpenSDK_LOCALIZATION_OK=@"ok";
NSString * const OpenSDK_LOCALIZATION_SERVERERROR1=@"serverError1";
NSString * const OpenSDK_LOCALIZATION_SERVERERROR2=@"serverError2";
NSString * const OpenSDK_LOCALIZATION_NETWORKERROR=@"networkError";
NSString * const OpenSDK_LOCALIZATION_ALERTTITLE=@"alertTiltle";
NSString * const OpenSDK_LOCALIZATION_PAY_ALERTTITLE=@"payAlertTiltle";
NSString * const OpenSDK_LOCALIZATION_ACTIVECODE_TITLE=@"activeCodeTiltle";
NSString * const OpenSDK_LOCALIZATION_GOBACK=@"goback";
NSString * const OpenSDK_LOCALIZATION_ALERTMSG=@"alertMsg";
NSString * const OpenSDK_LOCALIZATION_PAY_ALERTMSG=@"payAlertMsg";
NSString * const OpenSDK_LOCALIZATION_PLAYER=@"player";
NSString * const OpenSDK_LOCALIZATION_TMPPLAYER=@"tmpPlayer";
NSString * const OpenSDK_LOCALIZATION_WELCOMETOGAME_PLAYER=@"welcomeToGamePlayer";
NSString * const OpenSDK_LOCALIZATION_WELCOMETOGAME_TMPPLAYER=@"welcomeToGameTmpPlayer";
NSString * const OpenSDK_LOCALIZATION_EMAILREGISTER=@"emailRegister";
NSString * const OpenSDK_LOCALIZATION_FIND_PASSWORD_EMAIL=@"findPasswordEmail";
NSString * const OpenSDK_LOCALIZATION_FINDPASSWORD_CONTENT=@"findPasswordContent";
NSString * const OpenSDK_LOCALIZATION_GAMEPROTROL=@"gameProtrol";
NSString * const OpenSDK_LOCALIZATION_COUNTRY_CODE_SELECT=@"countryCodeSelect";
NSString * const OpenSDK_LOCALIZATION_GAMEPROTROLCONTENT=@"gameProtrolContent";
NSString * const OpenSDK_LOCALIZATION_CURACCOUNT=@"curAccount";
NSString * const OpenSDK_LOCALIZATION_WEB=@"web";
NSString * const OpenSDK_LOCALIZATION_PHONE=@"phone";
NSString * const OpenSDK_LOCALIZATION_BINDBTN=@"bindBtn";
NSString * const OpenSDK_LOCALIZATION_BIND_USER=@"bindUser";
NSString * const OpenSDK_LOCALIZATION_CLOSE=@"close";
NSString * const OpenSDK_LOCALIZATION_CHANGEACCOUNTBTN=@"changeAccountBtn";
NSString * const OpenSDK_LOCALIZATION_TMPPLAYERLOGIN=@"tmpPlayerLogin";
NSString * const OpenSDK_LOCALIZATION_APPECTPROTROL=@"acceptProtrol";
NSString * const OpenSDK_LOCALIZATION_BINDINAUTOVIEW=@"bindInAutoView";
NSString * const OpenSDK_LOCALIZATION_WARN2=@"warn2";
NSString * const OpenSDK_LOCALIZATION_WARN3=@"warn3";
NSString * const OpenSDK_LOCALIZATION_LOGIN_SELECT=@"loginSelect";
NSString * const OpenSDK_LOCALIZATION_PHONE_LOGIN=@"phoneLogin";
NSString * const OpenSDK_LOCALIZATION_EMAIL_LOGIN=@"emailLogin";
NSString * const OpenSDK_LOCALIZATION_TOURIST_LOGIN=@"touristLogin";
NSString * const OpenSDK_LOCALIZATION_FACEBOOK_ACCOUNT_LOGIN=@"facebookAccountLogin";
NSString * const OpenSDK_LOCALIZATION_FACEBOOK_EMAIL_LOGIN=@"facebookEmailLogin";
NSString * const OpenSDK_LOCALIZATION_FACEBOOK_PHONE_LOGIN=@"facebookPhoneLogin";
NSString * const OpenSDK_LOCALIZATION_PHONE_YZM_LOGIN=@"phoneYZMLogin";
NSString * const OpenSDK_LOCALIZATION_PHONE_MM_LOGIN=@"phoneMMLogin";
NSString * const OpenSDK_LOCALIZATION_PHONE_NUMBER_INPUT=@"phoneNumberInput";
NSString * const OpenSDK_LOCALIZATION_PHONE_YZM_INPUT=@"phoneYZMInput";
NSString * const OpenSDK_LOCALIZATION_PHONE_YZM_BTN=@"phoneYZMBtn";
NSString * const OpenSDK_LOCALIZATION_PHONE_MODIFY_PASSWORD_BTN=@"modifyPasswordBtn";
NSString * const OpenSDK_LOCALIZATION_NEW_PASSWORD_INPUT=@"newPasswordInput";
NSString * const OpenSDK_LOCALIZATION_NEW_PASSWORD_BTN=@"newPasswordBtn";
NSString * const OpenSDK_LOCALIZATION_WARN5=@"warn5";
NSString * const OpenSDK_LOCALIZATION_WARN6=@"warn6";
NSString * const OpenSDK_LOCALIZATION_WARN7=@"warn7";
NSString * const OpenSDK_LOCALIZATION_WARN8=@"warn8";
NSString * const OpenSDK_LOCALIZATION_WARN9=@"warn9";
NSString * const OpenSDK_LOCALIZATION_WARN10=@"warn10";
NSString * const OpenSDK_LOCALIZATION_WARN11=@"warn11";
NSString * const OpenSDK_LOCALIZATION_BIND_PHONE=@"bindPhone";
NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN1=@"applePayWarn1";
NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN2=@"applePayWarn2";
NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN3=@"applePayWarn3";
NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN4=@"applePayWarn4";
NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN5=@"applePayWarn5";
NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN6=@"applePayWarn6";
NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN7=@"applePayWarn7";
NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN8=@"applePayWarn8";
NSString * const OpenSDK_LOCALIZATION_APPLEPAY_WARN9=@"applePayWarn9";

+(NSString *) getValueByKey:(NSString *)key
{
//    if([[LocalizationManager getLocalLanguageConfig] isEqualToString:OpenSDK_L_LANGUAGE_ZHHANS]) {
//        return [[Localization_ZH getInstance] getValueByKey:key];
//    } else if([[LocalizationManager getLocalLanguageConfig] isEqualToString:OpenSDK_L_LANGUAGE_ENUS]) {
//        return [[Localization_EN getInstance] getValueByKey:key];
//    } else if([[LocalizationManager getLocalLanguageConfig] isEqualToString:OpenSDK_L_LANGUAGE_ZHHANS_TW]) {
//        return [[Localization_ZH_TW getInstance] getValueByKey:key];
//    } else {
//        return [[Localization_ZH getInstance] getValueByKey:key];
//    }
    
    /// 根据语言强制获取table名
    NSString *tableName;
    NSString *language = [LocalizationManager getLocalLanguageConfig];
    if ([language isEqualToString:OpenSDK_L_LANGUAGE_ZHHANS]) {
        tableName = @"zh-Hans";
    } else if ([language isEqualToString:OpenSDK_L_LANGUAGE_ENUS]) {
        tableName = @"en";
    } else if ([language isEqualToString:OpenSDK_L_LANGUAGE_ZHHANS_TW]) {
        tableName = @"zh-Hant";
    } else {
        tableName = @"zh-Hans";
    }
    
    /// 获取国际化文件Bundle
    NSArray *bundlePaths = [[NSBundle bundleForClass:[LocalizationManager class]] pathsForResourcesOfType:@"bundle" inDirectory:nil];
    NSBundle *localizationBundle;
    for (NSString *path in bundlePaths) {
        if ([path hasSuffix:@"Localization.bundle"]) {
            localizationBundle = [NSBundle bundleWithPath:path];
        }
    }
    /// 找不到Bundle，返回key
    if (localizationBundle == nil) {
        return key;
    }
    NSString *lprojPath = [localizationBundle pathForResource:tableName ofType:@"lproj"];
    NSBundle *lprojBundle = [NSBundle bundleWithPath:lprojPath];
    /// 根据key从Bundle中获取对应值
    NSString *result = [lprojBundle localizedStringForKey:key value:nil table:@"OpenSDK_Localization"];
    return result;
}

+(NSString *) getHttpLanHeaderValue
{
    if([[LocalizationManager getLocalLanguageConfig] isEqualToString:OpenSDK_L_LANGUAGE_ZHHANS]) {
        return @"zh-CN";
    } else if([[LocalizationManager getLocalLanguageConfig] isEqualToString:OpenSDK_L_LANGUAGE_ENUS]) {
        return @"en-US";
    } else if([[LocalizationManager getLocalLanguageConfig] isEqualToString:OpenSDK_L_LANGUAGE_ZHHANS_TW]) {
        return @"zh-TW";
    } else {
        return @"zh-CN";
    }
}

//根据本地配置返回语言设置信息
+(NSString *) getLocalLanguageConfig
{
    NSDictionary *localData = [[NSBundle mainBundle] infoDictionary];
    NSDictionary *rootConfig=[localData objectForKey:OpenSDK_CONFIG];
    NSString *languageConfig=[rootConfig objectForKey:OpenSDK_L_LANGUAGE_CONFIG];
    
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *currentLanguage = [languages objectAtIndex:0];
    if(languageConfig==nil)
    {
        //如果没有配置就从系统获取
        return currentLanguage;
    }
    
    if([languageConfig isEqualToString:OpenSDK_L_LANGUAGE_AUTO])
    {
        return currentLanguage;
    }
    
    //设定为汉语-简体
    if ([languageConfig isEqualToString:OpenSDK_L_LANGUAGE_ZHHANS]) {
        return OpenSDK_L_LANGUAGE_ZHHANS;
    }
    
    //设定为汉语-繁体
    if ([languageConfig isEqualToString:OpenSDK_L_LANGUAGE_ZHHANS_TW]) {
        return OpenSDK_L_LANGUAGE_ZHHANS_TW;
    }
    
    //设定为英语
    if ([languageConfig isEqualToString:OpenSDK_L_LANGUAGE_ENUS]) {
        return OpenSDK_L_LANGUAGE_ENUS;
    }
    
    return currentLanguage;
}

@end
