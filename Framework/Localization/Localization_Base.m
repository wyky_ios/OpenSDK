//
//  Localization_Base.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/30.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import "Localization_Base.h"

@implementation Localization_Base

static Localization_Base *instance = nil;

+(instancetype) getInstance
{
    if(instance==nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            instance = [[self alloc] init];
        });
    }
    return instance;
}

-(NSString *) getValueByKey:(NSString *) key
{
    return [localizationInfo objectForKey:key];
}

@end
