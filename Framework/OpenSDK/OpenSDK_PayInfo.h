//
//  OpenSDK_PayInfo.h
//  
//
//  Created by wyht－ios－dev on 16/3/2.
//
//

#import <Foundation/Foundation.h>

@interface OpenSDK_PayInfo : NSObject

@property (nonatomic, strong, readwrite) NSString *productID;
@property (nonatomic, strong, readwrite) NSString *orderAmount;
@property (nonatomic, strong, readwrite) NSString *productName;
@property (nonatomic, strong, readwrite) NSString *appData;
@property (nonatomic, strong, readwrite) NSString *currency;
@property (nonatomic, strong, readwrite) NSString *notifyUrl;
@property (nonatomic, strong, readwrite) NSString *uid;
@property (nonatomic, strong, readwrite) NSString *platform;
@property (nonatomic, strong, readwrite) NSString *payVersion;

//交易类型(1:SDK 2:WEB)
@property (nonatomic, strong, readwrite) NSString *tradeType;

@end
