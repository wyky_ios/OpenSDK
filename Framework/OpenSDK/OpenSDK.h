//
//  OpenSDK.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/2/20.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_UserInfo.h"
#import "OpenSDK_PayInfo.h"
#import "OpenSDK_OrderInfo.h"
#import "OpenSDK_Enum.h"
#import "OPKGameRoleModel.h"

//! Project version number for OpenSDK.
FOUNDATION_EXPORT double OpenSDKVersionNumber;

//! Project version string for OpenSDK.
FOUNDATION_EXPORT const unsigned char OpenSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OpenSDK/PublicHeader.h>

/**
 * OpenSDK回调
 */
@protocol OpenSDK_Delegate <NSObject>

@required

// 初始化回调
- (void) openSDKInitWithCode:(OpenSDK_InitAction) code
                      errStr:(NSString *) errStr;

// 登录回调
-(void) openSDKLoginWithCode:(OpenSDK_UserAction) code
                    userInfo:(OpenSDK_UserInfo *) userInfo
                      errStr:(NSString *) errStr;

// 支付回调
-(void) openSDKPayWithCode:(OpenSDK_PayAction) code
                 orderInfo:(OpenSDK_OrderInfo *) orderInfo
                    errStr:(NSString *) errStr;


@optional

// OpenSDK数据回调
-(void) openSDKStatisticsWithTpye:(NSString *) type
                         dataInfo:(NSDictionary *) dataInfo;

// OpenSDK 主动切换账号
-(void) openSDKSwitchAccountWithMessage:(NSString *)msg;

// OpenSDK 主动退出登录
-(void) openSDKLoginOutWithMessage:(NSString *)msg;

@end


/**
 * OpenSDK接口
 */
@interface OpenSDK : NSObject

@property (nonatomic, weak) id<OpenSDK_Delegate> delegate;

@property (nonatomic, strong, readonly)OPKGameRoleModel *currentGameRole;

+(instancetype) getInstance;

// 初始化(应用启动时调用一次)
-(void) initWithDelegate:(id<OpenSDK_Delegate>) delegate;

// 登录
-(void) login;

// 登出
-(void) logout;

// 是否已经登录
-(BOOL) IsLogin;

// 切换账号
-(void) switchAccount;

// 用户中心
- (void)userCenter;

// 进入游戏
- (void)enteredGame:(NSDictionary *)data;

// 支付
-(void) payWithPayInfo:(OpenSDK_PayInfo *) payInfo;

// ios生命周期回调
+(void) applicationDidBecomeActive:(UIApplication *)application;

// ios生命周期回调
+(void) applicationWillEnterForeground:(UIApplication *)application;

// ios生命周期回调
+(void) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

// ios生命周期回调
+(void) application:(id)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

// ios生命周期回调
+(void) application:(id)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options;

// ios生命周期回调
+(void) application:(UIApplication *)application handleOpenURL:(NSURL *)url;

// ios生命周期回调
+ (void)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity;

//  iOS生命周期
+ (void)applicationWillTerminate:(UIApplication *)application;
@end
