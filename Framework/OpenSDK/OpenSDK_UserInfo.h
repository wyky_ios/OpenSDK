//
//  OpenSDK_UserInfo.h
//  用于OpenSDK_Delegate的回调的用户数据结构。
//
//  Created by wyht－ios－dev on 16/3/2.
//
//

#import <Foundation/Foundation.h>

@interface OpenSDK_UserInfo : NSObject

@property (nonatomic, strong, readwrite) NSString *openSDK_token;
@property (nonatomic, strong, readwrite) NSString *openSDK_openID;
@property (nonatomic, strong, readwrite) NSString *openSDK_nickName;

@end
