//
//  SSDKDataKeys.m
//  SuperSDK
//
//  Created by wyht－ios－dev on 15/10/7.
//  Copyright © 2015年 WYHT. All rights reserved.
//

#import "OpenSDK_DataKeys.h"

@implementation OpenSDK_DataKeys

NSString * const OpenSDK_JSON_DATA = @"jsonData";
NSString * const OpenSDK_TIME = @"time";
NSString * const OpenSDK_STATUS = @"status";
NSString * const OpenSDK_DATA = @"data";
NSString * const OpenSDK_EXTRA = @"extra";
NSString * const OpenSDK_COMMON = @"common";
NSString * const OpenSDK_APP_ID = @"app_id";
NSString * const OpenSDK_USER_NAME = @"name";
NSString * const OpenSDK_USER_PASSWD = @"password";
NSString * const OpenSDK_UID = @"id";
NSString * const OpenSDK_TOKEN = @"token";
NSString * const OpenSDK_SIGN = @"sign";
NSString * const OpenSDK_ERROR = @"error";
NSString * const OpenSDK_ERROR_NO = @"error_no";
NSString * const OpenSDK_ERROR_MSG = @"error_msg";

NSString * const OpenSDK_CONFIG=@"OpenSDKConfig";
NSString * const OpenSDK_DEBUG_URL=@"OpenSDK_Debug_Url";
NSString * const OpenSDK_LOGO=@"OpenSDK_Logo";
NSString * const OpenSDK_CHANNELID=@"ChannelID";
NSString * const OpenSDK_JAPP_KEY=@"JAppKey";
NSString * const OpenSDK_PUBKEY=@"OpenSDK_PubKey";
NSString * const OpenSDK_CURRENCY=@"OpenSDK_Currency";
NSString * const OpenSDK_PAYBACKURL=@"OpenSDK_PayBackUrl";
NSString * const OpenSDK_APPID=@"OpenSDK_AppID";
NSString * const OpenSDK_APPSFLYER_KEYS = @"OpenSDK_AppsFlyer_Key";
NSString * const OpenSDK_CONFIG_PHONE=@"OpenSDK_Phone";
NSString * const OpenSDK_CONFIG_WEB=@"OpenSDK_Web";
NSString * const OpenSDK_UNIVERSAL_LINKS=@"OpenSDK_Universal_Links";
NSString * const OpenSDK_L_GOOGLESDK=@"Google";
NSString * const OpenSDK_L_VIEWCONTROLLER=@"viewController";
NSString * const OpenSDK_L_FACEBOOKSDK=@"FacebookAccount";
NSString * const OpenSDK_L_FACEBOOK_TYPE=@"FacebookType";
NSString * const OpenSDK_L_FACEBOOK_ACCOUNT=@"Facebook";
NSString * const OpenSDK_L_FACEBOOK_PHONE=@"Phone";
NSString * const OpenSDK_L_FACEBOOK_EMAIL=@"Email";
NSString * const OpenSDK_L_TWITTERSDK=@"Twitter";
NSString * const OpenSDK_L_USERTOKEN=@"OpenSDK_userToken";
NSString * const OpenSDK_L_OPENID=@"OpenSDK_openID";
NSString * const OpenSDK_L_AUTOLOGINFLAG=@"OpenSDK_autoLoginFlag";
NSString * const OpenSDK_L_KEYCHAIN=@"OpenSDK_KeychainRecord";
NSString * const OpenSDK_L_KEYCHAIN_KEY=@"OpenSDK_Keychain_uuid";
NSString * const OpenSDK_L_TEMP_USERID=@"OpenSDK_Temp_UserID";
NSString * const OpenSDK_L_USERNAME=@"OpenSDK_userName";
NSString * const OpenSDK_L_USERTYPE=@"OpenSDK_userType";
NSString * const OpenSDK_L_LANGUAGE_CONFIG=@"SupportLanguage";
NSString * const OpenSDK_L_LANGUAGE_ZHHANS=@"zh-Hans";
NSString * const OpenSDK_L_LANGUAGE_ENUS=@"en-US";
NSString * const OpenSDK_L_LANGUAGE_ZHHANS_TW=@"zh-TW";
NSString * const OpenSDK_L_LANGUAGE_AUTO=@"auto";
NSString * const OpenSDK_L_COUNTRY_CODE=@"OpenSDK_Local_Country_Code";

NSString * const OpenSDK_S_FACEBOOKSDK=@"Facebook";
NSString * const OpenSDK_S_FACEBOOKSDK_ACCOUNTKIT=@"FacebookAccountKit";
NSString * const OpenSDK_S_SIGN=@"sign";
NSString * const OpenSDK_S_OAUTH_APP=@"oauth_app";
NSString * const OpenSDK_S_USERNAME=@"username";
NSString * const OpenSDK_S_PASSWORD=@"password";
NSString * const OpenSDK_S_NEW_PASSWORD=@"new_password";
NSString * const OpenSDK_S_TARGET=@"target";
NSString * const OpenSDK_S_MOBILE_ACCOUNT=@"mobile";
NSString * const OpenSDK_S_MOBILE_RANDOM_PASSWORD_ID=@"target_id";
NSString * const OpenSDK_S_SECURITY=@"security";
NSString * const OpenSDK_S_EMAIL=@"email";
NSString * const OpenSDK_S_CHANNEL=@"channel";
NSString * const OpenSDK_S_OAUTH_TYPE=@"oauth_type";
NSString * const OpenSDK_S_TEMP_ID=@"temp_id";
NSString * const OpenSDK_S_OPEN_ID=@"open_id";
NSString * const OpenSDK_S_PLATFORM_ID=@"platform_id";
NSString * const OpenSDK_S_IS_REAL=@"is_real";
NSString * const OpenSDK_S_NEED_REAL=@"need_real";
NSString * const OpenSDK_S_IS_MOBILE=@"is_mobile";
NSString * const OpenSDK_S_NEED_MOBILE=@"need_mobile";
NSString * const OpenSDK_S_REAL_NAME=@"real_name";
NSString * const OpenSDK_S_CARD_TYPE=@"card_type";
NSString * const OpenSDK_S_CARD_NUMBER=@"card_number";
NSString * const OpenSDK_S_TOKEN=@"token";
NSString * const OpenSDK_S_ACTIVE_BIND_TYPE=@"bind_type";
NSString * const OpenSDK_S_ACTIVE_BIND_ID=@"bind_id";
NSString * const OpenSDK_S_ACTIVE_AFFECT=@"affect";
NSString * const OpenSDK_S_ACTIVE_CODE=@"code";
NSString * const OpenSDK_S_ACCOUNT=@"Account";
NSString * const OpenSDK_S_INTERVAL=@"interval";
NSString * const OpenSDK_S_MOBILE=@"Mobile";
NSString * const OpenSDK_S_DYNAMIC=@"Dynamic";
NSString * const OpenSDK_S_ONE_KEY=@"OneKey";
NSString * const OpenSDK_S_DEVICE=@"Device";
NSString * const OpenSDK_S_TEMPORARY=@"Temporary";
NSString * const OpenSDK_S_DEVICE_TYPE=@"device_type";
NSString * const OpenSDK_S_DEVICE_ID=@"device_id";
NSString * const OpenSDK_S_MY_DEVICE=@"iOS";
NSString * const OpenSDK_S_STATUS=@"status";
NSString * const OpenSDK_S_ERROR_NO=@"error_no";
NSString * const OpenSDK_S_ERROR_MSG=@"error_msg";
NSString * const OpenSDK_S_PLATFORM_VERSION=@"platform_version";
NSString * const OpenSDK_S_ACCESS_TOKEN=@"access_token";
NSString * const OpenSDK_S_USER=@"user";
NSString * const OpenSDK_S_EXTRA=@"extra";
NSString * const OpenSDK_S_NICKNAME=@"nickname";
NSString * const OpenSDK_S_TOKEN_INFO=@"token_info";
NSString * const OpenSDK_S_OPEN_INFO=@"open_info";
NSString * const OpenSDK_S_INIT_MAP=@"init_map";
NSString * const OpenSDK_S_INIT_MAP_AFFECT_AUTH=@"code_affect_auth";
NSString * const OpenSDK_S_FORBID_GUEST_PAY=@"pay_realname_switch";// 禁止游客支付的key
NSString * const OpenSDK_S_SUPPORT_PAY_PLATFORM=@"ios_pay_platform";// 服务器配置的支付方式key(ios专用配置)
NSString * const OpenSDK_S_SUPPORT_LOGIN_TYPE=@"android_auth_type";// 服务器配置的登录方式key(和android共用一个配置)
NSString * const OpenSDK_S_IOS_UI = @"ios_ui";
NSString * const OpenSDK_S_AUTH_TYPE=@"auth_type";//登录方式列表key
NSString * const OpenSDK_S_SUPPORT_REGISTER_TYPE=@"android_register_type";// 服务器配置的注册方式key(和android共用一个配置)
NSString * const OpenSDK_S_REGISTER_TYPE=@"register_type";//注册方式列表key
NSString * const OpenSDK_S_IMG_VERIFICATION=@"verification_client_code";//滑动解锁图片开关
NSString * const OpenSDK_S_VER_AUDIT=@"ios_version_audit";
NSString * const OpenSDK_S_SHOW_CONTRACT = @"show_contract";    // 是否展示用户协议
NSString * const OpenSDK_S_CONTRACT_CHECKED = @"contract_checked";  // 用户协议选择状态
NSString * const OpenSDK_S_ONEKEY_SWITCH = @"onekey_server_switch";  // 一键登录状态
NSString * const OpenSDK_S_SECRET_KEY = @"secret_key";  // 验签key
NSString * const OpenSDK_S_PLATFORM=@"platform";
NSString * const OpenSDK_S_INIT_MAP_SWITCH=@"switch";
NSString * const OpenSDK_S_INIT_MAP_ROTE=@"rote";
NSString * const OpenSDK_S_INIT_MAP_CONTRACT_URL = @"contract_url";
NSString * const OpenSDK_S_INIT_MAP_PRIVACY_URL = @"privacy_url";
NSString * const OpenSDK_S_INIT_MAP_AFFECT=@"affect";
NSString * const OpenSDK_S_PLATFORM_UID=@"platform_uid";
NSString * const OpenSDK_S_PRODUCTID=@"product_id";
NSString * const OpenSDK_S_APPDATA=@"app_data";
NSString * const OpenSDK_S_PRODUCT_NAME=@"product_name";
NSString * const OpenSDK_S_PRODUCT_AMOUNT=@"product_amount";
NSString * const OpenSDK_S_GRANT_APP=@"grant_app";
NSString * const OpenSDK_S_UID=@"uid";
NSString * const OpenSDK_S_PAYMENT=@"payment";
NSString * const OpenSDK_S_ID=@"id";
NSString * const OpenSDK_S_DEVICEID=@"device_id";
NSString * const OpenSDK_S_PAY_STATUS=@"pay_status";
NSString * const OpenSDK_S_PAY_AMOUNT=@"pay_amount";
NSString * const OpenSDK_S_TRADE_TYPE=@"trade_type";
NSString * const OpenSDK_S_CURRENCY=@"currency";
NSString * const OpenSDK_S_PAY_NOTIFYURL=@"notify_url";
NSString * const OpenSDK_S_PAYTIME=@"pay_time";
NSString * const OpenSDK_S_USER_INFO=@"user_info";
NSString * const OpenSDK_S_PROTROL=@"considerations_message";
NSString * const OpenSDK_S_PRIACY=@"privacypolicy_message";
NSString * const OpenSDK_S_ISDEBUG=@"is_debug";
NSString * const OpenSDK_S_SMS_MOBILE_LOGIN=@"mobile_login";
NSString * const OpenSDK_S_SMS_MOBILE_REGISTER=@"mobile_register";
NSString * const OpenSDK_S_SMS_MOBILE_BIND=@"mobile_binding";
NSString * const OpenSDK_S_USERCENTER_CLIENT_SWITCH=@"usercenter_client_switch";
NSString * const OpenSDK_S_CENTER_URL=@"center_url";
NSString * const OpenSDK_S_COUNTRY_NAME=@"name";
NSString * const OpenSDK_S_COUNTRY_CODE=@"code";
NSString * const OpenSDK_S_COUNTRY_NUMBER=@"number";
NSString * const OpenSDK_S_COUNTRY_INFO=@"country";
NSString * const OpenSDK_S_PAY_TOAST=@"client_toast_switch";

NSString * const OpenSDK_E_LOGIN=@"OpenSDK_E_LOGIN";
NSString * const OpenSDK_E_PAY=@"OpenSDK_E_PAY";
NSString * const OpenSDK_E_APPLICATION=@"OpenSDK_E_APPLICATION";
NSString * const OpenSDK_E_OPENURL=@"OpenSDK_E_OPENURL";
NSString * const OpenSDK_E_USERACTIVITY=@"OpenSDK_E_USERACTIVITY";
NSString * const OpenSDK_E_SOURCEAPPLICATION=@"OpenSDK_E_SOURCEAPPLICATION";
NSString * const OpenSDK_E_NOTIFICATIONSETTINGS=@"OpenSDK_E_NOTIFICATIONSETTINGS";
NSString * const OpenSDK_E_ANNOTATION=@"OpenSDK_E_ANNOTATION";
NSString * const OpenSDK_E_HANDLEOPENURL=@"OpenSDK_E_HANDLEOPENURL";
NSString * const OpenSDK_E_DIDFINISHLAUNCHWITHOPTIONS=@"OpenSDK_E_DIDFINISHLAUNCHWITHOPTIONS";
NSString * const OpenSDK_E_OPTIONS=@"OpenSDK_E_OPTIONS";

@end

