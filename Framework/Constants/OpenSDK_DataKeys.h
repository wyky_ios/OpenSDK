//
//  SSDKDataKeys.h
//  SuperSDK
//
//  Created by wyht－ios－dev on 15/10/7.
//  Copyright © 2015年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenSDK_DataKeys : NSObject

extern NSString * const OpenSDK_JSON_DATA;
extern NSString * const OpenSDK_TIME;;
extern NSString * const OpenSDK_STATUS;
extern NSString * const OpenSDK_DATA;
extern NSString * const OpenSDK_EXTRA;
extern NSString * const OpenSDK_COMMON;
extern NSString * const OpenSDK_APP_ID;
extern NSString * const OpenSDK_USER_NAME;
extern NSString * const OpenSDK_USER_PASSWD;
extern NSString * const OpenSDK_UID;
extern NSString * const OpenSDK_TOKEN;
extern NSString * const OpenSDK_SIGN;
extern NSString * const OpenSDK_ERROR;
extern NSString * const OpenSDK_ERROR_NO;
extern NSString * const OpenSDK_ERROR_MSG;
extern NSString * const OpenSDK_FACEBOOK_APPID;
extern NSString * const OpenSDK_FACEBOOK_NAME;

//本地配置键值
extern NSString * const OpenSDK_CONFIG;
extern NSString * const OpenSDK_DEBUG_URL;//OpenSDKDebug地址
extern NSString * const OpenSDK_LOGO;
extern NSString * const OpenSDK_CONFIG_PHONE;
extern NSString * const OpenSDK_CONFIG_WEB;
extern NSString * const OpenSDK_CHANNELID;
extern NSString * const OpenSDK_PUBKEY;
extern NSString * const OpenSDK_APPID;
extern NSString * const OpenSDK_APPSFLYER_KEYS;
extern NSString * const OpenSDK_CURRENCY;
extern NSString * const OpenSDK_PAYBACKURL;
extern NSString * const OpenSDK_S_IOS_UI;
extern NSString * const OpenSDK_JAPP_KEY;

extern NSString * const OpenSDK_UNIVERSAL_LINKS;    // 通用链接
extern NSString * const OpenSDK_L_GOOGLESDK;
extern NSString * const OpenSDK_L_FACEBOOKSDK;
extern NSString * const OpenSDK_L_TWITTERSDK;
extern NSString * const OpenSDK_L_USERTOKEN;
extern NSString * const OpenSDK_L_OPENID;
extern NSString * const OpenSDK_L_KEYCHAIN;
extern NSString * const OpenSDK_L_KEYCHAIN_KEY;
extern NSString * const OpenSDK_L_VIEWCONTROLLER;
extern NSString * const OpenSDK_L_USERNAME;
extern NSString * const OpenSDK_L_USERTYPE;//true 帐号，false 游客
extern NSString * const OpenSDK_L_LANGUAGE_CONFIG;
extern NSString * const OpenSDK_L_LANGUAGE_ZHHANS;
extern NSString * const OpenSDK_L_LANGUAGE_AUTO;
extern NSString * const OpenSDK_L_TEMP_USERID;
extern NSString * const OpenSDK_L_LANGUAGE_ENUS;
extern NSString * const OpenSDK_L_FACEBOOK_TYPE;
extern NSString * const OpenSDK_L_FACEBOOK_ACCOUNT;
extern NSString * const OpenSDK_L_FACEBOOK_PHONE;
extern NSString * const OpenSDK_L_FACEBOOK_EMAIL;
extern NSString * const OpenSDK_L_LANGUAGE_ZHHANS_TW;
extern NSString * const OpenSDK_L_COUNTRY_CODE;
extern NSString * const OpenSDK_L_AUTOLOGINFLAG;



//服务器配置的关键字
extern NSString * const OpenSDK_S_FACEBOOKSDK;
extern NSString * const OpenSDK_S_FACEBOOKSDK_ACCOUNTKIT;
extern NSString * const OpenSDK_S_SIGN;
extern NSString * const OpenSDK_S_OAUTH_APP;
extern NSString * const OpenSDK_S_USERNAME;
extern NSString * const OpenSDK_S_PASSWORD;
extern NSString * const OpenSDK_S_EMAIL;
extern NSString * const OpenSDK_S_CHANNEL;
extern NSString * const OpenSDK_S_OAUTH_TYPE;
extern NSString * const OpenSDK_S_OPEN_ID;
extern NSString * const OpenSDK_S_PLATFORM_ID;
extern NSString * const OpenSDK_S_TOKEN;
extern NSString * const OpenSDK_S_ACCOUNT;
extern NSString * const OpenSDK_S_ONE_KEY;
extern NSString * const OpenSDK_S_EXTRA;
extern NSString * const OpenSDK_S_DEVICE;
extern NSString * const OpenSDK_S_DEVICE_TYPE;
extern NSString * const OpenSDK_S_DEVICE_ID;
extern NSString * const OpenSDK_S_MY_DEVICE;
extern NSString * const OpenSDK_S_STATUS;
extern NSString * const OpenSDK_S_INIT_MAP;
extern NSString * const OpenSDK_S_INIT_MAP_ROTE;
extern NSString * const OpenSDK_S_SUPPORT_PAY_PLATFORM;
extern NSString * const OpenSDK_S_SUPPORT_LOGIN_TYPE;// 服务器配置的登录方式key(和android共用一个配置)
extern NSString * const OpenSDK_S_AUTH_TYPE;
extern NSString * const OpenSDK_S_SUPPORT_REGISTER_TYPE;// 服务器配置的注册方式key(和android共用一个配置)
extern NSString * const OpenSDK_S_REGISTER_TYPE;//注册方式列表key
extern NSString * const OpenSDK_S_IMG_VERIFICATION;//滑动解锁图片开关
extern NSString * const OpenSDK_S_VER_AUDIT;
extern NSString * const OpenSDK_S_SHOW_CONTRACT;    // 是否展示用户协议
extern NSString * const OpenSDK_S_CONTRACT_CHECKED;  // 用户协议选择状态
extern NSString * const OpenSDK_S_ONEKEY_SWITCH;  // 一键登录状态
extern NSString * const OpenSDK_S_SECRET_KEY;  // 验签key
extern NSString * const OpenSDK_S_ERROR_NO;
extern NSString * const OpenSDK_S_ERROR_MSG;
extern NSString * const OpenSDK_S_INIT_MAP_SWITCH;
extern NSString * const OpenSDK_S_INIT_MAP_AFFECT_AUTH;
extern NSString * const OpenSDK_S_INIT_MAP_AFFECT;
extern NSString * const OpenSDK_S_FORBID_GUEST_PAY;
extern NSString * const OpenSDK_S_PLATFORM_VERSION;
extern NSString * const OpenSDK_S_ACCESS_TOKEN;
extern NSString * const OpenSDK_S_USER;
extern NSString * const OpenSDK_S_TOKEN_INFO;
extern NSString * const OpenSDK_S_OPEN_INFO;
extern NSString * const OpenSDK_S_USER_INFO;
extern NSString * const OpenSDK_S_PLATFORM_UID;
extern NSString * const OpenSDK_S_PRODUCTID;
extern NSString * const OpenSDK_S_PRODUCT_NAME;
extern NSString * const OpenSDK_S_NICKNAME;
extern NSString * const OpenSDK_S_PRODUCT_AMOUNT;
extern NSString * const OpenSDK_S_GRANT_APP;
extern NSString * const OpenSDK_S_UID;
extern NSString * const OpenSDK_S_PAYMENT;
extern NSString * const OpenSDK_S_ID;
extern NSString * const OpenSDK_S_DEVICEID;
extern NSString * const OpenSDK_S_PAY_STATUS;
extern NSString * const OpenSDK_S_PAY_AMOUNT;
extern NSString * const OpenSDK_S_TRADE_TYPE;
extern NSString * const OpenSDK_S_CURRENCY;
extern NSString * const OpenSDK_S_PAY_NOTIFYURL;
extern NSString * const OpenSDK_S_PLATFORM;
extern NSString * const OpenSDK_S_PAYTIME;
extern NSString * const OpenSDK_S_PROTROL;
extern NSString * const OpenSDK_S_PRIACY;
extern NSString * const OpenSDK_S_APPDATA;
extern NSString * const OpenSDK_S_TEMPORARY;
extern NSString * const OpenSDK_S_TEMP_ID;
extern NSString * const OpenSDK_S_ISDEBUG;
extern NSString * const OpenSDK_S_ACTIVE_BIND_TYPE;
extern NSString * const OpenSDK_S_ACTIVE_BIND_ID;
extern NSString * const OpenSDK_S_ACTIVE_AFFECT;
extern NSString * const OpenSDK_S_ACTIVE_CODE;
extern NSString * const OpenSDK_S_MOBILE;
extern NSString * const OpenSDK_S_DYNAMIC;
extern NSString * const OpenSDK_S_MOBILE_ACCOUNT;
extern NSString * const OpenSDK_S_SECURITY;
extern NSString * const OpenSDK_S_MOBILE_RANDOM_PASSWORD_ID;
extern NSString * const OpenSDK_S_TARGET;
extern NSString * const OpenSDK_S_SMS_MOBILE_LOGIN;
extern NSString * const OpenSDK_S_SMS_MOBILE_REGISTER;
extern NSString * const OpenSDK_S_SMS_MOBILE_BIND;
extern NSString * const OpenSDK_S_USERCENTER_CLIENT_SWITCH;
extern NSString * const OpenSDK_S_INIT_MAP_CONTRACT_URL;
extern NSString * const OpenSDK_S_INIT_MAP_PRIVACY_URL;
extern NSString * const OpenSDK_S_CENTER_URL;
extern NSString * const OpenSDK_S_INTERVAL;
extern NSString * const OpenSDK_S_NEW_PASSWORD;
extern NSString * const OpenSDK_S_IS_REAL;
extern NSString * const OpenSDK_S_NEED_REAL;
extern NSString * const OpenSDK_S_IS_MOBILE;
extern NSString * const OpenSDK_S_NEED_MOBILE;
extern NSString * const OpenSDK_S_REAL_NAME;
extern NSString * const OpenSDK_S_CARD_TYPE;
extern NSString * const OpenSDK_S_CARD_NUMBER;
extern NSString * const OpenSDK_S_COUNTRY_NAME;
extern NSString * const OpenSDK_S_COUNTRY_CODE;
extern NSString * const OpenSDK_S_COUNTRY_NUMBER;
extern NSString * const OpenSDK_S_COUNTRY_INFO;
extern NSString * const OpenSDK_S_PAY_TOAST;

//事件
extern NSString * const OpenSDK_E_LOGIN;
extern NSString * const OpenSDK_E_PAY;
extern NSString * const OpenSDK_E_APPLICATION;
extern NSString * const OpenSDK_E_OPENURL;
extern NSString * const OpenSDK_E_SOURCEAPPLICATION;
extern NSString * const OpenSDK_E_NOTIFICATIONSETTINGS;
extern NSString * const OpenSDK_E_ANNOTATION;
extern NSString * const OpenSDK_E_HANDLEOPENURL;
extern NSString * const OpenSDK_E_DIDFINISHLAUNCHWITHOPTIONS;
extern NSString * const OpenSDK_E_OPTIONS;
extern NSString * const OpenSDK_E_USERACTIVITY;

@end

