//
//  OpenSDK_InitResData.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/8/17.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_BaseResData.h"

@interface OpenSDK_InitResData : OpenSDK_BaseResData

-(instancetype) initWithData:(NSMutableDictionary *)dictionary;

//获取登陆和注册是否需要激活码的标志
-(BOOL) getLoginAndRegisterActiveCodeFlag;

//获取激活码的affect内容
-(NSString *) getActiveAffectData;

//获取禁止支付的开关
-(BOOL) getForbidGuestPayFlag;

//获取服务器配置的登录方式
-(NSArray*) getSupportLoginTypes;

//获取服务器配置的注册方式
-(NSArray*) getSupportRegisterTypes;

//获取支持的xyz类型
-(NSArray*) getSupportXyzTypes;

//获取服务器存储的国家编码
-(NSArray *) getCountryCodeFromServer;

//获取图片验证码地址
-(NSString *) getImgVerificationUrl;

// UI风格 "0": "跟随系统"，"1": "Light"，"2": "Dark"，
- (NSString *) getUIStyle;

//获取显示用户协议的开关
-(BOOL) getShowContract;

//获取用户协议勾选状态开关
-(BOOL) getContractChecked;

// 获取用户中心地址
- (NSString *)getUserCenterURL;

// 获取用户中心地址
- (NSString *)getContractWebURL;

// 获取用户中心地址
- (NSString *)getPrivacyWebURL;

// 一键登录开关
- (BOOL)getOneKeySwitch;

// 一键登录比例
- (NSInteger)getOneKeyRote;

/// 验签key
- (NSString *) getSecretKey;

// true 审核， false 非审核
- (BOOL) getAppStoreReviewStatus;

// true 不弹， false 弹
- (BOOL) isPayToastEnable;

@end

