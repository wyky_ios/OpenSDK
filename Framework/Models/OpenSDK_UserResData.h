//
//  OpenSDK_UserResData.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/19.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_BaseResData.h"

@interface OpenSDK_UserResData : OpenSDK_BaseResData

-(instancetype) initWithData:(NSMutableDictionary *)dictionary;

-(NSString *) getUserID;

-(NSString *) getOpenID;

-(NSString *) getToken;

-(NSString *) getNickName;

-(NSString *) getUser_id;

-(NSString *) getMobile;

-(BOOL) isReal;

-(int) needReal;

- (BOOL) isMobile;

- (int) needMobile;

- (NSString *)platform;

@end
