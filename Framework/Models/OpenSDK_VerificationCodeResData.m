//
//  OpenSDK_VerificationCodeResData.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 17/3/21.
//  Copyright © 2017年 WYHT. All rights reserved.
//

#import "OpenSDK_VerificationCodeResData.h"
#import "OpenSDK_DataKeys.h"

@implementation OpenSDK_VerificationCodeResData

-(long) getVerificationCodeValidTime {
    if ([super content]!=nil) {
        if([[super content] objectForKey:OpenSDK_S_INTERVAL]!=nil) {
            return [[[super content] objectForKey:OpenSDK_S_INTERVAL] longValue];
        } else {
            return 0;
        }
    }
    return 0;
}

@end
