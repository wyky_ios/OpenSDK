//
//  OpenSDK_PayResData.m
//  
//
//  Created by wyht－ios－dev on 16/3/22.
//
//

#import "OpenSDK_PayResData.h"
#import "OpenSDK_DataKeys.h"

@interface OpenSDK_PayResData()
@property(nonatomic, strong)NSMutableDictionary *payment;
@end

@implementation OpenSDK_PayResData

-(instancetype) initWithData:(NSMutableDictionary *)dictionary
{
    self = [super initWithDictionary:dictionary];
    
    _payment=[[self content] objectForKey:OpenSDK_S_PAYMENT];
    
    return self;
}

-(NSString *) getOrderID
{
    if(_payment==nil)
        return nil;
    return [_payment objectForKey:OpenSDK_S_ID];
}

-(NSString *) getPayStatus
{
    if(_payment==nil)
        return nil;
    return [_payment objectForKey:OpenSDK_S_PAY_STATUS];
}

-(NSString *) getUID
{
    if(_payment==nil)
        return nil;
    return [_payment objectForKey:OpenSDK_S_UID];
}

-(NSString *) getProduct
{
    if(_payment==nil)
        return nil;
    NSString *product = [_payment objectForKey:OpenSDK_S_PRODUCTID];
    if (product == nil) {
        product = [_payment objectForKey:@"product"];
    }
    return product;
}

- (NSString *)getProductName {
   if(_payment==nil)
        return nil;
    return [_payment objectForKey:@"product"];
}

-(NSString *) getAppData {
    if(_payment==nil)
        return nil;
    return [_payment objectForKey:OpenSDK_S_APPDATA];
}

-(NSString *) getPayAmount
{
    if(_payment==nil)
        return nil;
    return [_payment objectForKey:OpenSDK_S_PAY_AMOUNT];
}

-(NSString *) getProductAmount
{
    if(_payment==nil)
        return nil;
    return [_payment objectForKey:OpenSDK_S_PRODUCT_AMOUNT];
}

-(NSString *) getCurrency
{
    if(_payment==nil)
        return nil;
    return [_payment objectForKey:OpenSDK_S_CURRENCY];
}

-(long) getPayTime
{
    if(_payment==nil)
        return 0;
    return [[_payment objectForKey:OpenSDK_S_PAYTIME] longValue];
}

-(OpenSDK_OrderInfo *) toUserOrderInfo
{
    OpenSDK_OrderInfo *orderInfo=[[OpenSDK_OrderInfo alloc] initWithOrderID:[self getOrderID] currency:[self getCurrency] productAmount:[self getProductAmount] product:[self getProductName] payStatus:[self getPayStatus] appData:[self getAppData] payTime:[self getPayTime]];
    
    return orderInfo;
}

@end
