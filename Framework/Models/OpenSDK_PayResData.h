//
//  OpenSDK_PayResData.h
//  
//
//  Created by wyht－ios－dev on 16/3/22.
//
//

#import <Foundation/Foundation.h>
#import "OpenSDK_BaseResData.h"
#import "OpenSDK_OrderInfo.h"

@interface OpenSDK_PayResData : OpenSDK_BaseResData

-(instancetype) initWithData:(NSMutableDictionary *)dictionary;

-(NSString *) getOrderID;

-(NSString *) getPayStatus;

-(NSString *) getUID;

-(NSString *) getProduct;

-(NSString *) getProductAmount;

-(NSString *) getPayAmount;

-(NSString *) getCurrency;

-(NSString *) getAppData;

-(long) getPayTime;

- (NSString *)getProductName;

//转换为反馈给用户的订单信息
-(OpenSDK_OrderInfo *) toUserOrderInfo;

@end


