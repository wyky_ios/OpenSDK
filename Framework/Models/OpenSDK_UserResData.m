//
//  OpenSDK_UserResData.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/19.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import "OpenSDK_UserResData.h"
#import "OpenSDK_DataKeys.h"

@implementation OpenSDK_UserResData
{
    NSMutableDictionary *openInfo;
    NSMutableDictionary *userInfo;
    NSMutableDictionary *extraInfo;
    BOOL isReal;
    int needReal;
    BOOL isMobile;
    int needMobile;
}

- (instancetype) initWithData:(NSMutableDictionary *)dictionary {
    
    self = [super initWithDictionary:dictionary];
    
    openInfo=[[[self content] objectForKey:OpenSDK_S_USER] objectForKey:OpenSDK_S_OPEN_INFO];
    userInfo=[[[self content] objectForKey:OpenSDK_S_USER] objectForKey:OpenSDK_S_USER_INFO];
    extraInfo=[[[self content] objectForKey:OpenSDK_S_USER] objectForKey:OpenSDK_S_EXTRA];
    
    if ([self.content objectForKey:OpenSDK_S_IS_REAL] != nil) {
        isReal=[[[self content] objectForKey:OpenSDK_S_IS_REAL] boolValue];
    }
    if ([self.content objectForKey:OpenSDK_S_NEED_REAL] != nil) {
        //0：关闭，1:开启，2:强制开
        needReal=[[[self content] objectForKey:OpenSDK_S_NEED_REAL] intValue];
    }
    
    if ([self.content objectForKey:OpenSDK_S_IS_MOBILE] != nil) {
        isMobile =[[[self content] objectForKey:OpenSDK_S_IS_MOBILE] boolValue];
    }
    if ([self.content objectForKey:OpenSDK_S_NEED_MOBILE] != nil) {
        //0：关闭，1:开启，2:强制开
        needMobile=[[[self content] objectForKey:OpenSDK_S_NEED_MOBILE] intValue];
    }
    
    
    return self;
}

-(NSString *) getUserID
{
    if(userInfo==nil)
        return @"";
    return [userInfo objectForKey:OpenSDK_S_ID];
}

- (NSString *)getUser_id {
    if(userInfo==nil)
        return @"";
    return [userInfo objectForKey:@"user_id"];
}

- (NSString *)getMobile {
    if(userInfo==nil)
        return @"";
    return [userInfo objectForKey:@"mobile"];
}

-(NSString *) getOpenID
{
    if(openInfo==nil)
        return @"";
    return [openInfo objectForKey:OpenSDK_S_OPEN_ID];
}

-(NSString *) getToken
{
    if(openInfo==nil)
        return @"";
    return [openInfo objectForKey:OpenSDK_S_TOKEN];
}


-(NSString *) getNickName {
    if(extraInfo==nil)
        return @"";
    NSString *mobile = [self getMobile];
    if (mobile.length > 0){
        return  mobile;
    }else{
        return [extraInfo objectForKey:OpenSDK_S_NICKNAME];
    }
}

-(BOOL) isReal {
    return isReal;
}

-(int) needReal {
    return needReal;
}

-(BOOL) isMobile {
    return isMobile;
}

-(int) needMobile {
    return needMobile;
}

- (NSString *)platform {
    if (extraInfo == nil) {
        return @"";
    }
    return [extraInfo objectForKey:@"platform"];;
}

@end
