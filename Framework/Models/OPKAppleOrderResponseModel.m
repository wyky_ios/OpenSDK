//
//  OPKAppleOrderResultModel.m
//  Pods-Unity-iPhone
//
//  Created by ycgame on 2020/5/26.
//

#import "OPKAppleOrderResponseModel.h"


@implementation OPKAppleOrderResponseModel

-(instancetype) initWithData:(NSMutableDictionary *)dictionary
{
    if (self = [super initWithDictionary:dictionary]) {
        NSMutableArray *successPayments = [[NSMutableArray alloc] initWithCapacity:0];
        NSMutableArray *failPayments = [[NSMutableArray alloc] initWithCapacity:0];
        NSMutableArray *undeterminedPayments = [[NSMutableArray alloc] initWithCapacity:0];
        NSMutableArray *reusedPayments = [[NSMutableArray alloc] initWithCapacity:0];
        NSMutableArray *expiredPayments = [[NSMutableArray alloc] initWithCapacity:0];
        
        NSDictionary *payment = [[self content] objectForKey:@"payment"];
        NSArray *successDataPayments = [payment objectForKey:@"successPaymentResult"];
        if ([successDataPayments isKindOfClass:[NSArray class]]) {
            for (NSDictionary *dic in successDataPayments) {
                NSMutableDictionary *content = [dic mutableCopy];
                content[@"status"] = dictionary[@"status"];
                OPKAppleOrderResultModel *resData = [[OPKAppleOrderResultModel alloc] initWithData:content];
                [successPayments addObject:resData];
            }
        }
        
        NSArray *failDataPayments = [payment objectForKey:@"failPaymentResult"];
        if ([failDataPayments isKindOfClass:[NSArray class]]) {
            for (NSDictionary *dic in failDataPayments) {
                NSMutableDictionary *content = [dic mutableCopy];
                content[@"status"] = dictionary[@"status"];
                OPKAppleOrderResultModel *resData = [[OPKAppleOrderResultModel alloc] initWithData:content];
                [failPayments addObject:resData];
            }
        }
        
        NSArray *undeterminedDataPayments = [payment objectForKey:@"undeterminedResult"];
        if ([undeterminedDataPayments isKindOfClass:[NSArray class]]) {
            for (NSDictionary *dic in undeterminedDataPayments) {
                NSMutableDictionary *content = [dic mutableCopy];
                content[@"status"] = dictionary[@"status"];
                OPKAppleOrderResultModel *resData = [[OPKAppleOrderResultModel alloc] initWithData:content];
                [undeterminedPayments addObject:resData];
            }
        }
        
        NSArray *reusedDataPayments = [payment objectForKey:@"repeatPaymentResult"];
        if ([reusedDataPayments isKindOfClass:[NSArray class]]) {
            for (NSDictionary *dic in reusedDataPayments) {
                NSMutableDictionary *content = [dic mutableCopy];
                content[@"status"] = dictionary[@"status"];
                OPKAppleOrderResultModel *resData = [[OPKAppleOrderResultModel alloc] initWithData:content];
                [reusedPayments addObject:resData];
            }
        }
        
        NSArray *expiredDataPayments = [payment objectForKey:@"expirePaymentResult"];
        if ([expiredDataPayments isKindOfClass:[NSArray class]]) {
            for (NSDictionary *dic in expiredDataPayments) {
                NSMutableDictionary *content = [dic mutableCopy];
                content[@"status"] = dictionary[@"status"];
                OPKAppleOrderResultModel *resData = [[OPKAppleOrderResultModel alloc] initWithData:content];
                [expiredPayments addObject:resData];
            }
        }
        
        _purchasedTransactions = successPayments;
        _failedTransactions = failPayments;
        _undeterminedTransactions = undeterminedPayments;
        _reusedTransactions = reusedPayments;
        _expiredTransactions = expiredPayments;
    }
    return self;
}

@end


@implementation OPKAppleOrderResultModel

-(instancetype) initWithData:(NSMutableDictionary *)dictionary {
    if (self = [super initWithDictionary:dictionary]) {
        self.payInfo = [[OpenSDK_PayResData alloc] initWithData:dictionary];
        self.transactionId = dictionary[@"transactionId"];
    }
    return self;
}

@end
