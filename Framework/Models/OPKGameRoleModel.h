//
//  OPKGameAccount.h
//  YCOpenSDK
//
//  Created by mqz on 2021/6/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OPKGameRoleModel : NSObject
@property (nonatomic, strong)NSString *roleId;
@property (nonatomic, strong)NSString *roleName;
@property (nonatomic, strong)NSString *roleBalance;
@property (nonatomic, strong)NSString *roleLevel;
@property (nonatomic, strong)NSString *zoneId;
@property (nonatomic, strong)NSString *zoneName;
@end

NS_ASSUME_NONNULL_END

/**
 
 {
     complete = true;
     ext = login;
     partyName = "\U65a7\U5934\U5e2e";
     professionName = "Li Xingyun";
     roleBalance = 1000;
     roleId = 147369;
     roleLevel = 3;
     roleName = testRoleName;
     roleVip = 5;
     "role_create_time" = 1624526418;
     sceneId = 666;
     "tutorial_id" = 1;
     userAge = "-1";
     userGender = 0;
     userType = unknown;
     zoneId = 996;
     zoneName = testZoneName;
 }
 
 [extraData setObject:[userInfo objectForKey:SLocalRoleID] forKey:@"role_id"];
 [extraData setObject:[userInfo objectForKey:SLocalRoleName] forKey:@"role_name"];
 [extraData setObject:[userInfo objectForKey:SLocalRoleGrade] forKey:@"role_grade"];
 [extraData setObject:[userInfo objectForKey:SLocalRoleBalance] forKey:@"role_balance"];
 [extraData setObject:[userInfo objectForKey:SLocalAppData] forKey:@"app_data"];
 [extraData setObject:[userInfo objectForKey:SLocalGameUID] forKey:@"game_uid"];
 [extraData setObject:[userInfo objectForKey:SLocalServerID] forKey:@"server_id"];
 
 */
