//
//  SSDKBaseResData.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 15/10/7.
//  Copyright © 2015年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenSDK_BaseResData : NSObject

@property (nonatomic, strong, readwrite) NSString *status;
@property (nonatomic, readwrite) long errorNo;
@property (nonatomic, strong, readwrite) NSString *errorMsg;
@property (nonatomic, strong, readwrite) NSMutableDictionary *content;

- (instancetype) initWithDictionary : (NSMutableDictionary *) dictionary;

- (BOOL) isOk;

-(void) fail;

@end
