//
//  OpenSDK_VerificationCodeResData.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 17/3/21.
//  Copyright © 2017年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_BaseResData.h"

@interface OpenSDK_VerificationCodeResData : OpenSDK_BaseResData

//获取验证码的有效时长
-(long) getVerificationCodeValidTime;

@end
