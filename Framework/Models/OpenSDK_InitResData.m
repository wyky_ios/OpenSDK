//
//  OpenSDK_InitResData.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/8/17.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import "OpenSDK_InitResData.h"
#import "OpenSDK_DataKeys.h"

@implementation OpenSDK_InitResData

NSMutableDictionary *initData=nil;
NSMutableArray *countryData=nil;
NSString *secret_key=nil;
BOOL isPayToastEnable=false;

-(instancetype) initWithData:(NSMutableDictionary *)dictionary
{
    self = [super initWithDictionary:dictionary];
    secret_key = [[self content] objectForKey:OpenSDK_S_SECRET_KEY] ?: @"1q2w3e";
    initData=[[self content] objectForKey:OpenSDK_S_INIT_MAP];
    countryData=[[self content] objectForKey:OpenSDK_S_COUNTRY_INFO];
    isPayToastEnable = [[[initData objectForKey:OpenSDK_S_PAY_TOAST] objectForKey:OpenSDK_S_INIT_MAP_SWITCH] boolValue];
    return self;
}

//获取登陆和注册是否需要激活码的标志
-(BOOL) getLoginAndRegisterActiveCodeFlag {
    if(initData==nil || [initData count]==0)
        return NO;
    return [[[initData objectForKey:OpenSDK_S_INIT_MAP_AFFECT_AUTH] objectForKey:OpenSDK_S_INIT_MAP_SWITCH] boolValue];
}

//获取激活码的affect内容
-(NSString *) getActiveAffectData {
    if(initData==nil || [initData count]==0)
        return @"";
    return [[initData objectForKey:OpenSDK_S_INIT_MAP_AFFECT_AUTH] objectForKey:OpenSDK_S_INIT_MAP_AFFECT]==nil?@"":[[initData objectForKey:OpenSDK_S_INIT_MAP_AFFECT_AUTH] objectForKey:OpenSDK_S_INIT_MAP_AFFECT];
}

//获取禁止支付的开关
-(BOOL) getForbidGuestPayFlag {
    if(initData==nil || [initData count]==0)
        return NO;
    return [[[initData objectForKey:OpenSDK_S_FORBID_GUEST_PAY] objectForKey:OpenSDK_S_INIT_MAP_SWITCH] boolValue];
}

//获取显示用户协议的开关
-(BOOL) getShowContract {
    if(initData==nil || [initData count]==0)
        return NO;
    return [[[initData objectForKey:OpenSDK_S_SHOW_CONTRACT] objectForKey:OpenSDK_S_INIT_MAP_SWITCH] boolValue];
}

//获取用户协议勾选状态开关
-(BOOL) getContractChecked {
    if(initData==nil || [initData count]==0)
        return NO;
    return [[[initData objectForKey:OpenSDK_S_CONTRACT_CHECKED] objectForKey:OpenSDK_S_INIT_MAP_SWITCH] boolValue];
}

- (NSString *)getUserCenterURL {
    if(initData==nil || [initData count]==0) {
        return @"";
    }
    return [[initData objectForKey:OpenSDK_S_USERCENTER_CLIENT_SWITCH] objectForKey:OpenSDK_S_CENTER_URL];
}

- (NSString *)getContractWebURL {
    if(initData==nil || [initData count]==0){
        return @"";
    }
    NSString *url = [[initData objectForKey:OpenSDK_S_SHOW_CONTRACT] objectForKey:OpenSDK_S_INIT_MAP_CONTRACT_URL];
    return url;
}


- (NSString *)getPrivacyWebURL {
    if(initData==nil || [initData count]==0) {
        return @"";
    }
    NSString *url = [[initData objectForKey:OpenSDK_S_SHOW_CONTRACT] objectForKey:OpenSDK_S_INIT_MAP_PRIVACY_URL];
    return url;
}

- (BOOL)getOneKeySwitch {
    if(initData==nil || [initData count]==0)
        return NO;
    return [[[initData objectForKey:OpenSDK_S_ONEKEY_SWITCH] objectForKey:OpenSDK_S_INIT_MAP_SWITCH] boolValue];
}

- (NSInteger)getOneKeyRote {
    if(initData==nil || [initData count]==0)
        return 0;
    return [[[initData objectForKey:OpenSDK_S_ONEKEY_SWITCH] objectForKey:OpenSDK_S_INIT_MAP_ROTE] integerValue];
}

//获取服务器配置的登录方式
-(NSArray*)  getSupportLoginTypes {
    if(initData==nil || [initData count]==0) {
        return nil;
    }
    NSDictionary *dic =[initData objectForKey:OpenSDK_S_SUPPORT_LOGIN_TYPE];
    if (dic == nil) {
        return nil;
    }
    // 如果开关关闭，返回nil
    if (![@"true" isEqualToString:[dic objectForKey:OpenSDK_S_INIT_MAP_SWITCH]]) {
        return nil;
    }
    // 返回登录方式
    return [[initData objectForKey:OpenSDK_S_SUPPORT_LOGIN_TYPE] objectForKey:OpenSDK_S_AUTH_TYPE];
}

- (NSString *)getUIStyle {
    if(initData==nil || [initData count]==0) {
        return nil;
    }
    NSDictionary *dic =[initData objectForKey:OpenSDK_S_IOS_UI];
    if (dic == nil) {
        return nil;
    }
    
    NSString *style = [dic objectForKey:@"style"];
    return style;
}

//获取服务器配置的注册方式
-(NSArray*) getSupportRegisterTypes {
    if(initData==nil || [initData count]==0) {
        return nil;
    }
    NSDictionary *dic =[initData objectForKey:OpenSDK_S_SUPPORT_REGISTER_TYPE];
    if (dic == nil) {
        return nil;
    }
    // 如果开关关闭，返回nil
    if (![@"true" isEqualToString:[dic objectForKey:OpenSDK_S_INIT_MAP_SWITCH]]) {
        return nil;
    }
    return [[initData objectForKey:OpenSDK_S_SUPPORT_REGISTER_TYPE] objectForKey:OpenSDK_S_REGISTER_TYPE];
}

//获取服务器配置的支付方式
-(NSArray*) getSupportXyzTypes {
    //默认是ApplePay
    if(initData==nil || [initData count]==0) {
        return @[@"ApplePay"];
    }
    NSDictionary *dic =[initData objectForKey:OpenSDK_S_SUPPORT_PAY_PLATFORM];
    if (dic == nil) {
        return @[@"ApplePay"];
    }
    // 如果开关关闭，返回默认支付方式
    if (![@"true" isEqualToString:[dic objectForKey:OpenSDK_S_INIT_MAP_SWITCH]]) {
        return @[@"ApplePay"];
    }
    return [[initData objectForKey:OpenSDK_S_SUPPORT_PAY_PLATFORM] objectForKey:OpenSDK_S_PLATFORM];
}

//获取服务器存储的国家编码
-(NSArray *) getCountryCodeFromServer {
    return countryData;
}


//获取图片验证码地址
-(NSString *) getImgVerificationUrl {
    if(initData==nil || [initData count]==0)
        return nil;
    if (![initData objectForKey:OpenSDK_S_IMG_VERIFICATION]) {
        return nil;
    }
    // 如果开关关闭，返回nil
    if (![@"true" isEqualToString:[[initData objectForKey:OpenSDK_S_IMG_VERIFICATION] objectForKey:OpenSDK_S_INIT_MAP_SWITCH]]) {
        return nil;
    }
    return [[initData objectForKey:OpenSDK_S_IMG_VERIFICATION] objectForKey:@"picture_url"];
}

/// 验签key
- (NSString *) getSecretKey{
    return secret_key;
}

// 
- (BOOL) getAppStoreReviewStatus{
    if(initData==nil || [initData count]==0)
        return true;
    NSDictionary *auditDict = [initData objectForKey:OpenSDK_S_VER_AUDIT];
    if (auditDict == nil) {
        return true;
    }
    NSArray *versions = [auditDict objectForKey:@"version"];
    id isSwitch = [auditDict objectForKey:@"switch"];
    if (versions.count == 0 || versions == nil || isSwitch == nil){
        return true;
    }
    NSString *localVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    if ([versions containsObject:localVersion] && [isSwitch boolValue]){
        return true;
    }
    return false;
}

- (BOOL) isPayToastEnable{
    return isPayToastEnable;
}

@end

