//
//  OpenSDK_AccountRecord.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/18.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>

//NS_ENUM，定义状态等普通枚举
typedef NS_ENUM(NSUInteger, OpenSDKAccountType) {
    OpenSDKAccountTypeDynamic,
    OpenSDKAccountTypeMobile,
    OpenSDKAccountTypeMail,
    OpenSDKAccountTypeGuest,
    OpenSDKAccountTypeOther// 第三方登录账号
};

@interface OpenSDK_AccountRecord : NSObject <NSCoding>

@property (nonatomic, strong, readwrite) NSString *accountName;
@property (nonatomic, strong, readwrite) NSString *accountValue;
@property (nonatomic, readwrite) OpenSDKAccountType accountType;
@property (nonatomic, strong) NSString *platform;
@property (nonatomic, strong, readwrite) NSString *platformName;//第三方sdk平台名
@property (nonatomic, readwrite) NSTimeInterval loginTime;
@property (nonatomic, strong, readwrite) NSString *openId;
@property (nonatomic, strong, readwrite) NSString *token;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, readwrite) BOOL isReal;
@property (nonatomic, readwrite) int needReal;// 0:关闭,1:开启,2:强制开
@property (nonatomic, assign) BOOL isMobile;
@property (nonatomic, assign) int needMobile;   // 0:关闭,1:开启,2:强制开

+(instancetype) initWithMobile:(NSString *)mobile type:(OpenSDKAccountType)type openId:(NSString *)openId token:(NSString *)token userId:(NSString *)userId isReal:(BOOL)isReal needReal:(int)needReal isMobile:(BOOL)isMobile needMobile:(int)needMobile;

+(instancetype) initWithMail:(NSString *)mail openId:(NSString *)openId token:(NSString *)token userId:(NSString *)userId mobile:(NSString *)mobile isReal:(BOOL)isReal needReal:(int)needReal isMobile:(BOOL)isMobile needMobile:(int)needMobile;

+(instancetype) initWithGuest:(NSString *)tempId openId:(NSString *)openId token:(NSString *)token userId:(NSString *)userId mobile:(NSString *)mobile isReal:(BOOL)isReal needReal:(int)needReal isMobile:(BOOL)isMobile needMobile:(int)needMobile;

+ (instancetype)initWithOtherSDK:(NSString *)nickName platform:(NSString *)platform platformName:(NSString *)platformName platformUid:(NSString *)platformUid openId:(NSString *)openId token:(NSString *)token userId:(NSString *)userId mobile:(NSString *)mobile isReal:(BOOL)isReal needReal:(int)needReal isMobile:(BOOL)isMobile needMobile:(int)needMobile;

@end
