//
//  OPKAppleOrderResultModel.h
//  Pods-Unity-iPhone
//
//  Created by ycgame on 2020/5/26.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_BaseResData.h"
#import "OpenSDK_PayResData.h"

@class OPKAppleOrderResultModel;

NS_ASSUME_NONNULL_BEGIN

/// 苹果订单验证返回数据
@interface OPKAppleOrderResponseModel : OpenSDK_BaseResData

-(instancetype) initWithData:(NSMutableDictionary *)dictionary;

/// 成功订单id列表
@property(nonatomic, strong)NSArray <OPKAppleOrderResultModel *>*purchasedTransactions;
/// 失败订单id列表
@property(nonatomic, strong)NSArray <OPKAppleOrderResultModel *>*failedTransactions;
/// 待校订单id列表
@property(nonatomic, strong)NSArray <OPKAppleOrderResultModel *>*undeterminedTransactions;

/// 重复使用订单列表
@property(nonatomic, strong)NSArray <OPKAppleOrderResultModel *>*reusedTransactions;
/// 已过期订单列表
@property(nonatomic, strong)NSArray <OPKAppleOrderResultModel *>*expiredTransactions;
@end

/// 苹果订单验证返回数据
@interface OPKAppleOrderResultModel : OpenSDK_BaseResData

-(instancetype) initWithData:(NSMutableDictionary *)dictionary;

@property(nonatomic, strong)OpenSDK_PayResData *payInfo;
@property(nonatomic, copy)NSString *transactionId;
@end

NS_ASSUME_NONNULL_END
