//
//  OpenSDK_AccountRecord.m
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/18.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import "OpenSDK_AccountRecord.h"

@implementation OpenSDK_AccountRecord

+ (instancetype)initWithMobile:(NSString *)mobile type:(OpenSDKAccountType)type openId:(NSString *)openId token:(NSString *)token userId:(NSString *)userId isReal:(BOOL)isReal needReal:(int)needReal isMobile:(BOOL)isMobile needMobile:(int)needMobile {
    OpenSDK_AccountRecord *result = [[OpenSDK_AccountRecord alloc] init];
    result.accountName = [self secureAccountWith:mobile];
    result.accountValue = mobile;
    result.accountType = type;
    result.loginTime = [[NSDate date] timeIntervalSince1970];//获取当前时间
    result.openId = openId;
    result.token = token;
    result.userId = userId;
    result.mobile = mobile;
    result.isReal = isReal;
    result.needReal = needReal;
    result.isMobile = isMobile;
    result.needMobile = needMobile;
    return result;
}

+ (instancetype)initWithMail:(NSString *)mail openId:(NSString *)openId token:(NSString *)token userId:(NSString *)userId mobile:(NSString *)mobile isReal:(BOOL)isReal needReal:(int)needReal isMobile:(BOOL)isMobile needMobile:(int)needMobile {
    OpenSDK_AccountRecord *result = [[OpenSDK_AccountRecord alloc] init];
    result.accountName = [self secureAccountWith:mail];
    result.accountValue = mail;
    result.accountType = OpenSDKAccountTypeMail;
    result.loginTime = [[NSDate date] timeIntervalSince1970];//获取当前时间
    result.openId = openId;
    result.token = token;
    result.userId = userId;
    result.mobile = mobile;
    result.isReal = isReal;
    result.needReal = needReal;
    result.isMobile = isMobile;
    result.needMobile = needMobile;
    return result;
}

+ (instancetype)initWithGuest:(NSString *)tempId openId:(NSString *)openId token:(NSString *)token userId:(NSString *)userId mobile:(NSString *)mobile isReal:(BOOL)isReal needReal:(int)needReal isMobile:(BOOL)isMobile needMobile:(int)needMobile {
    OpenSDK_AccountRecord *result = [[OpenSDK_AccountRecord alloc] init];
    NSString *shortTempId = tempId;
    if ([tempId length] > 6) {
        shortTempId = [tempId substringToIndex:6];
    }
    result.accountName = [@"游客" stringByAppendingString:shortTempId];//显示"游客+6位id"
    result.accountValue = tempId;
    result.accountType = OpenSDKAccountTypeGuest;
    result.loginTime = [[NSDate date] timeIntervalSince1970];//获取当前时间,单位:秒
    result.openId = openId;
    result.token = token;
    result.userId = userId;
    result.mobile = mobile;
    result.isReal = isReal;
    result.needReal = needReal;
    result.isMobile = isMobile;
    result.needMobile = needMobile;
    return result;
}

+ (instancetype)initWithOtherSDK:(NSString *)nickName platform:(NSString *)platform platformName:(NSString *)platformName platformUid:(NSString *)platformUid openId:(NSString *)openId token:(NSString *)token userId:(NSString *)userId mobile:(NSString *)mobile isReal:(BOOL)isReal needReal:(int)needReal isMobile:(BOOL)isMobile needMobile:(int)needMobile {
    OpenSDK_AccountRecord *result = [[OpenSDK_AccountRecord alloc] init];
    result.accountName = nickName;
    result.accountValue = platformUid;
    result.platform = platform;
    result.accountType = OpenSDKAccountTypeOther;
    result.platformName = platformName;
    result.loginTime = [[NSDate date] timeIntervalSince1970];//获取当前时间,单位:秒
    result.openId = openId;
    result.token = token;
    result.userId = userId;
    result.mobile = mobile;
    result.isReal = isReal;
    result.needReal = needReal;
    result.isMobile = isMobile;
    result.needMobile = needMobile;
    return result;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.accountName forKey:@"accountName"];
    [aCoder encodeObject:self.accountValue forKey:@"accountValue"];
    [aCoder encodeInteger:self.accountType forKey:@"accountType"];
    [aCoder encodeDouble:self.loginTime forKey:@"loginTime"];
    [aCoder encodeObject:self.openId forKey:@"openId"];
    [aCoder encodeObject:self.token forKey:@"token"];
    [aCoder encodeBool:self.isReal forKey:@"isReal"];
    [aCoder encodeInt:self.needReal forKey:@"needReal"];
    [aCoder encodeBool:self.isMobile forKey:@"isMobile"];
    [aCoder encodeInt:self.needMobile forKey:@"needMobile"];
    [aCoder encodeObject:self.platform forKey:@"platform"];
    

}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self == [self init]) {
        self.accountName = [aDecoder decodeObjectForKey:@"accountName"];
        self.accountValue = [aDecoder decodeObjectForKey:@"accountValue"];
        self.accountType = [aDecoder decodeIntegerForKey:@"accountType"];
        self.loginTime = [aDecoder decodeDoubleForKey:@"loginTime"];
        self.openId = [aDecoder decodeObjectForKey:@"openId"];
        self.token = [aDecoder decodeObjectForKey:@"token"];
        self.isReal = [aDecoder decodeBoolForKey:@"isReal"];
        self.needReal = [aDecoder decodeIntForKey:@"needReal"];
        self.isMobile = [aDecoder decodeBoolForKey:@"isMobile"];
        self.needMobile = [aDecoder decodeIntForKey:@"needMobile"];
        self.platform = [aDecoder decodeObjectForKey:@"platform"];
    }
    return self;
}

+ (NSString *)secureAccountWith:(NSString *)account {
    if ([self validateMobile:account]) {
        return [self securePhoneNumberWith:account];
    }
    
    if ([self validateEMail:account]) {
        return [self secureEMailWith:account];
    }
    
    return account;
}


+ (NSString *)securePhoneNumberWith:(NSString *)number {
    if (number == nil) {
        return @"";
    }
    if (number.length <= 6) {
        return number;
    }
    NSInteger index = floor((number.length - 4) / 2.0);
    NSString *result = [number stringByReplacingCharactersInRange:NSMakeRange(index, 4) withString:@"****"];
    return result;
}

+ (NSString *)secureEMailWith:(NSString *)mail {
    if (mail == nil) {
        return @"";
    }
    if (![mail containsString:@"@"]) {
        return mail;
    }
    
    NSInteger atIndex = [mail rangeOfString:@"@"].location;
    NSInteger xCount = floor(atIndex / 2.0);
    NSString *x = @"";
    for (int i = 0; i < xCount; i++) {
        x = [x stringByAppendingString:@"*"];
    }
    NSString *result = [mail stringByReplacingCharactersInRange:NSMakeRange(xCount, xCount) withString:x];
    return result;
}

+ (BOOL)validateEMail:(NSString*)email{
    NSString*regex =@"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
    NSPredicate* emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return[emailPredicate evaluateWithObject:email];
    
}

+ (BOOL) validateMobile:(NSString *)mobile {
    NSString *phoneRegex = @"^(1[3-9][0-9])\\d{8}$";
    NSPredicate *phonePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phonePredicate evaluateWithObject:mobile];
}

@end
