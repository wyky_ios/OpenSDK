//
//  SSDKBaseResData.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 15/10/7.
//  Copyright © 2015年 WYHT. All rights reserved.
//

#import "OpenSDK_BaseResData.h"

#import "OpenSDK_DataKeys.h"

#import "OpenSDK_JsonUtil.h"

@implementation OpenSDK_BaseResData

NSString * const OpenSDK_STATUS_OK = @"ok";

NSString * const OpenSDK_STATUS_FAIL = @"fail";

- (instancetype) initWithDictionary:(NSMutableDictionary *)dictionary {
    self = [super init];
    
    if (self) {
        [self setStatus:[dictionary objectForKey:OpenSDK_S_STATUS]];
        if(![self isOk])
        {
            [self setErrorMsg:[dictionary objectForKey:OpenSDK_S_ERROR_MSG]];
            [self setErrorNo:[[dictionary objectForKey:OpenSDK_S_ERROR_NO] longValue]];
        }
        else
            _content=dictionary;
    }
    
    return self;
}

- (BOOL) isOk {
    if ([OpenSDK_STATUS_OK isEqualToString: self.status]) {
        return YES;
    }
    return NO;
}

-(void) fail
{
    [self setStatus:OpenSDK_STATUS_FAIL];
}

@end
