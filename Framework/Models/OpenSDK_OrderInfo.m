//
//  OpenSDK_OrderInfo.m
//  
//
//  Created by wyht－ios－dev on 16/3/2.
//
//

#import "OpenSDK_OrderInfo.h"

@implementation OpenSDK_OrderInfo

-(instancetype) initWithOrderID:(NSString *) orderID
                       currency:(NSString *) currency
                  productAmount:(NSString *) productAmount
                        product:(NSString *) product
                      payStatus:(NSString *) payStatus
                        appData:(NSString *) appData
                        payTime:(long) payTime
{
    self=[super init];
    
    _orderID=orderID;
    _currency=currency;
    _productAmount=productAmount;
    _product=product;
    _payStatus=payStatus;
    _payTime=payTime;
    _appData=appData;
    
    return self;
}

@end
