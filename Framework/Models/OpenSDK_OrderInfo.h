//
//  OpenSDK_OrderInfo.h
//  
//
//  Created by wyht－ios－dev on 16/3/2.
//
//

#import <Foundation/Foundation.h>

@interface OpenSDK_OrderInfo : NSObject

@property (nonatomic, strong, readonly) NSString *product;
@property (nonatomic, strong, readonly) NSString *productAmount;
@property (nonatomic, strong, readonly) NSString *orderID;
@property (nonatomic, strong, readonly) NSString *payStatus;
@property (nonatomic, strong, readonly) NSString *currency;
@property (nonatomic, strong, readonly) NSString *appData;
@property (readonly) long payTime;

-(instancetype) initWithOrderID:(NSString *) orderID
                       currency:(NSString *) currency
                  productAmount:(NSString *) productAmount
                        product:(NSString *) product
                      payStatus:(NSString *) payStatus
                        appData:(NSString *) appData
                        payTime:(long) payTime;

@end
