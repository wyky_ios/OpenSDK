//
//  OpenSDK_BaseClass.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/18.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import "OpenSDK_BaseClass.h"
#import "OpenSDK_HttpUtil.h"
#import "OpenSDK_BaseResData.h"
#import "OpenSDK_Enum.h"
#import "OpenSDK_UserResData.h"
#import "OpenSDK_LocalRecord.h"
#import "OpenSDK_DataKeys.h"
#import "OpenSDK_LoginManager.h"
#import "OpenSDK_DataManager.h"
#import "OpenSDK_PayResData.h"
#import "LocalizationManager.h"

@implementation OpenSDK_BaseClass

-(void) checkOrderWithData:(NSString *)data orderID:(NSString *) orderID httpDelegate:(void (^)(OpenSDK_BaseResData *))gotInfo
{
    [OpenSDK_HttpUtil checkOrderWithOrderInfo:data orderID:orderID httpDelegate:gotInfo];
}

@end
