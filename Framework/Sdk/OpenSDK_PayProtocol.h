//
//  OpenSDK_PayProtocol.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/11/2.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_PayInfo.h"
#import "OpenSDK_PayResData.h"

@protocol OpenSDK_PayProtocol <NSObject>

@required

// 展示的图标
-(NSString *) getDisplayIcon;

// 展示的名字
-(NSString *) getDisplayName;

// 展示的平台
-(NSString *) getPlatform;

// 展示的平台版本号
-(NSString *) getPlatformVersion;

// 调起第三方平台支付，传入opensdk订单号
-(void) startPay:(OpenSDK_PayResData *) opensdkOrder;

@optional

// 统计的类型名
-(NSString *) getStatisticsType;

// 创建OpenSDK订单前的回调（可以设置额外的参数）
-(void) beforeCreateOrder:(OpenSDK_PayInfo *) payInfo;

@end
