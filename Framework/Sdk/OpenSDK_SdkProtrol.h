//
//  OpenSDK_BaseProtrol.h
//  
//
//  Created by wyht－ios－dev on 16/2/25.
//
//
#import <UIKit/UIKit.h>

@protocol OpenSDK_SdkProtrol <NSObject>

@required

//初始化信息，注册事件监听
-(void) initInfo;

//返回服务器的对应唯一标识
-(NSString *) getServerName;

@end
