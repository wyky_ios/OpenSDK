//
//  OpenSDK_BaseClass.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/18.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenSDK.h"
#import "OpenSDK_BaseResData.h"

@interface OpenSDK_BaseClass : NSObject

-(void) checkOrderWithData : (NSString *) data
                    orderID:(NSString *) orderID
              httpDelegate : (void (^)(OpenSDK_BaseResData* resData)) gotInfo;

@end
