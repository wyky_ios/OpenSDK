//
//  OpenSDK_LoginProtocol.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/11/2.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_BaseResData.h"

@protocol OpenSDK_LoginProtocol <NSObject>

@property (nonatomic, copy) void (^loginDelegate)(OpenSDK_BaseResData* resData);

@required

- (NSString *)getBackgroundImage;

// 展示的图标
-(NSString *) getDisplayIcon;

// 展示的登录选项名字
-(NSString *) getDisplayName;

// 展示的平台名
-(NSString *) getDisplaySdkName;

// 展示的平台
-(NSString *) getPlatform;

// 展示的平台版本号
-(NSString *) getPlatformVersion;

// 调起第三方平台登录
-(void) startLogin;

@optional

@property (nonatomic, copy) void (^actionBlock)(NSInteger type, NSString *content);

// 开始验证
- (void) startAuth;

// 状态 -1：未知, 0：成功， 1：失败
- (NSInteger)status;

@end
