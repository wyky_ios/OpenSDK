//
//  OpenSDK_ConfirmDialog.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/23.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>

//selected
typedef void(^BlockOpenSDKDialogDelegate)(BOOL result);

@interface OpenSDK_ConfirmDialog : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *msgView;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;
@property (strong, nonatomic) NSString *cancelBtnTitle;
@property (strong, nonatomic) NSString *okBtnTitle;

@property (copy, nonatomic) BlockOpenSDKDialogDelegate blockDelegate;
@property (strong, nonatomic) NSString *msg;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *bgView;

+ (instancetype)showWithMessage:(NSString *)msg
                      okBtnName:(NSString *)okTitle
                      cancelBtnName:(NSString *)cancelTitle
                       delegate:(BlockOpenSDKDialogDelegate)delegate
                           root:(UIViewController *)root;

@end
