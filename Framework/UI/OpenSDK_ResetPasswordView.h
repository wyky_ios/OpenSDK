//
//  OpenSDK_ResetPasswordView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/25.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_TextField.h"
#import "OpenSDK_BaseKeyboradView.h"
#import "OpenSDK_SlideView.h"

@interface OpenSDK_ResetPasswordView : OpenSDK_BaseKeyboradView

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UILabel *tipTextView;

@property (weak, nonatomic) IBOutlet OpenSDK_TextField *mailInputView;

@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@property (weak, nonatomic) IBOutlet OpenSDK_SlideView *slideView;


@end
