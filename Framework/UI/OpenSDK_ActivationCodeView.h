//
//  OpenSDK_ActivationCodeView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/10/10.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_TextField.h"
#import "OpenSDK_BaseKeyboradView.h"

@interface OpenSDK_ActivationCodeView : OpenSDK_BaseKeyboradView

@property (weak, nonatomic) IBOutlet OpenSDK_TextField *codeInputView;

@property (weak, nonatomic) IBOutlet UIButton *okBtn;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *bgView;

@end
