//
//  HistoryAccountCell.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/18.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_AccountRecord.h"

@protocol HistoryAccountCellDeleteDelegate

@required

- (void)onDelete:(OpenSDK_AccountRecord *)account;

@end


@interface HistoryAccountCell : UITableViewCell

@property (strong, nonatomic, readwrite) OpenSDK_AccountRecord *account;

@property (strong, nonatomic, readwrite) id<HistoryAccountCellDeleteDelegate> deleteDelegate;

@property (weak, nonatomic) IBOutlet UIImageView *accountTypeView;

@property (weak, nonatomic) IBOutlet UILabel *accountNameView;

@property (weak, nonatomic) UIViewController *vc;

@end
