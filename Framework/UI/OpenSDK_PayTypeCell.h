//
//  OpenSDK_PayTypeCell.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/23.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSDK_PayTypeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *nameView;

@end
