//
//  OpenSDK_LoginInvalidView.m
//  YCOpenSDK
//
//  Created by Mac on 2021/6/25.
//

#import "OpenSDK_LoginInvalidView.h"
#import "OpenSDK_UIManager.h"

@interface OpenSDK_LoginInvalidView ()

@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) UILabel *msgLabel;

@property (nonatomic, strong) UIButton *loginBtn;

@property (nonatomic, copy) NSArray<NSLayoutConstraint *> *containerViewConstraints;

@property (nonatomic, copy) NSArray<NSLayoutConstraint *> *msgLabelConstraints;

@property (nonatomic, copy) NSArray<NSLayoutConstraint *> *loginBtnConstraints;
@end

@implementation OpenSDK_LoginInvalidView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
    [self.view addSubview:self.containerView];
    [NSLayoutConstraint activateConstraints:self.containerViewConstraints];
    [self.containerView addSubview:self.msgLabel];
    [NSLayoutConstraint activateConstraints:self.msgLabelConstraints];
    [self.containerView addSubview:self.loginBtn];
    [NSLayoutConstraint activateConstraints:self.loginBtnConstraints];
    
}

-(void) loginAction{
    [[OpenSDK_UIManager getInstance] closeUI];
    if(!_complateBlock){
        return;
    }
    self.complateBlock();
}

-(UIView *) containerView{
    if (!_containerView){
        _containerView = [[UIView alloc] initWithFrame:CGRectZero];
        _containerView.backgroundColor = UIColor.whiteColor;
        _containerView.layer.cornerRadius = 10;
        _containerView.layer.masksToBounds = YES;
        _containerView.translatesAutoresizingMaskIntoConstraints = NO;
        
    }
    return _containerView;
}

-(UILabel *) msgLabel{
    if (!_msgLabel){
        _msgLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _msgLabel.text = @"登录信息已经失效，请重新登录";
        _msgLabel.font = [UIFont systemFontOfSize:12];
        _msgLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _msgLabel;
}

-(UIButton *) loginBtn{
    if (!_loginBtn){
        _loginBtn = [[UIButton alloc] initWithFrame:CGRectZero];
        [_loginBtn setTitle:@"好的" forState:UIControlStateNormal];
        [_loginBtn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
        _loginBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [_loginBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        [_loginBtn setBackgroundImage:[UIImage imageNamed:@"op_btn_confirm"] forState:UIControlStateNormal];
        _loginBtn.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _loginBtn;
}

- (NSArray<NSLayoutConstraint *> *)containerViewConstraints{
    if (!_containerViewConstraints){
        _containerViewConstraints = @[[NSLayoutConstraint constraintWithItem:_containerView
                                                                   attribute:NSLayoutAttributeCenterX
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.view
                                                                   attribute:NSLayoutAttributeCenterX
                                                                  multiplier:1.0
                                                                    constant:0],
                                      [NSLayoutConstraint constraintWithItem:_containerView
                                                                   attribute:NSLayoutAttributeCenterY
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.view
                                                                   attribute:NSLayoutAttributeCenterY
                                                                  multiplier:1.0
                                                                    constant:0],
                                      [NSLayoutConstraint constraintWithItem:_containerView
                                                                   attribute:NSLayoutAttributeWidth
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:nil
                                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                                  multiplier:1.0
                                                                    constant:276],
                                      [NSLayoutConstraint constraintWithItem:_containerView
                                                                   attribute:NSLayoutAttributeHeight
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:nil
                                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                                  multiplier:1.0
                                                                    constant:131]];
    }
    return _containerViewConstraints;
}

- (NSArray<NSLayoutConstraint *> *)msgLabelConstraints{
    if (!_msgLabelConstraints){
        _msgLabelConstraints = @[[NSLayoutConstraint constraintWithItem:_msgLabel
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.containerView
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.0 constant:0],
                                 [NSLayoutConstraint constraintWithItem:_msgLabel
                                                              attribute:NSLayoutAttributeTop
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.containerView
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.0
                                                               constant:40]];
    }
    return _msgLabelConstraints;
}

- (NSArray<NSLayoutConstraint *> *)loginBtnConstraints{
    if (!_loginBtnConstraints){
        _loginBtnConstraints = @[[NSLayoutConstraint constraintWithItem:_loginBtn
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.containerView
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.0 constant:0],
                                 [NSLayoutConstraint constraintWithItem:_loginBtn
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.containerView
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1.0
                                                               constant:-20],
                                 [NSLayoutConstraint constraintWithItem:_loginBtn
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1.0
                                                               constant:110],
                                 [NSLayoutConstraint constraintWithItem:_loginBtn
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1.0
                                                               constant:30]];
    }
    return _loginBtnConstraints;
}


@end
