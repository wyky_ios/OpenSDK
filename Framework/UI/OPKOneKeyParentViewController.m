//
//  OPKOneKeyParentViewController.m
//  YCOpenSDK
//
//  Created by MacPro on 2020/12/28.
//

#import "OPKOneKeyParentViewController.h"

#import "OpenSDK_DataManager.h"
#import "OpenSDK_DataKeys.h"
#import "OpenSDK_LoginManager.h"
#import "OpenSDK_LoginProtocol.h"
#import "OpenSDK_LoginLoadingView.h"
#import "OpenSDK_Toast.h"
#import "OpenSDK_UIManager.h"

#import "OPKTracking.h"
#import "OPKCoreConfiguration.h"

@interface OPKOneKeyParentViewController ()
@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *promptLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *backButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *closeButton;

@end

@implementation OPKOneKeyParentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _bgView.backgroundColor = UIColor.whiteColor;
    _bgView.layer.cornerRadius = 10;
    _bgView.layer.masksToBounds = true;
    // Do any additional setup after loading the view.
    if (self.navigationController.viewControllers.count > 1) {
        [_backButton setHidden:NO];
        [_closeButton setHidden:YES];
    } else {
        [_backButton setHidden:YES];
        [_closeButton setHidden:NO];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_activityIndicatorView setHidden:NO];
    [self showOneKeyLogin];
}

- (void)showOneKeyLogin {
    NSString *loginType = OpenSDK_S_ONE_KEY;
    id<OpenSDK_LoginProtocol> sdk = [[OpenSDK_DataManager getInstance].supportSDKServerNames objectForKey:loginType];
    // loading框
    UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"LoginMain" bundle:nil];
    OpenSDK_LoginLoadingView *vc = (OpenSDK_LoginLoadingView *) [loginStoryboard instantiateViewControllerWithIdentifier:@"OpenSDK_LoginLoadingView"];
    
    __block typeof(sdk) weakSdk = sdk;
    sdk.actionBlock = ^(NSInteger type, NSString *content) {
        if (type == 1) {
            NSLog(@"closed");
        } else if (type == 2) {
            NSLog(@"started");
            [self.activityIndicatorView setHidden:YES];
            [[OPKTracking shared] pageView:OPKPageLoginOneKey];
        } else if(type == 6) {
            [[OpenSDK_DataManager getInstance] setContranctChecked:YES];
        } else if(type == 7) {
            [[OpenSDK_DataManager getInstance] setContranctChecked:NO];
        } else if (type == 8) {
            [[OPKTracking shared] event:OPKEventLoginOnekeyNext];
            vc.accoutnName = [weakSdk getDisplaySdkName];
            [self.navigationController pushViewController:vc animated:NO];
        } else if (type == 6002) {
            NSLog(@"一键登录取消");
        } else if (type == 6004) {
            NSLog(@"已登录");
        } else if (type == 19909) { // 返回
            [[OpenSDK_UIManager getInstance] goBack];
        } else if (type == 19902) { // 关闭
            [[OpenSDK_UIManager getInstance] closeUI];
        } else if (type == 19903) { // 预取号失败
            // 登录方式中移除
            NSMutableArray *types = [OpenSDK_DataManager getInstance].supportLoginTypes.mutableCopy;
            [types removeObject:OpenSDK_S_ONE_KEY];
            [OpenSDK_DataManager getInstance].supportLoginTypes = types.copy;
            
            if ([OpenSDK_UIManager getInstance].navigationController.viewControllers.count > 1) {
                [[OpenSDK_UIManager getInstance] goBack];
                [[[OpenSDK_iToast makeText:@"预取号失败"] setDuration:2000] show];
            } else {
                [[OpenSDK_UIManager getInstance] showMoboleViewControllerWithAccount:nil canBack:NO];
                [self popSelf];
            }
        } else if(type == 666) {
            [[OpenSDK_UIManager getInstance] handleOtherLoginMethodWithCurLoginType:OpenSDK_S_ONE_KEY contranctChecked:[OpenSDK_DataManager getInstance].contranctChecked];
        }
        else {
            
        }
    };
    
    // 再调用第三方登录接口
    [[OpenSDK_LoginManager getInstance] loginWithPlatform:loginType delegate:^(BOOL result, OpenSDK_AccountRecord *account, long errorNo, NSString *err) {
        if (!result) {
            // 登录失败
            [[[OpenSDK_iToast makeText:err] setDuration:2000] show];
        }
        [vc loginResultCallback:result account:account];
    }];
}

- (IBAction)back:(UIButton *)sender {
    [[OpenSDK_UIManager getInstance] goBack];
}

- (void)popSelf {
    NSMutableArray *viewControllers = [self.navigationController.viewControllers mutableCopy];
    if (viewControllers.count > 1) {
        [viewControllers removeObjectAtIndex:0];
    }
    [self.navigationController setViewControllers:viewControllers.copy animated:NO];
}
- (IBAction)close:(id)sender {
    [[OpenSDK_UIManager getInstance] closeUI];
}

@end
