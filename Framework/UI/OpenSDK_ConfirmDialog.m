//
//  OpenSDK_ConfirmDialog.m
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/23.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import "OpenSDK_ConfirmDialog.h"
#import "OpenSDK_UIUtil.h"

@implementation OpenSDK_ConfirmDialog

#pragma mark - 生命周期函数
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _bgView.backgroundColor = UIColor.whiteColor;
    _bgView.layer.cornerRadius = 10;
    _bgView.layer.masksToBounds = true;
    if (self.msg) {
        self.msgView.text = self.msg;
    }
    
    if (self.okBtnTitle != nil && self.okBtnTitle.length > 0) {
        [self.okBtn setTitle:self.okBtnTitle forState:UIControlStateNormal];
    }
    if (self.cancelBtnTitle != nil && self.cancelBtnTitle.length > 0) {
        [self.cancelBtn setTitle:self.cancelBtnTitle forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (instancetype)showWithMessage:(NSString *)msg
                      okBtnName:(NSString *)okTitle
                  cancelBtnName:(NSString *)cancelTitle
                       delegate:(BlockOpenSDKDialogDelegate)delegate
                           root:(UIViewController *)root {
    UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"LoginMain" bundle:nil];
    OpenSDK_ConfirmDialog *vc = (OpenSDK_ConfirmDialog *) [loginStoryboard instantiateViewControllerWithIdentifier:@"OpenSDK_ConfirmDialog"];
    vc.msg = msg;
    vc.blockDelegate = delegate;
    vc.okBtnTitle = okTitle;
    vc.cancelBtnTitle = cancelTitle;
    if (root == nil) {
        root=[OpenSDK_UIUtil getCurrentVC];
    }
    vc.modalPresentationStyle=UIModalPresentationCustom;
    [root presentViewController:vc animated:NO completion:nil];
    return vc;
}

- (IBAction)didClickCancelBtn:(id)sender {
    [self dismissViewControllerAnimated:NO completion:^{
        if (self.blockDelegate) {
            self.blockDelegate(NO);
        }
    }];
}
- (IBAction)didClickOkBtn:(id)sender {
    [self dismissViewControllerAnimated:NO completion:^{
        if (self.blockDelegate) {
            self.blockDelegate(YES);
        }
    }];
}

@end
