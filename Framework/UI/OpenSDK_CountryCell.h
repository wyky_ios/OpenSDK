//
//  OpenSDK_CountryCell.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/24.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSDK_CountryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *numberView;
@property (weak, nonatomic) IBOutlet UILabel *nameView;

@end
