//
//  OpenSDK_PayView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/23.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_PayInfo.h"

@interface OpenSDK_PayView : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *productNameView;
@property (weak, nonatomic) IBOutlet UILabel *productPriceView;
@property (weak, nonatomic) IBOutlet UICollectionView *payTypesView;

//展示支付界面
-(void) setPayInfo:(OpenSDK_PayInfo *) payInfo wihtConfig:(NSArray *)confInfo;

@end
