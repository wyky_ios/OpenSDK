//
//  OpenSDK_BaseKeyboradView.m
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/30.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import "OpenSDK_BaseKeyboradView.h"

@interface OpenSDK_BaseKeyboradView ()

@end

@implementation OpenSDK_BaseKeyboradView

{
    //记录选择屏幕引起的输入框
    UITextField *curEditField;
}

#pragma mark - 生命周期函数
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    if (@available(iOS 13.0, *)) {
//        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
//    } else {
//        // Fallback on earlier versions
//    }
    
    // 默认键盘额外上移距离
    self.keyboardExtraMargin = 20;
    // 点击背景，收起输入法
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didHideKeyboard:)]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // 键盘弹出界面移动
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didClickKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    // 键盘弹出界面恢复
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didKboardDisappear:) name:UIKeyboardWillHideNotification object:nil];
}

-(void) viewWillDisappear:(BOOL)animated {
    // 移除键盘监听
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 收起输入法,public api
-(void)didHideKeyboard:(id)sender {
    //收起输入法
    if (curEditField != nil) {
        [curEditField endEditing:YES];
    }
}

#pragma mark - 监听输入框
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    curEditField = textField;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    curEditField = nil;
}

#pragma mark - 键盘即将跳出
-(void)didClickKeyboard:(NSNotification *)sender {
    if (curEditField == nil) {
        return;
    }
    if (!curEditField.isFirstResponder) {
        NSLog(@"didClickKeyboard，不是对应输入框获取焦点，忽略！");
        return;
    }
    //获取键盘高度
//    CGFloat duration = [[sender.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] CGRectValue].size.height;
    CGFloat duration = [[sender.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGFloat keyboardHeight = [[sender.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
    // 移动
    UIView *moveView = self.keyboardTargetView;
    if (moveView == nil) {
        moveView = curEditField;
    }
    //计算出textField相对应屏幕的坐标
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    CGRect rect= [moveView convertRect: moveView.bounds toView:window];
    CGFloat inputViewY = rect.origin.y;
    CGFloat inputViewHeight = rect.size.height;
    CGFloat bottomMargin = self.view.frame.size.height - (inputViewY + inputViewHeight);
    NSLog(@"input.y=%f,input.height=%f,input.bottomMargin=%f,keyboardHeight=%f", inputViewY, inputViewHeight, bottomMargin, keyboardHeight);
    // 只有键盘高度大于输入框底部边距，即键盘可能覆盖输入框时，才上移
    if (bottomMargin > keyboardHeight + self.keyboardExtraMargin) {
        NSLog(@"输入框高于键盘，无需上移");
    } else {
        CGFloat offset = keyboardHeight + self.keyboardExtraMargin - bottomMargin;
        [UIView animateWithDuration:duration animations:^{
            self.view.transform = CGAffineTransformMakeTranslation(0, -offset);
        }];
    }
}

#pragma mark - 当键盘即将消失
-(void)didKboardDisappear:(NSNotification *)sender {
    if (curEditField == nil) {
        return;
    }
    if (!curEditField.isFirstResponder) {
        NSLog(@"didKboardDisappear，不是对应输入框获取焦点，忽略！");
        return;
    }
    CGFloat duration = [sender.userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        self.view.transform = CGAffineTransformIdentity;
    }];
}

@end
