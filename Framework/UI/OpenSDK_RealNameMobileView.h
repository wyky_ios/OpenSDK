//
//  OpenSDK_RealNameMobileView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/30.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_TextField.h"
#import "OpenSDK_BaseKeyboradView.h"

@interface OpenSDK_RealNameMobileView : OpenSDK_BaseKeyboradView

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet OpenSDK_TextField *realnameInputView;

@property (weak, nonatomic) IBOutlet OpenSDK_TextField *cardInputView;

@property (weak, nonatomic) IBOutlet OpenSDK_TextField *mobileInputView;

@property (weak, nonatomic) IBOutlet OpenSDK_TextField *codeInputView;

@property (weak, nonatomic) IBOutlet UIButton *codeBtn;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *smsIndicator;

@end
