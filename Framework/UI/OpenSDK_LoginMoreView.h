//
//  OpenSDK_LoginMoreView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/31.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSDK_LoginMoreView : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *loginTypesView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *bgView;

@end
