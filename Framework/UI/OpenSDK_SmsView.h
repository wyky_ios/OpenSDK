//
//  OpenSDK_SmsView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/19.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_NumberInputView.h"
#import "OpenSDK_BaseKeyboradView.h"
#import "OpenSDK_PayInfo.h"

@interface OpenSDK_SmsView : OpenSDK_BaseKeyboradView

@property (nonatomic, strong) NSString *mobile;

//是否绑定手机账号？否则就是登录
@property (nonatomic, readwrite) BOOL isBind;

//是否从支付进入的绑定手机界面，会有订单信息
@property (nonatomic, readwrite) OpenSDK_PayInfo *order;

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UILabel *msgView;

@property (weak, nonatomic) IBOutlet OpenSDK_NumberInputView *smsInputView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *smsBtnIndicator;

@property (weak, nonatomic) IBOutlet UIButton *smsSendBtn;

@property (weak, nonatomic) IBOutlet UILabel *countDownView;

@end
