//
//  OpenSDK_ResetPasswordView.m
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/25.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import "OpenSDK_ResetPasswordView.h"
#import "OpenSDK_Toast.h"
#import "OpenSDK_LoginManager.h"
#import "OpenSDK_DataManager.h"

@interface OpenSDK_ResetPasswordView ()

@end

@implementation OpenSDK_ResetPasswordView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _bgView.backgroundColor = UIColor.whiteColor;
    _bgView.layer.cornerRadius = 10;
    _bgView.layer.masksToBounds = true;
    // 输入框内容缩进
    self.mailInputView.delegate = self;
    self.mailInputView.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    // 输入框左边是mail图标
    self.mailInputView.iconImage = [UIImage imageNamed:@"op_icon_mail"];
    
    NSString *imgUrl = [OpenSDK_DataManager getSlideImageUrl];
    if (imgUrl == nil || imgUrl.length == 0) {
        // 没有图片地址，则隐藏滑块解锁
        self.slideView.hidden = YES;
    } else {
        self.slideView.sliderThumbImage = [UIImage imageNamed:@"op_img_slider"];
        self.slideView.sliderTipText = @">>>请拖动滑块完成拼图>>>";
        self.slideView.sliderSuccessText = @"完成";
        self.slideView.errorResetCount = 3;
        self.slideView.indentifyCompletion = ^(OpenSDK_SlideView* view, BOOL success) {
            NSLog(@"Identify %@",(success ? @"success" : @"fail"));
            if (success) {
                [[[OpenSDK_iToast makeText:@"验证成功"] setDuration:2000] show];
            } else {
                [[[OpenSDK_iToast makeText:@"验证失败，请重新拖动滑块"] setDuration:2000] show];
            }
        };
        self.slideView.loadNewImage = ^(OpenSDK_SlideView *view) {
            // 加载新图片
            [view setImageWithUrl:imgUrl];
        };
        // 滑块验证控件为隐藏模式
        [self.slideView setHideMode:^(BOOL display) {
            if (display) {
                self.mailInputView.hidden = YES;
                self.tipTextView.hidden = YES;
            } else {
                self.mailInputView.hidden = NO;
                self.tipTextView.hidden = NO;
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.slideView setImageWithUrl:[OpenSDK_DataManager getSlideImageUrl]];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - btn callback
- (IBAction)didClickBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)didClickSendBtn:(id)sender {
    // 收起键盘
    [self didHideKeyboard:sender];
    
    NSString *mail = self.mailInputView.text;
    if (mail == nil || mail.length == 0) {
        [[[OpenSDK_iToast makeText:@"抱歉，请输入邮箱"] setDuration:2000] show];
        return;
    }
    // 判断邮箱格式
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if(![emailTest evaluateWithObject:mail]) {
        [[[OpenSDK_iToast makeText:@"抱歉，请输入正确的邮箱"] setDuration:2000] show];
        return;
    }
    
    // 判断滑块验证
    if(!self.slideView.hidden && !self.slideView.isSuccess) {
        [[[OpenSDK_iToast makeText:@"抱歉，请完成滑块验证"] setDuration:2000] show];
        return;
    }
    
    [self.okBtn setEnabled:NO];
    // 调用重置密码接口
    [[OpenSDK_LoginManager getInstance] resetPassworkWithMail:mail delegate:^(BOOL result, NSString *err) {
        [self.okBtn setEnabled:YES];
        if (!result) {
            // 重置密码失败
            [[[OpenSDK_iToast makeText:err] setDuration:2000] show];
        } else {
            [[[OpenSDK_iToast makeText:[NSString stringWithFormat:@"邮件发送成功，请前往邮箱%@完成重置密码操作", mail]] setDuration:3000] show];
            [self didClickBackBtn:nil];
        }
    }];
}

@end
