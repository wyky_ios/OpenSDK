//
//  OpenSDK_ContactView.m
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/19.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import "OpenSDK_ContactView.h"
#import "OpenSDK_HttpUtil.h"
#import "OpenSDK_DataKeys.h"
#import "OpenSDK_DataManager.h"

@interface OpenSDK_ContactView ()

@end

@implementation OpenSDK_ContactView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (_titleText != nil && _titleText.length != 0) {
        _titleLabel.text = _titleText;
    }
    
    NSString *url = [OpenSDK_DataManager getProtrolUrl];
    NSString *key = OpenSDK_S_PROTROL;
    if (_type == OpenSDKAgreementTypePrivacy) {
        url = [OpenSDK_DataManager getPrivacyUrl];
        key = OpenSDK_S_PRIACY;
    }
    
    [OpenSDK_HttpUtil getWithUrl:url data:nil httpDelegate:^(OpenSDK_BaseResData *resData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if([resData isOk]) {
                self.contactView.text=[[resData content] objectForKey:key];
            } else {
                self.contactView.text=resData.errorMsg;
            }
            if (self.indicator) {
                [self.indicator stopAnimating];
            }
        });
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didClickBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

@end
