//
//  OpenSDK_RealNameView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/25.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_TextField.h"
#import "OpenSDK_BaseKeyboradView.h"

@interface OpenSDK_RealNameView : OpenSDK_BaseKeyboradView

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet OpenSDK_TextField *realnameInputView;

@property (weak, nonatomic) IBOutlet OpenSDK_TextField *cardInputView;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@end
