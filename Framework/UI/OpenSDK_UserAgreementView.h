//
//  OpenSDK_UserAgreementView.h
//  YCOpenSDK
//
//  Created by ycgame on 2020/7/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenSDK_UserAgreementView : UIView
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *agreeButton;

@end

NS_ASSUME_NONNULL_END
