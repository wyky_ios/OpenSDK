//
//  OpenSDK_ContactView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/19.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>

// 协议类型
typedef enum {
    OpenSDKAgreementTypeContract,
    OpenSDKAgreementTypePrivacy
} OpenSDKAgreementType;

@interface OpenSDK_ContactView : UIViewController
@property(nonatomic, copy)NSString *titleText;
@property(nonatomic, assign)OpenSDKAgreementType type;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *contactView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
