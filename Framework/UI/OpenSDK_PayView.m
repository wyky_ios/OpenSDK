//
//  OpenSDK_PayView.m
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/23.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import "OpenSDK_PayView.h"
#import "OpenSDK_PayTypeCell.h"
#import "OpenSDK_StatisticsManager.h"
#import "OpenSDK_PayManager.h"
#import "OpenSDK_DataManager.h"
#import "OpenSDK_PayProtocol.h"

#import "OpenSDK_UIUtil.h"

@interface OpenSDK_PayView () <UICollectionViewDelegate, UICollectionViewDataSource>
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation OpenSDK_PayView

{
    OpenSDK_PayInfo *curPayInfo;
    NSArray *configInfo;
}
static CGFloat const padding = 20;
static CGFloat const insetMargin = 30;
static CGFloat const itemHeight = 50;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (@available(iOS 11.0, *)) {
        _titleLabel.backgroundColor = [UIColor colorNamed:@"YCOPayTitleBGColor"];
    } else {
        // Fallback on earlier versions
        _titleLabel.backgroundColor = [UIColor colorWithRed:0.91 green:0.96 blue:1 alpha:1];
    }
    
    self.payTypesView.delegate = self;
    self.payTypesView.dataSource = self;
    [self.payTypesView registerNib:[UINib nibWithNibName:@"OpenSDK_PayCell" bundle:nil] forCellWithReuseIdentifier:@"OpenSDK_PayTypeCell"];
    
    self.productNameView.text = curPayInfo.productName;
    self.productPriceView.text = [NSString stringWithFormat:@"支付金额：¥%@", curPayInfo.orderAmount];
    [self.payTypesView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - public api
-(void) setPayInfo:(OpenSDK_PayInfo *) payInfo wihtConfig:(NSArray *)confInfo {
    curPayInfo = payInfo;
    configInfo = confInfo;
    [self.payTypesView reloadData];
}

#pragma mark - btn callback
- (IBAction)didClickCloseBtn:(id)sender {
    // 上报支付取消统计
    [[OpenSDK_StatisticsManager getInstance] collectPayCancelWithCpOrderId:curPayInfo.appData detail:OPENSDK_STATS_DETAIl_PAY_CANCEL_UI];
    // 隐藏界面
    [self dismissViewControllerAnimated:NO completion:nil];
    // 回调支付取消
    [[OpenSDK_PayManager getInstance] callbackPayCancel];
}

#pragma mark - 支付列表回调函数
//定义展示的Section的个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

//定义展示的UICollectionViewCell的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [configInfo count];
}

//每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%@,%@",NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    OpenSDK_PayTypeCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"OpenSDK_PayTypeCell" forIndexPath:indexPath];
    [cell.layer setBorderWidth:1.0];
    [cell.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    NSString *payType = [configInfo objectAtIndex:indexPath.row];
    id<OpenSDK_PayProtocol> sdk = [[OpenSDK_DataManager getInstance].supportSDKServerNames objectForKey:payType];
    cell.iconView.image = [UIImage imageNamed:[sdk getDisplayIcon]];
    cell.nameView.text = [sdk getDisplayName];
    return cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = (self.payTypesView.frame.size.width - 2*[OpenSDK_UIUtil getUISize:padding]) / 2.0;
    NSLog(@"collectionView insetForSectionAtIndex! cvWidth=%f, itemWidth=%f", self.payTypesView.frame.size.width, width);
    return CGSizeMake(width > 0 ? width : 0, itemHeight);
}

//设置整个组的缩进量是多少
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsZero;
}

//设置最小行间距，也就是前一行与后一行的中间最小间隔
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return [OpenSDK_UIUtil getUISize:insetMargin];
}

//设置最小列间距，也就是左列与右一列的中间最小间隔
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return [OpenSDK_UIUtil getUISize:insetMargin];
}

//UICollectionView被选中时调用的方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"collectionView click! index=%ld", (long)indexPath.row);
    NSString *payType = [configInfo objectAtIndex:indexPath.row];
    [[OpenSDK_PayManager getInstance] pay:curPayInfo serverName:payType];
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
