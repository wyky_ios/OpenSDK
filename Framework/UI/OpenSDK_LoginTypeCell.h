//
//  OpenSDK_LoginTypeCell.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/31.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSDK_LoginTypeCell : UICollectionViewCell
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (weak, nonatomic) IBOutlet UIImageView *iconView;

@property (weak, nonatomic) IBOutlet UILabel *nameView;

@end
