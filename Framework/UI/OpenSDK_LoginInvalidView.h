//
//  OpenSDK_LoginInvalidView.h
//  YCOpenSDK
//
//  Created by Mac on 2021/6/25.
//

#import <UIKit/UIKit.h>

@interface OpenSDK_LoginInvalidView : UIViewController

@property (nonatomic , copy)void(^complateBlock)(void);

@end
