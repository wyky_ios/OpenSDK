//
//  OpenSDK_LoginMobileView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/12.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_TextField.h"
#import "OpenSDK_BaseKeyboradView.h"

@interface OpenSDK_LoginMobileView : OpenSDK_BaseKeyboradView

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (weak, nonatomic) IBOutlet UIImageView *logoImgView;

@property (weak, nonatomic) IBOutlet OpenSDK_TextField *mobileInputView;

@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@property (weak, nonatomic) IBOutlet UIButton *otherLoginBg;

//是否展示返回按钮
@property (nonatomic) BOOL showBack;

// 是否不能返回
@property (nonatomic, assign) BOOL cantBack;


@property (weak, nonatomic) NSString *inputMobile;

@end
