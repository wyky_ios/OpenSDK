//
//  OpenSDK_UserAgreementView.m
//  YCOpenSDK
//
//  Created by ycgame on 2020/7/16.
//

#import "OpenSDK_UserAgreementView.h"
#import "OpenSDK_UIManager.h"
#import "OpenSDK_ContactView.h"
#import "OpenSDK_DataManager.h"
#import "OPKWebViewController.h"
#import "OpenSDK_UIUtil.h"

@interface OpenSDK_UserAgreementView()
@property (strong, nonatomic) IBOutlet UIView *view;

@end

@implementation OpenSDK_UserAgreementView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initViewFormNib];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initViewFormNib];
    }
    return self;
}

- (void)initViewFormNib {
    [self addSubview:self.view];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.view.frame = self.bounds;
}

- (UIView *)view {
    if (_view == nil) {
        _view = [[NSBundle mainBundle] loadNibNamed:@"OpenSDK_UserAgreementView" owner:self options:nil].firstObject;
    }
    return _view;
}

- (IBAction)agreeButton:(UIButton *)sender {
    sender.selected = !sender.selected;
    [[OpenSDK_DataManager getInstance] setContranctChecked:sender.selected];
}

- (IBAction)userAgreementButtonDidClick:(UIButton *)sender {
    OPKWebViewController *webViewController = [[OPKWebViewController alloc] init];
    webViewController.url = [OpenSDK_DataManager getInstance].contractWebUrl;
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:webViewController];
    navController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [navController setNavigationBarHidden:YES];
    [[[OpenSDK_UIManager getInstance] navigationController] presentViewController:navController animated:YES completion:nil];
}
- (IBAction)userPrivateButtonDidClick:(UIButton *)sender {
    OPKWebViewController *webViewController = [[OPKWebViewController alloc] init];
    webViewController.url = [OpenSDK_DataManager getInstance].privacyWebUrl;
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:webViewController];
    navController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [navController setNavigationBarHidden:YES];
    [[[OpenSDK_UIManager getInstance] navigationController] presentViewController:navController animated:YES completion:nil];
}


@end
