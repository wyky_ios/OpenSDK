//
//  OpenSDK_RealNameView.m
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/25.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import "OpenSDK_RealNameView.h"
#import "OpenSDK_LoginManager.h"
#import "OpenSDK_DataManager.h"
#import "OpenSDK_UIManager.h"
#import "OpenSDK_Toast.h"
#import "OpenSDK_StringUtil.h"

@interface OpenSDK_RealNameView ()

@end

@implementation OpenSDK_RealNameView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _bgView.backgroundColor = UIColor.whiteColor;
    _bgView.layer.cornerRadius = 10;
    _bgView.layer.masksToBounds = true;
    // 输入框内容缩进
    self.realnameInputView.delegate = self;
    self.realnameInputView.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    // 输入框左边是mail图标
    self.realnameInputView.iconImage = [UIImage imageNamed:@"op_icon_realname"];
    
    // 输入框内容缩进
    self.cardInputView.delegate = self;
    self.cardInputView.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    // 输入框左边是mail图标
    self.cardInputView.iconImage = [UIImage imageNamed:@"op_icon_card"];
    
    // 点击背景，收起输入法
    [self.bgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didHideKeyboard:)]];
    
    //判断是否强制实名认证
    OpenSDK_AccountRecord *curAccount = [[OpenSDK_DataManager getInstance] getCurAccount];
    if (curAccount != nil && curAccount.needReal == 2) {
        self.closeBtn.hidden = YES;// 强制实名认证，不显示关闭按钮
    } else {
        self.closeBtn.hidden = NO;// 非强制实名认证，显示关闭按钮
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - btn callback
- (IBAction)didClickOkBtn:(id)sender {
    //收起键盘
    [self didHideKeyboard:sender];
    
    NSString *name = self.realnameInputView.text;
    if (name == nil || name.length == 0) {
        [[[OpenSDK_iToast makeText:@"抱歉，请输入真实姓名"] setDuration:2000] show];
        return;
    }
    
    NSString *card = self.cardInputView.text;
    if (card == nil || card.length == 0) {
        [[[OpenSDK_iToast makeText:@"抱歉，请输入身份证号"] setDuration:2000] show];
        return;
    }
    
    if (![OpenSDK_StringUtil validateIDCardNumber:card]){
        [[[OpenSDK_iToast makeText:@"抱歉，请输入正确身份证号"] setDuration:2000] show];
        return;
    }
    
    [self.okBtn setEnabled:NO];
    [[OpenSDK_LoginManager getInstance] realnameWithName:name card:card phone:nil securityCode:nil delegate:^(BOOL result, NSString *err) {
        [self.okBtn setEnabled:YES];
        if (!result) {
            [[[OpenSDK_iToast makeText:err] setDuration:2000] show];
        } else {
            [[[OpenSDK_iToast makeText:@"实名认证成功"] setDuration:2000] show];
            [self didClickCloseBtn:nil];
        }
    }];
}

- (IBAction)didClickCloseBtn:(id)sender {
    [[OpenSDK_UIManager getInstance] closeUI];
    // 实名认证成功(或直接关闭)，发出登录成功事件
    [[OpenSDK_LoginManager getInstance] sendLoginSuccessEvent];
}

@end
