//
//  OpenSDK_RegisterMailView.m
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/30.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import "OpenSDK_RegisterMailView.h"
#import "OpenSDK_LoginLoadingView.h"
#import "OpenSDK_Toast.h"

#import "OpenSDK_UIManager.h"
#import "OpenSDK_LoginManager.h"
#import "OpenSDK_DataManager.h"
#import "OpenSDK_UIUtil.h"

@interface OpenSDK_RegisterMailView ()

@end

@implementation OpenSDK_RegisterMailView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _bgView.backgroundColor = UIColor.whiteColor;
    _bgView.layer.cornerRadius = 10;
    _bgView.layer.masksToBounds = true;
    // 输入框内容缩进
    self.mailInputView.delegate = self;
    self.mailInputView.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    // 输入框左边是mail图标
    self.mailInputView.iconImage = [UIImage imageNamed:@"op_icon_mail"];
    
    // 输入框内容缩进
    self.passwordInputView.delegate = self;
    self.passwordInputView.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    // 输入框左边是password图标
    self.passwordInputView.iconImage = [UIImage imageNamed:@"op_icon_password"];
    self.passwordInputView.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.passwordInputView.secureTextEntry = YES;
    
    // 输入框内容缩进
    self.password2InputView.delegate = self;
    self.password2InputView.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    // 输入框左边是password图标
    self.password2InputView.iconImage = [UIImage imageNamed:@"op_icon_password"];
    self.password2InputView.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.password2InputView.secureTextEntry = YES;
    
    
    NSString *imgUrl = [OpenSDK_DataManager getSlideImageUrl];
    if (imgUrl == nil || imgUrl.length == 0) {
        // 没有图片地址，则隐藏滑块解锁
        self.slideView.hidden = YES;
    } else {
        self.slideView.sliderThumbImage = [UIImage imageNamed:@"op_img_slider"];
        self.slideView.sliderTipText = @">>>请拖动滑块完成拼图>>>";
        self.slideView.sliderSuccessText = @"完成";
        self.slideView.errorResetCount = 3;
        self.slideView.indentifyCompletion = ^(OpenSDK_SlideView* view, BOOL success) {
            NSLog(@"Identify %@",(success ? @"success" : @"fail"));
            if (success) {
                [[[OpenSDK_iToast makeText:@"验证成功"] setDuration:2000] show];
            } else {
                [[[OpenSDK_iToast makeText:@"验证失败，请重新拖动滑块"] setDuration:2000] show];
            }
        };
        self.slideView.loadNewImage = ^(OpenSDK_SlideView *view) {
            // 加载新图片
            [view setImageWithUrl:imgUrl];
        };
        // 滑块验证控件为隐藏模式
        [self.slideView setHideMode:^(BOOL display) {
            if (display) {
                self.mailInputView.hidden = YES;
                self.passwordInputView.hidden = YES;
                self.password2InputView.hidden = YES;
            } else {
                self.mailInputView.hidden = NO;
                self.passwordInputView.hidden = NO;
                self.password2InputView.hidden = NO;
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.slideView setImageWithUrl:[OpenSDK_DataManager getSlideImageUrl]];
}

#pragma mark - btn callback
- (IBAction)didClickBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)didClickContractBtn:(id)sender {
    // 收起键盘
    [self didHideKeyboard:sender];
    //跳转到协议界面
    [[OpenSDK_UIManager getInstance] showNextUIWithStoryBoard:@"LoginMain" withVCId:@"OpenSDK_ContactView"];
}

- (IBAction)didClickRegitser:(id)sender {
    // 收起键盘
    [self didHideKeyboard:sender];
    
    NSString *mail = self.mailInputView.text;
    if (mail == nil || mail.length == 0) {
        [[[OpenSDK_iToast makeText:@"抱歉，请输入邮箱"] setDuration:2000] show];
        return;
    }
    // 判断邮箱格式
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if(![emailTest evaluateWithObject:mail]) {
        [[[OpenSDK_iToast makeText:@"抱歉，请输入正确的邮箱"] setDuration:2000] show];
        return;
    }
    
    // 判断密码
    NSString *password = self.passwordInputView.text;
    if (password == nil || password.length == 0) {
        [[[OpenSDK_iToast makeText:@"抱歉，请输入密码"] setDuration:2000] show];
        return;
    }
    if(password.length < 6 || password.length > 15) {
        [[[OpenSDK_iToast makeText:@"抱歉，密码长度不符合要求"] setDuration:2000] show];
        return;
    }
    
    // 判断两次密码是否一致
    if(![password isEqualToString:self.password2InputView.text]) {
        [[[OpenSDK_iToast makeText:@"抱歉，两次密码输入不一致"] setDuration:2000] show];
        return;
    }
    
    // 判断滑块验证
    if(!self.slideView.hidden && !self.slideView.isSuccess) {
        [[[OpenSDK_iToast makeText:@"抱歉，请完成滑块验证"] setDuration:2000] show];
        return;
    }
    
    // 先弹出loading框
    UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"LoginMain" bundle:nil];
    OpenSDK_LoginLoadingView *vc = (OpenSDK_LoginLoadingView *) [loginStoryboard instantiateViewControllerWithIdentifier:@"OpenSDK_LoginLoadingView"];
    vc.accoutnName = mail;
    [self.navigationController pushViewController:vc animated:NO];
    
    // 再调用注册接口
    [[OpenSDK_LoginManager getInstance] registerWithMail:mail password:password delegate:^(BOOL result, OpenSDK_AccountRecord *account, NSString *err) {
        if (!result) {
            // 注册失败
            [[[OpenSDK_iToast makeText:err] setDuration:2000] show];
        }
        [vc loginResultCallback:result account:account];
    }];
}

@end
