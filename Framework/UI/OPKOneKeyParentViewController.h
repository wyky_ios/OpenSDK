//
//  OPKOneKeyParentViewController.h
//  YCOpenSDK
//
//  Created by MacPro on 2020/12/28.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_BaseKeyboradView.h"

NS_ASSUME_NONNULL_BEGIN

@interface OPKOneKeyParentViewController : OpenSDK_BaseKeyboradView
@property (unsafe_unretained, nonatomic) IBOutlet UIView *bgView;

@end

NS_ASSUME_NONNULL_END
