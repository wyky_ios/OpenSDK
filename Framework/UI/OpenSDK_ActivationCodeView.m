//
//  OpenSDK_ActivationCodeView.m
//  OpenSDK
//
//  Created by 涂俊 on 2018/10/10.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import "OpenSDK_ActivationCodeView.h"
#import "OpenSDK_Toast.h"
#import "OpenSDK_LoginManager.h"
#import "OpenSDK_UIManager.h"

@interface OpenSDK_ActivationCodeView ()

@end

@implementation OpenSDK_ActivationCodeView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _bgView.backgroundColor = UIColor.whiteColor;
    _bgView.layer.cornerRadius = 10;
    _bgView.layer.masksToBounds = true;
    
    // 输入框内容缩进
    self.codeInputView.delegate = self;
    self.codeInputView.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)didClickOKBtn:(id)sender {
    //收起键盘
    [self didHideKeyboard:sender];
    
    NSString *code = self.codeInputView.text;
    if (code == nil || code.length == 0) {
        [[[OpenSDK_iToast makeText:@"抱歉，请输入激活码"] setDuration:2000] show];
        return;
    }
    
    [self.okBtn setEnabled:NO];
    // 调用绑定激活码接口
    [[OpenSDK_LoginManager getInstance] bindActivationCode:code delegate:^(BOOL result, NSString *err) {
        [self.okBtn setEnabled:YES];
        if (!result) {
            // 绑定激活码失败
            [[[OpenSDK_iToast makeText:err] setDuration:2000] show];
        } else {
            // 绑定激活码成功
            [[[OpenSDK_iToast makeText:@"激活成功"] setDuration:3000] show];
            // 跳转登录后续界面
            [self dismissViewControllerAnimated:NO completion:^{
                [[OpenSDK_UIManager getInstance] showLoginUI];
            }];
        }
    }];
}

@end
