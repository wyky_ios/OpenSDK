//
//  OpenSDK_RealNameMobileView.m
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/30.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import "OpenSDK_RealNameMobileView.h"
#import "OpenSDK_DataManager.h"
#import "OpenSDK_LoginManager.h"
#import "OpenSDK_UIManager.h"
#import "OpenSDK_DataKeys.h"
#import "OpenSDK_Toast.h"
#import "OpenSDK_Proxy.h"
#import "OpenSDK_StringUtil.h"

@interface OpenSDK_RealNameMobileView ()

@end

@implementation OpenSDK_RealNameMobileView

{
    int smsTimeTotal;
    NSTimer *timer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _bgView.backgroundColor = UIColor.whiteColor;
    _bgView.layer.cornerRadius = 10;
    _bgView.layer.masksToBounds = true;
    // 输入框内容缩进
    self.realnameInputView.delegate = self;
    self.realnameInputView.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    // 输入框左边是mail图标
    self.realnameInputView.iconImage = [UIImage imageNamed:@"op_icon_realname"];
    
    // 输入框内容缩进
    self.cardInputView.delegate = self;
    self.cardInputView.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    // 输入框左边是mail图标
    self.cardInputView.iconImage = [UIImage imageNamed:@"op_icon_card"];
    
    // 输入框内容缩进
    self.mobileInputView.delegate = self;
    self.mobileInputView.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    // 输入框左边是mail图标
    self.mobileInputView.iconImage = [UIImage imageNamed:@"op_icon_mobile"];
    
    // 输入框内容缩进
    self.codeInputView.delegate = self;
    self.codeInputView.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    // 输入框左边是mail图标
    self.codeInputView.iconImage = [UIImage imageNamed:@"op_icon_sms"];
    
    // 点击背景，收起输入法
    [self.bgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didHideKeyboard:)]];
    
    //判断是否强制实名认证
    OpenSDK_AccountRecord *curAccount = [[OpenSDK_DataManager getInstance] getCurAccount];
    if (curAccount != nil && curAccount.needReal == 2) {
        self.closeBtn.hidden = YES;// 强制实名认证，不显示关闭按钮
    } else {
        self.closeBtn.hidden = NO;// 非强制实名认证，显示关闭按钮
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


 #pragma mark - btn callback
- (IBAction)didClickCloseBtn:(id)sender {
    [[OpenSDK_UIManager getInstance] closeUI];
    // 实名认证成功(或直接关闭)，发出登录成功事件
    [[OpenSDK_LoginManager getInstance] sendLoginSuccessEvent];
}

- (IBAction)didClickCodeBtn:(id)sender {
    NSString *mobile = self.mobileInputView.text;
    if (mobile == nil || mobile.length == 0) {
        [[[OpenSDK_iToast makeText:@"手机号不能为空"] setDuration:2000] show];
        return;
    }
    
    // 按钮清空，转菊花
    [self.codeBtn setEnabled:NO];
    [self.codeBtn setTitle: @"" forState: UIControlStateNormal];
    [self.smsIndicator startAnimating];
    // 输入框获取焦点
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.codeInputView becomeFirstResponder];
    });
    [[OpenSDK_LoginManager getInstance] sendSMS:mobile smsType:OpenSDK_S_SMS_MOBILE_REGISTER delegate:^(BOOL result, long validTime, NSString *err) {
        [self.smsIndicator stopAnimating];
        if (result) {
            [[[OpenSDK_iToast makeText:@"短信验证码已发送，请注意查收"] setDuration:2000] show];
            // 短信验证码发送成功，开始倒计时
            self -> smsTimeTotal = validTime / 1000.0;
            self -> timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                     target:[OpenSDK_Proxy proxyWithTarget:self]
                                                   selector:@selector(changeTimeWords)
                                                   userInfo:nil
                                                    repeats:YES];
            [self.codeBtn setTitle: [NSString stringWithFormat:@"%d", self -> smsTimeTotal] forState: UIControlStateNormal];
            [self.codeBtn setEnabled:NO];
        } else {
            if (err != nil) {
                [[[OpenSDK_iToast makeText:err] setDuration:2000] show];
            }
            self -> smsTimeTotal=0;
            [self.codeBtn setTitle: @"重新发送" forState: UIControlStateNormal];
            [self.codeBtn setEnabled:YES];
        }
    }];
}

- (IBAction)didClickOkBtn:(id)sender {
    //收起键盘
    [self didHideKeyboard:sender];
    
    NSString *name = self.realnameInputView.text;
    if (name == nil || name.length == 0) {
        [[[OpenSDK_iToast makeText:@"抱歉，请输入真实姓名"] setDuration:2000] show];
        return;
    }
    
    NSString *card = self.cardInputView.text;
    if (card == nil || card.length == 0) {
        [[[OpenSDK_iToast makeText:@"抱歉，请输入身份证号"] setDuration:2000] show];
        return;
    }
    
    if (![OpenSDK_StringUtil validateIDCardNumber:card]){
        [[[OpenSDK_iToast makeText:@"抱歉，请输入正确身份证号"] setDuration:2000] show];
        return;
    }
    
    NSString *mobile = self.mobileInputView.text;
    if (mobile == nil || mobile.length == 0) {
        [[[OpenSDK_iToast makeText:@"手机号不能为空"] setDuration:2000] show];
        return;
    }
    
    NSString *security = self.codeInputView.text;
    if (security == nil || security.length == 0) {
        [[[OpenSDK_iToast makeText:@"请输入短信验证码"] setDuration:2000] show];
        return;
    }
    
    [self.okBtn setEnabled:NO];
    
    [[OpenSDK_LoginManager getInstance] realnameWithName:name card:card phone:mobile securityCode:security delegate:^(BOOL result, NSString *err) {
        [self.okBtn setEnabled:YES];
        if (!result) {
            [[[OpenSDK_iToast makeText:err] setDuration:2000] show];
        } else {
            [[[OpenSDK_iToast makeText:@"实名认证成功"] setDuration:2000] show];
            [self didClickCloseBtn:nil];
        }
    }];
}


 #pragma mark - timer callback
// 定时器回调函数，每秒回调
- (void) changeTimeWords {
    if(smsTimeTotal > 0) {
        smsTimeTotal--;
        [self.codeBtn setTitle: [NSString stringWithFormat:@"%d",smsTimeTotal] forState: UIControlStateNormal];
        [self.codeBtn setEnabled:NO];
    } else {
        // 计时结束
        if (timer!=nil) {
            [timer invalidate];
        }
        smsTimeTotal=0;
        [self.codeBtn setTitle: @"重新发送" forState: UIControlStateNormal];
        [self.codeBtn setEnabled:YES];
    }
}

@end
