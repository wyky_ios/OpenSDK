//
//  OpenSDK_LoginLoadingView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/20.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_AccountRecord.h"
#import "OpenSDK_PayInfo.h"

@interface OpenSDK_LoginLoadingView : UIViewController

//是否隐藏"切换账号"按钮
@property (nonatomic, readwrite) BOOL isHideSwitchAccount;

//是否从支付进入的绑定手机界面，会有订单信息
@property (nonatomic, readwrite) OpenSDK_PayInfo *order;

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UIImageView *loadingIconView;

@property (weak, nonatomic) IBOutlet UIButton *switchAccountBtn;

@property (weak, nonatomic) IBOutlet UILabel *loadingMsgView;

@property (weak, nonatomic) IBOutlet UILabel *loginSuccessView;

@property (nonatomic, strong) NSString *accoutnName;

//登录结果回调
- (void)loginResultCallback:(BOOL) result account:(OpenSDK_AccountRecord *)account;

//登录结果回调(带delegate,登录失败，关闭界面，会回调delegate)
- (void)loginResultCallback:(BOOL) result account:(OpenSDK_AccountRecord *)account delegate:(void (^ __nullable)(void))delegate;

@end
