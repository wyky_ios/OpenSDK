//
//  OpenSDK_LoginMailView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/20.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_TextField.h"
#import "OpenSDK_BaseKeyboradView.h"

@interface OpenSDK_LoginMailView : OpenSDK_BaseKeyboradView

@property (weak, nonatomic) IBOutlet OpenSDK_TextField *mailInputView;
@property (weak, nonatomic) IBOutlet OpenSDK_TextField *passwordInputField;
@property (weak, nonatomic) IBOutlet UILabel *registerMsgView;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) IBOutlet UIButton *passwordVisibleBtn;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *otherLoginBtn;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *bgView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *backBtn;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *closeBtn;

@end
