//
//  OpenSDK_BaseKeyboradView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/30.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSDK_BaseKeyboradView : UIViewController <UITextFieldDelegate>

// 键盘相对于targetView的额外的间隔距离,默认20
@property (nonatomic, readwrite) CGFloat keyboardExtraMargin;

/**
 * 键盘弹出时，如果targetView被遮挡，targetView会整体上移，直到键盘和targetView距离为keyboardExtraMargin
 * 如果targetView为空，则默认以获取焦点的输入框为targetView
 */
@property (nonatomic, weak, readwrite) UIView *keyboardTargetView;

// 收起键盘
-(void)didHideKeyboard:(id)sender;

@end
