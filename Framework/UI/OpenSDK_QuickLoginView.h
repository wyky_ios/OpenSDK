//
//  OpenSDK_QuickLoginView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/16.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSDK_QuickLoginView : UIViewController

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UIImageView *logoImgView;

@property (weak, nonatomic) IBOutlet UIView *curAccountBg;

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *curAccountBGImageView;

@property (weak, nonatomic) IBOutlet UIImageView *curLoginTypeView;

@property (weak, nonatomic) IBOutlet UILabel *curLoginAccountView;

@property (weak, nonatomic) IBOutlet UIButton *historyAccountBtn;

@property (weak, nonatomic) IBOutlet UIButton *historyBtn;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *otherButton;

@end
