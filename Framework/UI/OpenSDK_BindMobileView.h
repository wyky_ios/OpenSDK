//
//  OpenSDK_BindMobileView.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/26.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSDK_TextField.h"
#import "OpenSDK_BaseKeyboradView.h"
#import "OpenSDK_PayInfo.h"

@interface OpenSDK_BindMobileView : OpenSDK_BaseKeyboradView

//是否从支付进入的绑定手机界面，会有订单信息
@property (nonatomic, readwrite) OpenSDK_PayInfo *order;

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet OpenSDK_TextField *mobileInputView;

@property (weak, nonatomic) IBOutlet UIButton *bindBtn;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@end
