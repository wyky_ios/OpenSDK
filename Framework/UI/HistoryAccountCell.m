//
//  HistoryAccountCell.m
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/18.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import "HistoryAccountCell.h"
#import "OpenSDK_ConfirmDialog.h"
#import "OpenSDK_DataManager.h"

@implementation HistoryAccountCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setAccount:(OpenSDK_AccountRecord *)account {
    _account = account;
    NSString *userName = account.accountName;
    if (userName == nil  || [userName isEqual:@""]) {
        userName = account.accountValue;
    }
    self.accountNameView.text = userName;
    switch (account.accountType) {
        case OpenSDKAccountTypeDynamic:
            self.accountTypeView.image = [UIImage imageNamed:@"op_icon_mobile"];
            break;
        case OpenSDKAccountTypeMobile:
            self.accountTypeView.image = [UIImage imageNamed:@"op_icon_mail"];
            break;
        case OpenSDKAccountTypeMail:
            self.accountTypeView.image = [UIImage imageNamed:@"op_icon_mail"];
            break;
        case OpenSDKAccountTypeGuest:
            self.accountTypeView.image = [UIImage imageNamed:@"op_icon_guest"];
            break;
        case OpenSDKAccountTypeOther:
            if ([account.platform isEqual:@"WeChatLogin"]) {
                self.accountTypeView.image = [UIImage imageNamed:@"op_icon_wx"];
            } else if ([account.platform isEqual:@"AppleLogin"]) {
                self.accountTypeView.image = [UIImage imageNamed:@"op_icon_apple"];
            } else if ([account.platform isEqual:@"QQ"]) {
                self.accountTypeView.image = [UIImage imageNamed:@"op_icon_qq"];
            } else if ([account.platform isEqual:@"OneKey"]) {
                self.accountTypeView.image = [UIImage imageNamed:@"op_icon_onekey"];
            } else {
                self.accountTypeView.image = nil;
            }
            break;
    }
}

- (IBAction)didClickDeleteBtn:(id)sender {
    NSString *msgWorld = nil;
    if (self.account.accountType == OpenSDKAccountTypeGuest) {
        msgWorld = @"您的游客账号还未绑定手机号，删除后账号会丢失";
    } else {
        msgWorld = @"是否确认删除该账号的登录记录？";
    }
    [OpenSDK_ConfirmDialog showWithMessage:msgWorld
                                 okBtnName:@"删除"
                             cancelBtnName:@"取消"
                                  delegate:^(BOOL result) {
        if (result) {
//            [[OpenSDK_DataManager getInstance] removeAccount:self.account];
            if (self.deleteDelegate) {
                [self.deleteDelegate onDelete:self.account];
            }
        }
    } root:self.vc];
}

@end
