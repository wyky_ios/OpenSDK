//
//  OpenSDK_StatusManager.m
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/8/16.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import "OpenSDK_StatusManager.h"

@implementation OpenSDK_StatusManager

static OpenSDK_StatusManager *instance = nil;

+(instancetype) getInstance
{
    if(instance==nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            instance = [[self alloc] init];
        });
    }
    return instance;
}

@end
