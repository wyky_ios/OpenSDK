//
//  OpenSDK_UIManager.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/9/7.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "OpenSDK_PayInfo.h"
#import "OpenSDK_AccountRecord.h"

@interface OpenSDK_UIManager : NSObject

+(instancetype) getInstance;

//跳转到下一个UI界面
-(void) showNextUIWithViewController:(UIViewController *)vc;

//跳转到下一个UI界面
-(void) showNextUIWithStoryBoard:(NSString *)storyboard withVCId:(NSString *)vcId;

// 跳转，如果已经有vc在栈中，则pop
- (void)showViewControllerWithSBId:(NSString *)sbId vcid:(NSString *)vcid;

// 跳转，如果已经有vc在栈中，则pop
- (void)showViewController:(UIViewController *)viewController;

//初始化登录UI(登录入口，进行登录前的各种前置判断)
-(void) initLoginUI;

//展示登录UI
-(void) showLoginUI;

// 展示切换账号UI
- (void)showSwitchAccountUI;

// 展示实名认证UI
- (void)showRealUI;

// 展示绑定手机
- (void)showBindPhoneUI;

// 展示默认首要登录页面
- (void)showDefaultLoginView;

// 展示一键登录页面
- (void)showOneKeyLogin;

- (void)showMoboleViewControllerWithAccount:(OpenSDK_AccountRecord *)account canBack:(BOOL)canBack;

//关闭登录UI
-(void) closeUI;

- (void)goBack;

//初始化支付UI(支付入口，进行支付前的各种前置判断)
-(void) initPayUIWithPayInfo:(OpenSDK_PayInfo *)payInfo;

//直接调起支付，展示支付界面
-(void) showPayUI:(OpenSDK_PayInfo *)payInfo;


-(void) loginInvalid;

- (UINavigationController *)navigationController;


//统一处理登录非此即彼显示方式
- (void)handleOtherLoginTypesWithButton:(UIButton *)button curLoginType:(NSString *)curLoginType;

//统一处理登录非此即彼跳转和登录
- (void)handleOtherLoginMethodWithCurLoginType:(NSString *)curLoginType contranctChecked:(BOOL)contranctChecked;


@end
