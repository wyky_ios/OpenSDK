//
//  SSDKEventManager.h
//  SuperSDK
//
//  Created by wyht－ios－dev on 16/7/18.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenSDK_EventManager : NSObject

@property (nonatomic, strong, readwrite) NSMutableDictionary *eventRecord;

+ (id) getInstance;

+ (void) addObsver:(id)notificationObserver
          selector:(SEL)notificationSelector
              name:(NSString *)notificationName
            object:(id)notificationSender
         className:(NSString *)className;

+ (BOOL) isSupportEvent:(NSString *)className
              eventName:(NSString *)eventName;

+ (void) postNotificationName:(NSString *)aName
                       object:(nullable id)anObject;

//返回对应SDK名称的SDK实例
+(id) getSDKWithName:(NSString *) sdkName;

@end
