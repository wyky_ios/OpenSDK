//
//  OpenSDK_StatusManager.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/8/16.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenSDK_StatusManager : NSObject

//支付过程标志位
@property (atomic, readwrite) BOOL payStatus;

+(instancetype) getInstance;

@end
