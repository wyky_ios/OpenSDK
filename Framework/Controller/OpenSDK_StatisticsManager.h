//
//  OpenSDK_StatisticsManager.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/10/12.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenSDK_StatisticsManager : NSObject


/**
 * 统计数据类型
 */
extern NSString * const OPENSDK_STATS_LOGIN_FAIL;// 登录失败
extern NSString * const OPENSDK_STATS_LOGIN_SUCCESS;// 登录成功
extern NSString * const OPENSDK_STATS_REGISTER_FAIL;// 注册失败
extern NSString * const OPENSDK_STATS_REGISTER_SUCCESS;// 注册成功
extern NSString * const OPENSDK_STATS_TEMPORARY_BIND_FAIL;// 游客绑定失败
extern NSString * const OPENSDK_STATS_TEMPORARY_BIND_SUCCESS;// 游客绑定失败
extern NSString * const OPENSDK_STATS_PAY_SELECT_UI;// 展示支付方式选择界面
extern NSString * const OPENSDK_STATS_PAY_CANCEL;// 支付取消
extern NSString * const OPENSDK_STATS_CREATE_ORDER_SUCCESS;// 创建订单成功
extern NSString * const OPENSDK_STATS_CREATE_ORDER_FAIL;// 创建订单失败
extern NSString * const OPENSDK_STATS_PAY_SUB;//选择支付方式
extern NSString * const OPENSDK_STATS_PAY_SUB_SUCCESS;// 支付成功
extern NSString * const OPENSDK_STATS_PAY_SUB_FAIL;// 支付失败
extern NSString * const OPENSDK_STATS_PAY_SUB_CHECK_SUCCESS;// 支付确认成功
extern NSString * const OPENSDK_STATS_PAY_SUB_CHECK_FAIL;// 支付确认失败

/**
 * 统计detail字段的值
 */
extern NSString * const OPENSDK_STATS_DETAIl_LOGIN_PHONE;// 登录统计详情：手机登录
extern NSString * const OPENSDK_STATS_DETAIl_LOGIN_EMAIL;// 登录统计详情：邮箱登录
extern NSString * const OPENSDK_STATS_DETAIl_LOGIN_TEMP;// 登录统计详情：游客登录
extern NSString * const OPENSDK_STATS_DETAIl_LOGIN_AUTO;// 登录统计详情：快捷登录
extern NSString * const OPENSDK_STATS_DETAIl_PAY_CANCEL_UI;// 支付统计详情：支付方式选择UI取消
extern NSString * const OPENSDK_STATS_DETAIl_PAY_CANCEL_BIND;// 支付统计详情：支付绑定取消

+(instancetype) getInstance;


// 收集统计数据（通用）
-(void) collectWithType:(NSString *)type data:(NSDictionary *)data;

// 记录登录开始时间(调起登录界面)
-(void) startLogin;

// 收集登录成功\注册成功\绑定成功的统计数据（不立即上报，调用sendLoginSuccessStatistics再上报）
-(void) collectLoginSuccessWithType:(NSString *)type detail:(NSString *)detail;

// 上报登录成功\注册成功\绑定成功的统计数据
-(void) sendLoginSuccessStatistics;

// 收集登录失败\注册失败\绑定失败的统计数据
-(void) collectLoginFailWithType:(NSString *)type
                          detail:(NSString *)detail
                        failCode:(long)code
                        failInfo:(NSString *)info;


// 收集展示支付界面/选择支付方式的统计数据
-(void) collectPayActionWithType:(NSString *)type
                      cpOrderId:(NSString *)cpOrderId
                         detail:(NSString *)detail;

// 收集创建订单成功\支付成功\支付校验成功的统计数据
-(void) collectPaySuccessWithType:(NSString *)type
                         orderId:(NSString *)orderId
                       cpOrderId:(NSString *)cpOrderId
                          detail:(NSString *)detail;

// 收集创建订单失败\支付失败\支付校验失败的统计数据
-(void) collectPayFailWithType:(NSString *)type
                      orderId:(NSString *)orderId
                    cpOrderId:(NSString *)cpOrderId
                       detail:(NSString *)detail
                     failInfo:(NSString *) info;

// 收集支付取消的统计数据
-(void) collectPayCancelWithCpOrderId:(NSString *)cpOrderId
                              detail:(NSString *)detail;
@end
