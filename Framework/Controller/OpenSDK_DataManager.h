//
//  OpenSDK_DataManager.h
//
//
//  Created by wyht－ios－dev on 16/2/24.
//
//

#import <Foundation/Foundation.h>

#import "OpenSDK_AccountRecord.h"

@interface OpenSDK_DataManager : NSObject

//opensdk地址
@property (nonatomic, strong, readwrite) NSString *url;

//opensdk版本号
@property (nonatomic, strong, readwrite) NSString *version;

@property (nonatomic, strong, readwrite) NSString *appID;
//RSA的公钥
@property (nonatomic, strong, readwrite) NSString *appPubKey;
@property (nonatomic, strong, readwrite) NSString *channelID;
@property (readwrite) BOOL autoLoginFlag;
//是否需要激活码(当前只有验证设备的情况)
@property (readwrite) BOOL needActiveCodeFlag;
//是否禁止游客支付
@property (readwrite) BOOL forbidGuestPayFlag;
//服务器返回的affect数据
@property (nonatomic, strong, readwrite) NSString *activeAffectData;
//国家区号
@property (nonatomic, strong, readwrite) NSArray *countryCodeInfo;


//本地加载的所有SDK(key:类名,value:类对象)
@property (nonatomic, strong, readwrite) NSDictionary *supportSDKs;
//本地加载的所有SDK(key:服务器标识,value:类对象)
@property (nonatomic, strong, readwrite) NSDictionary *supportSDKServerNames;

//最终支持的Xyz类型(服务器端配置+本地配置求交集)
@property (nonatomic, strong, readwrite) NSArray *supportXyzTypes;
//最终支持的登录类型(服务器端配置+本地配置求交集)
@property (nonatomic, strong, readwrite) NSArray *supportLoginTypes;
//最终支持的注册类型(服务器端配置+本地配置求交集)
@property (nonatomic, strong, readwrite) NSArray *supportRegisterTypes;

//当前登录账号
@property (nonatomic, strong) OpenSDK_AccountRecord *currentAccount;

//预登录账号
@property (nonatomic, strong) OpenSDK_AccountRecord *preLoginAccount;
//账号历史记录
@property (nonatomic, strong, readwrite) NSMutableDictionary *historyAccounts;

//滑块解锁图片地址
@property (nonatomic, strong, readwrite) NSString *imgVerificationUrl;

// 是否显示用户协议
@property (nonatomic, assign)BOOL showContranct;

// 用户协议选择状态
@property (nonatomic, assign)BOOL contranctChecked;

// 用户协议web地址
@property (nonatomic, copy)NSString *contractWebUrl;

// 隐私协议web地址
@property (nonatomic, copy)NSString *privacyWebUrl;


@property (nonatomic, strong)NSString *uiStyle;

@property (nonatomic, strong)NSString *userCenterUrl;

/// 验签key
@property (nonatomic, copy) NSString *secret_key;

//  审核状态 true, 非审核false
@property (nonatomic, assign)BOOL isAppstoreReview;

@property (nonatomic, assign)BOOL isPayToastEnable;

+(instancetype) getInstance;

//获取初始化地址
+(NSString*) getInitUrl;
//获取用户注册地址
+(NSString*) getRegisterUrl;
//获取用户信息绑定地址
+(NSString*) getUserBindingUrl;
//获取用户信息确认地址
+(NSString*) getUserCheckUrl;
//获取支付的地址
+(NSString*) getPayUrl;
//获取设备唯一标识
+(NSString *) getDeviceID;
//获取游戏协议地址
+(NSString *) getProtrolUrl;
//获取用户隐私
+(NSString *) getPrivacyUrl;
//获取绑定激活地址
+(NSString *) getBindActiveCodeUrl;
//获取查询激活状态地址
+(NSString *) getQueryActiveStatusUrl;
//获取没有订单号进行补单的地址
+(NSString *) getOrderCheckUrl;
//获取检查苹果支付订单地址
+(NSString *) getAppleOrderCheckUrl;
//获取查找密码的地址
+(NSString *) getFindPasswordUrl;
//获取游客用户ID
+(NSString *) getTemporaryUserID;
//获取发送验证码的地址
+(NSString *) getSMSUrl;
//获取手机重置密码的地址
+(NSString *) getResetPasswordUrl;
//获取实名制验证地址
+(NSString *) getUserRealNameUrl;
//获取商品列表的地址
+(NSString *) getProductUrl;
//获取实名制验证地址
+(NSString *) getSlideImageUrl;
// 获取UI Style
+(NSString *)getUIStyle;
// 获取用户中心地址
+(NSString *)getUserCenterUrl;

//获取本地的国家编码
-(NSDictionary *) getCountryCodeFromCurMachine;

// 兑换订单Check
+(NSString *) getApplePromoOrderCheckUrl;

#pragma mark - CurrentAccount 当前账号
- (void) setCurAccount:(OpenSDK_AccountRecord *)account;
- (OpenSDK_AccountRecord *) getCurAccount;

#pragma mark - CurrentAccount 预登登录账号
- (void)setPreLoginAccount:(OpenSDK_AccountRecord *)account;
- (OpenSDK_AccountRecord *)getPreLoginAccount;

#pragma mark - HistoryAccount 账号历史记录
- (void)addAccount:(OpenSDK_AccountRecord *) account;
- (void)removeAccount:(OpenSDK_AccountRecord *) account;
- (void)removeAccountByOpenId:(NSString *) openId;
- (NSArray *)getAccountList:(OpenSDK_AccountRecord *) account;
// 获取全部账号
- (NSArray *)getAccountList;
@end

