//
//  OpenSDK_EventManager.m
//  SuperSDK
//
//  Created by wyht－ios－dev on 16/7/18.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import "OpenSDK_EventManager.h"
#import "OpenSDK_DataManager.h"
#import "OpenSDK_DataKeys.h"
#import "OpenSDK_SdkProtrol.h"

@implementation OpenSDK_EventManager

static OpenSDK_EventManager *instance = nil;
static NSString *EVENT_RECORD=@"_Event";

+ (id) getInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

//添加事件监听
+ (void) addObsver:(id)notificationObserver selector:(SEL)notificationSelector name:(NSString *)notificationName object:(id)notificationSender className:(NSString *)className {
    [[NSNotificationCenter defaultCenter] addObserver:notificationObserver selector:notificationSelector name:notificationName object:notificationSender];
    if([[OpenSDK_EventManager getInstance] eventRecord]==nil) {
        [[OpenSDK_EventManager getInstance] setEventRecord:[[NSMutableDictionary alloc] init]];
    }
    if(![[[[OpenSDK_EventManager getInstance] eventRecord] allKeys] containsObject:className]) {
        [[[OpenSDK_EventManager getInstance] eventRecord] setObject:notificationObserver forKey:className];
        [[[OpenSDK_EventManager getInstance] eventRecord] setObject:[[NSMutableArray alloc] init] forKey:[className stringByAppendingString:EVENT_RECORD]];
    }
    NSMutableArray *classEventRecord=[[[OpenSDK_EventManager getInstance] eventRecord] objectForKey:[className stringByAppendingString:EVENT_RECORD]];
    if(![classEventRecord containsObject:notificationName])
        [classEventRecord addObject:notificationName];
    [[[OpenSDK_EventManager getInstance] eventRecord] setObject:classEventRecord forKey:[className stringByAppendingString:EVENT_RECORD]];
}

//查询是否监听了事件
+ (BOOL) isSupportEvent:(NSString *)className eventName:(NSString *)eventName {
    if(className==nil)
        return NO;
    if([[OpenSDK_EventManager getInstance] eventRecord]!=nil) {
        NSMutableArray *classEventRecord=[[[OpenSDK_EventManager getInstance] eventRecord] objectForKey:[className stringByAppendingString:EVENT_RECORD]];
        if(classEventRecord!=nil) {
            if([classEventRecord containsObject:eventName])
                return YES;
            else
                return NO;
        } else {
            return NO;
        }
    } else {
        return NO;
    }
}

+ (void) postNotificationName:(NSString *)aName object:(id)anObject {
    [OpenSDK_EventManager initAllObject];
    [[NSNotificationCenter defaultCenter] postNotificationName:aName object:anObject];
}

//post第一个消息前初始化所有的对象，并注册绑定
+ (void) initAllObject {
    if ([[OpenSDK_EventManager getInstance] eventRecord]==nil) {
        [[OpenSDK_EventManager getInstance] setEventRecord:[[NSMutableDictionary alloc] init]];
    }
    //只初始化的时候执行一次
    if ([[OpenSDK_EventManager getInstance] eventRecord].count==0) {
        NSDictionary *supportSDKs = [OpenSDK_DataManager getInstance].supportSDKs;
        for (NSString *sdkName in [supportSDKs allKeys]) {
            if(![[[[OpenSDK_EventManager getInstance] eventRecord] allKeys] containsObject:sdkName]) {
                [[[OpenSDK_EventManager getInstance] eventRecord] setObject:[supportSDKs objectForKey:sdkName] forKey:sdkName];
                [[[OpenSDK_EventManager getInstance] eventRecord] setObject:[[NSMutableArray alloc] init] forKey:[sdkName stringByAppendingString:EVENT_RECORD]];
                [(id<OpenSDK_SdkProtrol>)[[[OpenSDK_EventManager getInstance] eventRecord] objectForKey:sdkName] initInfo];
            }
        }
    }
}

+(id) getSDKWithName:(NSString *)sdkName {
    [OpenSDK_EventManager initAllObject];
    return [[[OpenSDK_EventManager getInstance] eventRecord] objectForKey:sdkName];
}

@end

