//
//  OpenSDK_LoginManager.h
//  OpenSDK
//
//  Created by 涂俊 on 2018/7/19.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_AccountRecord.h"

@interface OpenSDK_LoginManager : NSObject

@property (nonatomic, strong, readwrite) NSString * curOpenId;
@property (nonatomic, strong, readwrite) NSString * curToken;

+(instancetype) getInstance;

/**
 * 发出登录成功事件(由UI层调用，决定登录事件时机)
 */
-(void) sendLoginSuccessEvent;

/**
 * 发出登录取消事件(由UI层调用，决定登录事件时机)
 */
-(void) sendLoginCacelEvent;

/**
 * 快捷登录
 */
-(void) loginWithRecord:(OpenSDK_AccountRecord *)record
               delegate:(void (^)(BOOL result, OpenSDK_AccountRecord *account, long errorNo, NSString *err))delegate;

/**
 * 游客登录
 */
-(void) loginWithGuest:(void (^)(BOOL result, OpenSDK_AccountRecord *account, long errorNo, NSString *err))delegate;

// 一键登录
-(void) loginWithOneKeyToken:(NSString *)token
                    delegate:(void (^)(BOOL result, OpenSDK_AccountRecord *account, long errorNo, NSString *err))delegate;

/**
 * 手机号验证码登录
 */
-(void) loginWithPhone:(NSString *)phone
          securityCode:(NSString *)securityCode
              delegate:(void (^)(BOOL result, OpenSDK_AccountRecord *account, long errorNo, NSString *err))delegate;
/**
 * 邮箱登录
 */
-(void) loginWithMail:(NSString *)mail
             password:(NSString *)password
             delegate:(void (^)(BOOL result, OpenSDK_AccountRecord *account, long errorNo, NSString *err))delegate;

/**
 * 手机号密码登录
 */
-(void) loginWithMobile:(NSString *)mobile
             password:(NSString *)password
               delegate:(void (^)(BOOL result, OpenSDK_AccountRecord *account, long errorNo, NSString *err))delegate;

/**
 * 第三方平台登录
 */
-(void) loginWithPlatform:(NSString *)serverName
                 delegate:(void (^)(BOOL result, OpenSDK_AccountRecord *account, long errorNo, NSString *err))delegate;


// 第三方平台验证
-(void) authWithPlatform:(NSString *)serverName
                delegate:(void (^)(BOOL result, NSDictionary *contet, long errorNo, NSString *err))delegate;
/**
 * 登出当前账号
 */
-(void) logout;

/**
 * 登出当前账号（本地直接登出，不调用接口）
 */
- (void)localLogout;

/**
 * 登出当前账号（本地直接登出，不调用接口）不清除本地记录
 */
- (void)localLogoutNotClean;

/**
 * 发送短信
 */
-(void) sendSMS:(NSString *)phone
        smsType:(NSString *)smsType
       delegate:(void (^)(BOOL result, long validTime, NSString *err))delegate;
/**
 * 绑定手机号
 */
-(void) bindWithPhone:(NSString *)phone
         securityCode:(NSString *)securityCode
             delegate:(void (^)(BOOL result, OpenSDK_AccountRecord *account, NSString *err))delegate;

/**
 * 邮箱注册
 */
-(void) registerWithMail:(NSString *)mail
                password:(NSString *)password
                delegate:(void (^)(BOOL result, OpenSDK_AccountRecord *account, NSString *err))delegate;

/**
 * 查询当前账号是否实名认证
 */
-(void) checkRealnameWithDelegate:(void (^)(BOOL result, BOOL isReal, NSString *err))delegate;

/**
 * 实名认证
 */
-(void) realnameWithName:(NSString *)name
                    card:(NSString *)card
                   phone:(NSString *)phone
            securityCode:(NSString *)securityCode
                delegate:(void (^)(BOOL result, NSString *err))delegate;

/**
 * 通过邮箱重置密码
 */
-(void) resetPassworkWithMail:(NSString *)mail
                     delegate:(void (^)(BOOL result, NSString *err))delegate;

/**
 * 查询绑定激活码信息
 */
-(void) checkActivationCodeWithDelegate:(void (^)(BOOL isBind, NSString *err))delegate;

/**
 * 绑定激活码
 */
-(void) bindActivationCode:(NSString *)code
                  delegate:(void (^)(BOOL isBind, NSString *err))delegate;

@end
