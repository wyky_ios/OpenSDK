//
//  OpenSDK_PayManager.h
//  OpenSDK
//
//  Created by wyht－ios－dev on 16/3/7.
//  Copyright © 2016年 WYHT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_PayInfo.h"
#import "OpenSDK_OrderInfo.h"

@interface OpenSDK_PayManager : NSObject

+(instancetype) getInstance;

/**
 * 回调支付取消(由UI层、或sdk内逻辑决定)
 */
-(void) callbackPayCancel;

// 回调游戏支付成功
-(void) callbackPaySuccess:(NSString *) msg
                 withOrder:(OpenSDK_OrderInfo *) order;

// 回调游戏支付失败
-(void) callbackPayFail:(NSString *) msg
              withOrder:(OpenSDK_OrderInfo *) order;

// 回调游戏支付状态待定
-(void) callbackPayWaitCheck:(OpenSDK_OrderInfo *) order;

/**
 * 调起支付
 */
-(void) pay:(OpenSDK_PayInfo *) payInfo serverName:(NSString *)serverName;

@end

