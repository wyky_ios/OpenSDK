//
//  OpenSDK_StatisticsManager.m
//  OpenSDK
//
//  Created by 涂俊 on 2018/10/12.
//  Copyright © 2018年 WYHT. All rights reserved.
//

#import "OpenSDK_StatisticsManager.h"
#import "OpenSDK.h"
#import "OpenSDK_DataManager.h"
#import "OpenSDK_LoginManager.h"

@implementation OpenSDK_StatisticsManager

{
    UInt64 loginStartTime;// 登录开始时间
    NSString *loginType;// 登录成功的类型
    NSMutableDictionary *loginData;//登录成功的上报数据
}

NSString * const OPENSDK_STATS_LOGIN_FAIL = @"OPENSDK_LOGIN_FAIL";// 登录失败
NSString * const OPENSDK_STATS_LOGIN_SUCCESS = @"OPENSDK_LOGIN_SUCCESS";// 登录成功
NSString * const OPENSDK_STATS_REGISTER_FAIL = @"OPENSDK_REGISTER_FAIL";// 注册失败
NSString * const OPENSDK_STATS_REGISTER_SUCCESS = @"OPENSDK_REGISTER_SUCCESS";// 注册成功
NSString * const OPENSDK_STATS_TEMPORARY_BIND_FAIL = @"OPENSDK_TEMPORARY_BIND_FAIL";// 游客绑定失败
NSString * const OPENSDK_STATS_TEMPORARY_BIND_SUCCESS = @"OPENSDK_TEMPORARY_BIND_SUCCESS";// 游客绑定失败
NSString * const OPENSDK_STATS_PAY_SELECT_UI = @"OPENSDK_PAY_SELECT_UI";// 展示支付方式选择界面
NSString * const OPENSDK_STATS_PAY_CANCEL = @"OPENSDK_PAY_CANCEL";// 支付取消
NSString * const OPENSDK_STATS_CREATE_ORDER_SUCCESS = @"OPENSDK_CREATE_ORDER_SUCCESS";// 创建订单成功
NSString * const OPENSDK_STATS_CREATE_ORDER_FAIL = @"OPENSDK_CREATE_ORDER_FAIL";// 创建订单失败
NSString * const OPENSDK_STATS_PAY_SUB = @"OPENSDK_PAY_SUB";//选择支付方式
NSString * const OPENSDK_STATS_PAY_SUB_SUCCESS = @"OPENSDK_PAY_SUB_SUCCESS";// 支付成功
NSString * const OPENSDK_STATS_PAY_SUB_FAIL = @"OPENSDK_PAY_SUB_FAIL";// 支付失败
NSString * const OPENSDK_STATS_PAY_SUB_CHECK_SUCCESS = @"OPENSDK_PAY_SUB_CHECK_SUCCESS";// 支付确认成功
NSString * const OPENSDK_STATS_PAY_SUB_CHECK_FAIL = @"OPENSDK_PAY_SUB_CHECK_FAIL";// 支付确认失败


NSString * const OPENSDK_STATS_DETAIl_LOGIN_PHONE = @"PHONE";// 登录统计详情：手机登录
NSString * const OPENSDK_STATS_DETAIl_LOGIN_EMAIL = @"EMAIL";// 登录统计详情：邮箱登录
NSString * const OPENSDK_STATS_DETAIl_LOGIN_TEMP = @"TEMPORARY";// 登录统计详情：游客登录
NSString * const OPENSDK_STATS_DETAIl_LOGIN_AUTO = @"AUTO";// 登录统计详情：快捷登录
NSString * const OPENSDK_STATS_DETAIl_PAY_CANCEL_UI = @"PAY_CANCEL";// 支付统计详情：支付方式选择UI取消
NSString * const OPENSDK_STATS_DETAIl_PAY_CANCEL_BIND = @"PAY_BIND";// 支付统计详情：支付绑定取消

static OpenSDK_StatisticsManager *instance = nil;

+(instancetype) getInstance {
    if(instance==nil) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            instance = [[self alloc] init];
        });
    }
    return instance;
}

-(void) collectWithType:(NSString *)type data:(NSDictionary *)data {
    NSMutableDictionary *dataWrapper = [[NSMutableDictionary alloc] initWithDictionary:data];
    // 添加sdk_uid
    if ([[OpenSDK_LoginManager getInstance] curOpenId] != nil) {
        [dataWrapper setValue:[OpenSDK_LoginManager getInstance].curOpenId forKey:@"sdk_uid"];
    }
    // 添加sdk_version
    [dataWrapper setValue:[OpenSDK_DataManager getInstance].version forKey:@"sdk_version"];
    // 上报统计信息，回调给外部监听者
    if ([OpenSDK getInstance].delegate) {
        [[OpenSDK getInstance].delegate openSDKStatisticsWithTpye:type dataInfo:dataWrapper];
    }
}

// 记录登录开始时间(调起登录界面)
-(void) startLogin {
    loginStartTime = [[NSDate date] timeIntervalSince1970] * 1000;
}

// 收集登录成功、注册、绑定的统计数据(不上报)
-(void) collectLoginSuccessWithType:(NSString *)type detail:(NSString *)detail {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    // 添加detail字段，登录方式
    if (detail) {
        [data setValue:detail forKey:@"detail"];
    }
    loginType = type;
    loginData = data;
}

// 上报登录成功、注册、绑定的统计数据
-(void) sendLoginSuccessStatistics {
    if (!loginData) {
        return;
    }
    // 添加rate_time字段，登录耗时=调起登录界面的时间戳-用户登录完成的时间戳
    if (loginStartTime > 0) {
        UInt64 loginEndTime = [[NSDate date] timeIntervalSince1970] * 1000;
        UInt64 loginCostTime = loginEndTime - loginStartTime;
        NSString *loginCostTimeStr = [NSString stringWithFormat:@"%llu", loginCostTime];
        NSLog(@"loginCostTime=%@", loginCostTimeStr);
        [loginData setValue:loginCostTimeStr forKey:@"rate_time"];
    }
    [self collectWithType:loginType data:loginData];
}

// 收集登录失败统计数据
-(void) collectLoginFailWithType:(NSString *)type
                          detail:(NSString *)detail
                        failCode:(long)code
                        failInfo:(NSString *)info {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    // 添加detail字段，登录方式
    if (detail) {
        [data setValue:detail forKey:@"detail"];
    }
    if (code) {
        [data setValue:[NSString stringWithFormat:@"%lu", code] forKey:@"fail_code"];
    }
    if (info) {
        [data setValue:info forKey:@"fail_info"];
    }
    [self collectWithType:type data:data];
}

// 收集展示支付界面/选择支付方式的统计数据
-(void) collectPayActionWithType:(NSString *)type
                      cpOrderId:(NSString *)cpOrderId
                         detail:(NSString *)detail {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    // 添加detail字段，支付方式
    if (detail) {
        [data setValue:detail forKey:@"detail"];
    }
    if (cpOrderId) {
        [data setValue:cpOrderId forKey:@"cp_order_id"];
    }
    [self collectWithType:type data:data];
}

// 收集创建订单成功\支付成功\支付校验成功的统计数据
-(void) collectPaySuccessWithType:(NSString *)type
                         orderId:(NSString *)orderId
                       cpOrderId:(NSString *)cpOrderId
                          detail:(NSString *)detail {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    // 添加detail字段，支付方式
    if (detail) {
        [data setValue:detail forKey:@"detail"];
    }
    if (orderId) {
        [data setValue:orderId forKey:@"order_id"];
    }
    if (cpOrderId) {
        [data setValue:cpOrderId forKey:@"cp_order_id"];
    }
    [self collectWithType:type data:data];
}

// 收集创建订单失败\支付失败\支付校验失败的统计数据
-(void) collectPayFailWithType:(NSString *)type
                      orderId:(NSString *)orderId
                    cpOrderId:(NSString *)cpOrderId
                       detail:(NSString *)detail
                     failInfo:(NSString *) info {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    // 添加detail字段，支付方式
    if (detail) {
        [data setValue:detail forKey:@"detail"];
    }
    if (orderId) {
        [data setValue:orderId forKey:@"order_id"];
    }
    if (cpOrderId) {
        [data setValue:cpOrderId forKey:@"cp_order_id"];
    }
    if (info) {
        [data setValue:info forKey:@"fail_info"];
    }
    [self collectWithType:type data:data];
}

// 收集支付取消的统计数据
-(void) collectPayCancelWithCpOrderId:(NSString *)cpOrderId
                              detail:(NSString *)detail {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    // 添加detail字段，支付方式
    if (detail) {
        [data setValue:detail forKey:@"detail"];
    }
    if (cpOrderId) {
        [data setValue:cpOrderId forKey:@"cp_order_id"];
    }
    [self collectWithType:OPENSDK_STATS_PAY_CANCEL data:data];
}

@end
