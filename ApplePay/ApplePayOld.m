//
//  ApplePay.m
//  苹果支付实现类
//
//  Created by wyht－ios－dev on 15/11/16.
//
//

#import "ApplePayOld.h"
#import "ApplePayLocalData.h"

#import "OpenSDK_Enum.h"
#import "OpenSDK_DataKeys.h"
#import "OpenSDK_ReqDataGenerator.h"
#import "OpenSDK_LoginManager.h"
#import "OpenSDK_EventManager.h"
#import "OpenSDK_EventKeys.h"
#import "OpenSDK_Toast.h"
#import "OpenSDK_PayManager.h"
#import "OpenSDK_StatisticsManager.h"
#import "OpenSDK_HttpUtil.h"
#import "OpenSDK_JsonUtil.h"

@implementation ApplePayOld
{
    //AppStore返回的商品列表
    NSMutableDictionary *_products;
    //当前的订单信息
    OpenSDK_PayResData *curPayInfo;
    
    //标识是点击支付触发的状态；而不是苹果补单机制的回调
    BOOL fromPayAction;
}

#pragma mark - OpenSDK_SdkProtrol声明方法
-(void) initInfo {
    //用户登录成功后进行初始化
    [OpenSDK_EventManager addObsver:self selector:@selector(initSDK:) name:OPENSDK_EVENT_LOGIN_SUCCESS object:nil className:@"ApplePayOld"];
}

#pragma mark - OpenSDK_SdkProtrol声明方法 - 服务器唯一标识
-(NSString *) getServerName {
//    return @"ApplePay";
    return @"ApplePayOld";
}

#pragma mark - OpenSDK_PayProtocol声明方法 - 展示的图标
-(NSString *) getDisplayIcon {
    return @"op_pay_apple.png";
}

#pragma mark - OpenSDK_PayProtocol声明方法 - 展示的名字
-(NSString *) getDisplayName {
    return @"苹果支付";
}

#pragma mark - OpenSDK_PayProtocol声明方法 - 支付平台名
-(NSString *) getPlatform {
    return @"ApplePay";
}

#pragma mark - OpenSDK_PayProtocol声明方法 - 支付平台版本号
-(NSString *) getPlatformVersion {
    return @"V1_";
}

#pragma mark - OpenSDK_PayProtocol声明方法 - 统计的类型名
-(NSString *) getStatisticsType {
    return @"APPLE";
}

#pragma mark - OpenSDK_PayProtocol声明方法 - 调起第三方平台支付，传入opensdk订单号
-(void) startPay:(OpenSDK_PayResData *) opensdkOrder {
    fromPayAction = YES;
    //防止重复支付
    if (curPayInfo != nil) {
        NSLog(@"当前有支付行为，无法进行支付");
        //提示用户
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[OpenSDK_iToast makeText:@"抱歉，有壹筆支付正在處理，請稍後重試"] setDuration:2000] show];
        });
        return;
    }
    if (_products == nil || [_products count] == 0) {
        //如果商品列表为空，则先初始化商品列表,再进行支付
        curPayInfo = opensdkOrder;
        [self initProductInfo];
    } else {
        //如果商品列表不为空，则直接调用ApplePay
        [self payWithPayInfo:opensdkOrder];
    }
}

-(void) initSDK:(NSNotification*) notification {
    if(_products != nil && [_products count] > 0) {
        return;
    }
    //初始化AppPay商品列表
    [self initProductInfo];
}

//初始化applePay与产品信息，用于后面的支付
-(void) initProductInfo {
    NSLog(@"ApplePay开始初始化商品");
    //绑定消息回调。如果有待处理订单在支付队列中，这个时候会有Apple的消息回调
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    // 加载OpenSDK服务器端配置的商品id列表
    [OpenSDK_HttpUtil getProductIds:^(OpenSDK_BaseResData *resData) {
        if([resData isOk]) {
            NSArray *products = [resData.content objectForKey:@"products"];
            NSLog(@"opensdk返回商品列表,products=%@", products);
            NSMutableSet *productItems=[[NSMutableSet alloc] init];
            if (products) {
                for (NSDictionary *item in products) {
                    [productItems addObject:[item objectForKey:@"product_id"]];
                }
            }
            // 再去苹果服务器验证商品id是否合法
            SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:productItems];
            request.delegate = self;
            [request start];
        } else {
            if (self->curPayInfo!=nil) {
                [[OpenSDK_PayManager getInstance] callbackPayFail:@"获取商品列表失败，请稍后重试" withOrder:nil];
            }
        }
    }];
    //加载本地的productID
    //    NSDictionary *config = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"OpenSDKConfig"];
    //    NSMutableArray *appProductItem = [config objectForKey:@"ApplePay_Product_Items"];
    //    NSMutableSet *productItems = [NSMutableSet setWithCapacity:[appProductItem count]];
    //    if (appProductItem) {
    //        for (NSString *item in appProductItem) {
    //            [productItems addObject:item];
    //        }
    //    }
    //    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:productItems];
    //    request.delegate = self;
    //    [request start];
}

#pragma mark - Apple的回调 - Apple的商品信息列表的回调
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSLog(@"ApplePay商品初始化完成");
    NSMutableDictionary *appleProducts = [[NSMutableDictionary alloc] init];
    for (SKProduct *product in response.products) {
        [appleProducts setObject:product forKey:product.productIdentifier];
    }
    _products = appleProducts;
    
    // 如果当前有订单对象，说明不是单纯的初始化，需要进行支付
    if (curPayInfo != nil) {
        [self payWithPayInfo:curPayInfo];
    }
}

//调用Apple的In-App-Purchase
-(void) payWithPayInfo:(OpenSDK_PayResData *)payInfo {
    //商品列表为空，直接结束
    if (_products == nil || [_products count] == 0) {
        [[OpenSDK_PayManager getInstance] callbackPayFail:@"商品信息无效，请检查网络连接，稍后重试" withOrder:nil];
        return;
    }
    
    // AppStore不允许支付
    if (![SKPaymentQueue canMakePayments]) {
        [[OpenSDK_PayManager getInstance] callbackPayFail:@"抱歉，AppStore禁止应用内支付购买" withOrder:nil];
        return;
    }
    
    // productId没有对应的商品信息，说明productId非法
    NSString *productId = [payInfo getProduct];
    SKProduct* selectedProduct = [_products objectForKey: productId];
    if(selectedProduct == NULL) {
        [[OpenSDK_PayManager getInstance] callbackPayFail:@"抱歉，商品异常无法支付" withOrder:nil];
        return;
    }
    
    //进行applePay的支付
    NSLog(@"开始ApplePay!product_id=%@,order_id=%@", productId, [payInfo getOrderID]);
    SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:selectedProduct];
    NSString *uid = [[OpenSDK_LoginManager getInstance] curOpenId];
    if (uid == nil) {
        uid = @"";
    }
    // 将opensdk订单号和支付uid存入applicationUsername字段
    payment.applicationUsername = [OpenSDK_JsonUtil toJsonWithObject:@{@"order":[payInfo getOrderID], @"uid":uid}];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

#pragma mark - Apple的回调 - Apple的In-App-Purchase的回调
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    //记录当前订单序号
    NSUInteger orderIndex = [transactions count];
    NSLog(@"总共的排队订单数量%lu", (unsigned long)orderIndex);
    for (SKPaymentTransaction *transaction in transactions) {
        orderIndex--;
        NSLog(@"当前的排队订单号:%lu",(unsigned long)orderIndex);
        //内存中维护一个等待处理的transaction
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchased:
                NSLog(@"Queue SKPaymentTransactionStatePurchased,product_id=%@,transaction_id=%@", transaction.payment.productIdentifier, transaction.transactionIdentifier);
                [self provideContent:transaction callback:((orderIndex == 0) && fromPayAction)];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Queue SKPaymentTransactionStateRestored,product_id=%@,transaction_id=%@", transaction.payment.productIdentifier, transaction.transactionIdentifier);
                [self provideContent:transaction callback:((orderIndex == 0) && fromPayAction)];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Queue SKPaymentTransactionStateFailed,product_id=%@,transaction_id=%@", transaction.payment.productIdentifier, transaction.transactionIdentifier);
                [self failedTransaction:transaction callback:((orderIndex == 0) && fromPayAction)];
                break;
            case SKPaymentTransactionStatePurchasing:
                //创建苹果订单成功，等待用户付款
                NSLog(@"Queue SKPaymentTransactionStatePurchasing,product_id=%@,transaction_id=%@", transaction.payment.productIdentifier, transaction.transactionIdentifier);
                //TODO 通过支付取消的回调，让游戏取消loading??
                [[OpenSDK_PayManager getInstance] callbackPayCancel];
                break;
            default:
                NSLog(@"Queue SKPaymentTransactionState Unknow,product_id=%@,transaction_id=%@", transaction.payment.productIdentifier, transaction.transactionIdentifier);
                //未知情况，但不能阻止用户正常支付
                [[OpenSDK_PayManager getInstance] callbackPayCancel];
                break;
        }
    }
    
}

+ (NSString *) getReceipt:(SKPaymentTransaction *)transaction {
    //目前苹果公司提倡的获取购买凭证的方法
    NSURL *receiptUrl = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptUrl];
    if (!receiptData) {
        //没有对应的receipt,说明此receipt已经被服务器验证过了。
        return nil;
    }
    //base64位的产品验证码单，base64是服务端和苹果进行校验所必须的，苹果的文档要求凭证经过Base64加密
    return [receiptData base64EncodedStringWithOptions:0];
}

/// Apple返回付款成功，到服务器进行checkOrder，并回调游戏
//- (void)handleApplePurchasedTransaction:(SKPaymentTransaction *)transaction canCallback:(BOOL)callBack {
//    /// 交易id
//    NSString *transactionId = transaction.transactionIdentifier;
//    /// 之前存储uid+orderId的字段
//    NSString *applicationUsername = transaction.payment.applicationUsername;
//
//    /// Check Order 需要参数对象
//    AppleCheckItem *checkItem = [[AppleCheckItem alloc] init];
//
//    // 如果出现 applicationUsername 异常，则从本地存储中获取
//    if (applicationUsername == nil || applicationUsername.length == 0) {
//        AppleCheckItem *localCheckItem = [ApplePayLocalData getCheckItemByTransactionId:transactionId];
//        if (localCheckItem != nil) {
//            checkItem.uid = localCheckItem.uid;
//            checkItem.orderId = localCheckItem.orderId;
//        } else {
//            // 如果本地没有存储，则去用户信息中获取uid
//            NSString *uid = [[OpenSDK_LoginManager getInstance] curOpenId];
//            if (uid != nil && uid.length > 0) {
//                checkItem.uid = [[OpenSDK_LoginManager getInstance] curOpenId];
//                checkItem.orderId = @"";
//            }
//        }
//    } else {    // 如果正常 则记录使用
//        NSDictionary *dic = [OpenSDK_JsonUtil toObjectWithJson:applicationUsername];
//        if (dic != nil) {
//            checkItem.uid = [dic objectForKey:@"uid"];
//            checkItem.orderId = [dic objectForKey:@"order"];
//        }
//    }
//
//    /// 沙盒中查询的支付凭证
//    NSString *recaipt = [ApplePay getReceipt:transaction];
//
//    // 如果从沙盒查询不到凭证，则去本地存储中获取
//    if (recaipt == nil || recaipt.length == 0) {
//        AppleCheckItem *localCheckItem = [ApplePayLocalData getCheckItemByTransactionId:transactionId];
//        if (localCheckItem != nil) {
//            checkItem.receipt = localCheckItem.receipt;
//        }
//    } else {
//        checkItem.receipt = recaipt;
//    }
//
//    // 如果获取参数不齐，则直接退出
//    if (checkItem.uid == nil || checkItem.orderId == nil || checkItem.receipt == nil) {
//        NSLog(@"[Apple Pay] 参数不齐");
//        return;
//    } else {    // 参数齐全，将其本地存储，以防网络异常或其他异常退出导致丢单
//        [ApplePayLocalData setCheckItem:checkItem withTransactionId:transactionId];
//    }
//
//    ///  去服务器CheckOrder
//}

//Apple返回付款成功Receipt，到服务器进行checkOrder，并回调游戏
- (void)provideContent:(SKPaymentTransaction *)transaction callback:(BOOL)callbackFlag {
    NSLog(@"++++++++++++++++++++++++++++++订单处理中,product_id=%@,transaction_id=%@,applicationUsername=%@", transaction.payment.productIdentifier, transaction.transactionIdentifier, transaction.payment.applicationUsername);
    NSString *applicationUsername = transaction.payment.applicationUsername;
    NSString *openSDKUID = nil;
    NSString *orderId = nil;
    // 优先尝试从applicationUsername关联用户和opensdk订单号
    if (applicationUsername != nil && applicationUsername.length > 0) {
        NSDictionary *appData = [OpenSDK_JsonUtil toObjectWithJson:applicationUsername];
        if (appData != nil) {
            openSDKUID = [appData objectForKey:@"uid"];
            orderId = [appData objectForKey:@"order"];
        }
    }
    // 如果从从applicationUsername关联uid为空，则从本地存储中找到购买商品对应的uid
    if (openSDKUID == nil || openSDKUID.length == 0) {
        NSString *openSDKUID = [ApplePayLocalData getUidByTransactionId:transaction.transactionIdentifier];
        //商品对应的uid为空，则使用当前登录用户uid
        if (openSDKUID == nil || openSDKUID.length == 0) {
            openSDKUID = [[OpenSDK_LoginManager getInstance] curOpenId];
            if (openSDKUID != nil) {
                [ApplePayLocalData addUid:openSDKUID withTransactionId:transaction.transactionIdentifier];
            }
        }
    }
    // 如果还是没有登录用户uid，则不做任何处理，跳过此次checkOrder,直接结束(等待下次Apple回调此订单)
    if (openSDKUID == nil || openSDKUID.length == 0) {
        NSLog(@"++++++++++++++++++++++++++++++OpenSDK订单对应的uid丢失，无法处理!product_id=%@,transaction_id=%@", transaction.payment.productIdentifier, transaction.transactionIdentifier);
        return;
    }
    
    NSString* receipt = [ApplePayOld getReceipt:transaction];
    NSLog(@"++++++++++++++++++++++++++++++CheckOrder前,product_id=%@,transaction_id=%@,对应的uid=%@,receipt=%@", transaction.payment.productIdentifier, transaction.transactionIdentifier, openSDKUID, receipt);
    // 如果Apple返回的receipt不为空，则保存receipt(防止网络问题或服务器CheckCOrder异常，导致receipt丢失，从而给玩家造成损失)
    if (receipt != nil && receipt.length != 0) {
        [ApplePayLocalData addReceipt:receipt withTransactionId:transaction.transactionIdentifier];
    }
    // 如果Apple返回的receipt为空，则尝试从本地存储中获取transaction对应的receipt
    else {
        receipt = [ApplePayLocalData getReceiptByTransactionId:transaction.transactionIdentifier];
    }
    
    // receipt为空(说明服务器check过，且本地存储的receipt也丢失了)，直接将订单从AppStore队列中移除，不回调游戏，结束
    if (receipt == nil || receipt.length == 0) {
        NSLog(@"++++++++++++++++++++++++++++++receipt为空，说明此receipt服务器已经验证过！将订单从AppStore队列中移除，product_id=%@,transaction_id=%@,对应的uid=%@", transaction.payment.productIdentifier, transaction.transactionIdentifier, openSDKUID);
        // 将订单从AppStore队列中移除
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        // 将receipt从本地持久化中删除
        [ApplePayLocalData delReceiptByTransactionId:transaction.transactionIdentifier];
        [ApplePayLocalData delUidByTransactionId:transaction.transactionIdentifier];
        return;
    }
    
    // 统计渠道支付成功
//    [[OpenSDK_StatisticsManager getInstance] collectPaySuccessWithType:OPENSDK_STATS_PAY_SUB_SUCCESS orderId:[curPayInfo getOrderID] cpOrderId:[curPayInfo getAppData] detail:[self getStatisticsType]];
//
    //到OpenSDK服务器checkOrder
    NSMutableDictionary *localInfo=[OpenSDK_ReqDataGenerator generateInitDataWithInfo];
    [localInfo setObject:openSDKUID forKey:OpenSDK_S_UID];
    [localInfo setObject:receipt forKey:@"receipt"];
    [localInfo setObject:[self getPlatform] forKey:OpenSDK_S_PLATFORM];
    [localInfo setObject:[self getPlatformVersion] forKey:OpenSDK_S_PLATFORM_VERSION];
    NSString *localInfoStr=[OpenSDK_ReqDataGenerator generateDataByDictionary:localInfo];
    [self checkOrderWithData:localInfoStr orderID:orderId httpDelegate:^(OpenSDK_BaseResData *resData) {
        if ([resData isOk]) {
            // CheckOrdet返回ok，检查订单状态
            OpenSDK_PayResData *payInfo=[[OpenSDK_PayResData alloc] initWithData:[resData content]];
            NSLog(@"++++++++++++++++++++++++++++++CheckOrder后,product_id=%@,transaction_id=%@,返回的uid=%@,返回的order_id=%@", [payInfo getProduct], transaction.transactionIdentifier, [payInfo getUID], [payInfo getOrderID]);
            
            if ([[payInfo getPayStatus] isEqualToString:@"SUCCESS"]) {
                // 统计checkOrder成功
                [[OpenSDK_StatisticsManager getInstance] collectPaySuccessWithType:OPENSDK_STATS_PAY_SUB_CHECK_SUCCESS orderId:[self->curPayInfo getOrderID] cpOrderId:[self->curPayInfo getAppData] detail:[self getStatisticsType]];
                
                if (callbackFlag) {
                    [[OpenSDK_PayManager getInstance] callbackPaySuccess:nil withOrder:[payInfo toUserOrderInfo]];
                }
                // 将订单从AppStore队列中移除
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                // 将receipt从本地持久化中删除
                [ApplePayLocalData delReceiptByTransactionId:transaction.transactionIdentifier];
                [ApplePayLocalData delUidByTransactionId:transaction.transactionIdentifier];
                NSLog(@"++++++++++++++++++++++++++++++CheckOrder成功，移除订单信息,product_id=%@,transaction_id=%@", transaction.payment.productIdentifier, transaction.transactionIdentifier);
            } else if ([[payInfo getPayStatus] isEqualToString:@"FAIL"]) {
                // 统计checkOrder失败
                [[OpenSDK_StatisticsManager getInstance] collectPayFailWithType:OPENSDK_STATS_PAY_SUB_CHECK_FAIL orderId:nil cpOrderId:[self->curPayInfo getAppData] detail:[self getStatisticsType] failInfo:@"Order status is FAIL"];
                
                if (callbackFlag) {
                    [[OpenSDK_PayManager getInstance] callbackPayFail:@"抱歉，支付凭证无效，支付失败" withOrder:nil];
                }
                // 将订单从AppStore队列中移除
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                // 将receipt从本地持久化中删除
                [ApplePayLocalData delReceiptByTransactionId:transaction.transactionIdentifier];
                [ApplePayLocalData delUidByTransactionId:transaction.transactionIdentifier];
                NSLog(@"++++++++++++++++++++++++++++++CheckOrder失败，receipt无效，移除订单信息,product_id=%@,transaction_id=%@", transaction.payment.productIdentifier, transaction.transactionIdentifier);
            } else {
                // 统计checkOrder为PAY_WAIT_CHECK状态
                [[OpenSDK_StatisticsManager getInstance] collectPayFailWithType:OPENSDK_STATS_PAY_SUB_CHECK_FAIL orderId:nil cpOrderId:[self->curPayInfo getAppData] detail:[self getStatisticsType] failInfo:@"Order status is NEW"];
                if (callbackFlag) {
                    // CheckOrder是NeW状态，则回调游戏OpenSDK_PAY_WAIT_CHECK，不移除订单，等待下次Apple回调此订单
                    OpenSDK_OrderInfo *userOrderInfo=[payInfo toUserOrderInfo];
                    [[OpenSDK_PayManager getInstance] callbackPayWaitCheck:userOrderInfo];
                }
            }
        } else {
            // 统计checkOrder网络失败
            [[OpenSDK_StatisticsManager getInstance] collectPayFailWithType:OPENSDK_STATS_PAY_SUB_CHECK_FAIL orderId:nil cpOrderId:[self->curPayInfo getAppData] detail:[self getStatisticsType] failInfo:[resData errorMsg]];
            
            // CheckOrder网络异常或其他失败，则不移除订单，等待下次Apple回调此订单
            NSLog(@"++++++++++++++++++++++++++++++CheckOrder网络异常,product_id=%@,transaction_id=%@,对应的uid=%@,错误码=%ld,错误信息=%@", transaction.payment.productIdentifier, transaction.transactionIdentifier, openSDKUID, [resData errorNo], [resData errorMsg]);
            if (callbackFlag) {
                NSString *errorNum=@"错误码: unknown";
                NSString *errorMsg=@"错误信息: unknown";
                NSString *errorNo=[NSString stringWithFormat:@"%ld",[resData errorNo]];
                if(errorNo!=nil)
                    errorNum=[@"错误码: " stringByAppendingString:errorNo];
                if([resData errorMsg]!=nil)
                    errorMsg=[@"错误信息: " stringByAppendingString:[resData errorMsg]];
                NSString *errorInfo=[errorNum stringByAppendingString:errorMsg];
                [[OpenSDK_PayManager getInstance] callbackPayFail:errorInfo withOrder:nil];
            }
        }
        NSLog(@"++++++++++++++++++++++++++++++订单处理结束");
    }];
}

//Apple返回付款失败，删除本地对应订单信息，并回调游戏
- (void)failedTransaction:(SKPaymentTransaction *)transaction callback:(BOOL)callbackFlag {
    NSLog(@"++++++++++++++++++++++++++++++订单处理失败,product_id=%@,transaction_id=%@,errorCode=%ld", transaction.payment.productIdentifier, transaction.transactionIdentifier, transaction.error.code);
    // 将订单从AppStore队列中移除
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    // 将receipt从本地持久化中删除
    [ApplePayLocalData delReceiptByTransactionId:transaction.transactionIdentifier];
    [ApplePayLocalData delUidByTransactionId:transaction.transactionIdentifier];
    
    // 统计渠道支付失败
    [[OpenSDK_StatisticsManager getInstance] collectPayFailWithType:OPENSDK_STATS_PAY_SUB_FAIL orderId:nil cpOrderId:[curPayInfo getAppData] detail:[self getStatisticsType] failInfo:[NSString stringWithFormat:@"Apple Error=%ld", transaction.error.code]];
    
    //如果是最后一个订单，则返回支付结果信息
    if (callbackFlag) {
        switch (transaction.error.code) {
            case SKErrorUnknown:
                [[OpenSDK_PayManager getInstance] callbackPayFail:@"抱歉，支付失败(请联系苹果客服)" withOrder:nil];
                break;
            case SKErrorClientInvalid:
                [[OpenSDK_PayManager getInstance] callbackPayFail:@"抱歉，当前苹果账户无法购买商品(如有疑问，请咨询苹果客服)" withOrder:nil];
                break;
            case SKErrorPaymentCancelled:
                [[OpenSDK_PayManager getInstance] callbackPayCancel];// 支付取消
                break;
            case SKErrorPaymentInvalid:
                [[OpenSDK_PayManager getInstance] callbackPayFail:@"抱歉，订单无效(如有疑问，请咨询苹果客服)" withOrder:nil];
                break;
            case SKErrorPaymentNotAllowed:
                [[OpenSDK_PayManager getInstance] callbackPayFail:@"抱歉，当前设备无法购买商品(如有疑问，请咨询苹果客服)" withOrder:nil];
                break;
            case SKErrorStoreProductNotAvailable:
                [[OpenSDK_PayManager getInstance] callbackPayFail:@"抱歉，支付失败，当前商品不可用" withOrder:nil];
                break;
            case SKErrorCloudServicePermissionDenied:
                [[OpenSDK_PayManager getInstance] callbackPayFail:@"抱歉，支付失败，苹果账号无法连接云服务" withOrder:nil];
                break;
            case SKErrorCloudServiceNetworkConnectionFailed:
                [[OpenSDK_PayManager getInstance] callbackPayFail:@"抱歉，支付失败，苹果设备无法连接云服务" withOrder:nil];
                break;
            case SKErrorCloudServiceRevoked:
                [[OpenSDK_PayManager getInstance] callbackPayFail:@"抱歉，支付失败，用户撤销使用苹果云服务的权限" withOrder:nil];
                break;
            default:
                [[OpenSDK_PayManager getInstance] callbackPayFail:@"抱歉，支付失败(未知错误)" withOrder:nil];
                break;
        }
    }
}

@end
