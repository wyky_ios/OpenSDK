//
//  OPKTransactionItem.h
//  PaymentDemo
//
//  Created by ycgame on 2020/4/21.
//  Copyright © 2020 maqianzheng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OPKOrderItem.h"

NS_ASSUME_NONNULL_BEGIN

/// 交易对象
@interface OPKTransactionItem : NSObject <NSCoding, NSCopying>
// 订单对象
@property(nonatomic, copy)OPKOrderItem *orderItem;
// 苹果交易 id
@property(nonatomic, copy)NSString *transactionId;
// 原始交易，订阅续费的原始交易
@property(nonatomic, copy, nullable)OPKTransactionItem *originalTransaction;

@end

NS_ASSUME_NONNULL_END
