//
//  OPKOrderItem.m
//  PaymentDemo
//
//  Created by ycgame on 2020/4/21.
//  Copyright © 2020 maqianzheng. All rights reserved.
//

#import "OPKOrderItem.h"

@implementation OPKOrderItem
- (nullable instancetype)initWithCoder:(nonnull NSCoder *)coder {
    if (self = [super init]) {
        self.uid = [coder decodeObjectForKey:@"uid"];
        self.orderId = [coder decodeObjectForKey:@"orderId"];
        self.appData = [coder decodeObjectForKey:@"appData"];
        self.productId = [coder decodeObjectForKey:@"productId"];
        self.isSubscriptionProduct = [coder decodeBoolForKey:@"isSubscriptionProduct"];
        self.productAmount = [coder decodeObjectForKey:@"productAmount"];
        self.currency = [coder decodeObjectForKey:@"currency"];
        self.payStatus = [coder decodeObjectForKey:@"payStatus"];
        self.payTime = [coder decodeObjectForKey:@"payTime"];
    }
    return self;
}

- (void)encodeWithCoder:(nonnull NSCoder *)coder {
    [coder encodeObject:self.uid forKey:@"uid"];
    [coder encodeObject:self.orderId forKey:@"orderId"];
    [coder encodeObject:self.appData forKey:@"appData"];
    [coder encodeObject:self.productId forKey:@"productId"];
    [coder encodeBool:self.isSubscriptionProduct forKey:@"isSubscriptionProduct"];
    [coder encodeObject:self.productAmount forKey:@"productAmount"];
    [coder encodeObject:self.currency forKey:@"currency"];
    [coder encodeObject:self.payStatus forKey:@"payStatus"];
    [coder encodeObject:self.payTime forKey:@"payTime"];
}

- (id)copyWithZone:(NSZone *)zone {
    OPKOrderItem *item = [[OPKOrderItem allocWithZone:zone] init];
    item.uid = self.uid;
    item.orderId = self.orderId;
    item.appData = self.appData;
    item.productId = self.productId;
    item.isSubscriptionProduct = self.isSubscriptionProduct;
    item.productAmount = self.productAmount;
    item.currency = self.currency;
    item.payStatus = self.payStatus;
    item.payTime = self.payTime;
    return item;
}

@end
