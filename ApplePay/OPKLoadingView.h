//
//  OPKLoadingView.h
//  Pods-Unity-iPhone
//
//  Created by ycgame on 2020/6/4.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OPKLoadingView : UIView
+ (void)start;
+ (void)stop;
@end

NS_ASSUME_NONNULL_END
