//
//  KeychainOrderItem.h
//  PaymentDemo
//
//  Created by ycgame on 2020/4/21.
//  Copyright © 2020 maqianzheng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OPKOrderItem.h"
#import "OPKTransactionItem.h"

NS_ASSUME_NONNULL_BEGIN

/// 钥匙串存储对象
@interface OPKKeychainItem : NSObject

@end

/// 针对 OPKOrderItem 存储扩展
@interface OPKKeychainItem (OPKOrder)

+ (void)setOrderItem:(OPKOrderItem *)item forKey:(NSString *)key;
+ (void)removeOrderItemForKey:(NSString *)key;
+ (OPKOrderItem *)orderItemForKey:(NSString *)key;

@end

/// 针对 OPKTransactionItem 存储扩展
@interface OPKKeychainItem (OPKTransaction)

+ (void)setTransactionItem:(OPKTransactionItem *)item forKey:(NSString *)key;
+ (void)removeTransactionItemForKey:(NSString *)key;
+ (OPKTransactionItem *)transactionItemForKey:(NSString *)key;

@end

/// 针对 Uid 存储扩展
@interface OPKKeychainItem (OPKUserId)

+ (void)setUid:(NSString *)uid;
+ (void)removeUid;
+ (NSString *)uid;

@end

/// 针对 字符串 存储扩展
@interface OPKKeychainItem (OPKValue)

+ (void)setValue:(NSString *)value forKey:(NSString *)key;
+ (void)removeValueForKey:(NSString *)key;
+ (NSString *)valueForKey:(NSString *)key;

@end


NS_ASSUME_NONNULL_END
