//
//  OPKStoreObserver.h
//  PaymentDemo
//
//  Created by ycgame on 2020/4/17.
//  Copyright © 2020 maqianzheng. All rights reserved.
//

@import StoreKit;
#import <Foundation/Foundation.h>
#import "OPKOrderItem.h"
#import "OPKTransactionItem.h"

NS_ASSUME_NONNULL_BEGIN

// 交易结果
typedef NS_ENUM(NSUInteger, OPKTransactionResult) {
    OPKTransactionResultPass = 1,
    OPKTransactionResultFail = 2,
    OPKTransactionResultUnknown = 0
};

@interface OPKStoreObserver : NSObject <SKPaymentTransactionObserver>
@property(nonatomic, copy)NSString *message;
@property(nonatomic)BOOL isShouldFinish;

/// 单例对象
+ (instancetype)sharedInstance;

/// 成为交易监听者
- (void)becomeTransactionObserver;
/// 移除对交易的监听
- (void)resignTransactionObserver;

/// 购买
- (void)buy:(SKProduct *)product order:(OPKOrderItem *)orderItem;

/// 查找交易对象
- (OPKTransactionItem *)transactionItemForId:(NSString *)transactionId;

/// 报告交易结果 （调用了buy方法则必须调用此方法，用以清除钥匙数据以及苹果沙盒订单数据）
- (void)confirmTransactionWithId:(NSString *)transactionId result:(OPKTransactionResult)result;
- (void)confirmTransactionWithItem:(OPKTransactionItem *)transactionItem result:(OPKTransactionResult)result;

@end


NS_ASSUME_NONNULL_END
