//
//  ApplePayLocalData.m
//  Unity-iPhone
//
//  Created by wyht－ios－dev on 15/11/18.
//
//

#import "ApplePayLocalData.h"

@implementation ApplePayLocalData

static NSString * const KEY_TRANSACTION_RECEIPT=@"OpenSDK_Transaction_Receipt";
static NSString * const KEY_PRODUCT_UID=@"OpenSDK_Product_Uid";
static NSString * const KEY_ITEM_TRANSACTION_RECEIPT = @"OpenSDK_Item_Transaction_Receipt";

#pragma mark TranscationId与Receipt的对应关系

//以transactionId为key，获取Receipt
+ (NSString *) getReceiptByTransactionId:(NSString *)transactionId {
    @synchronized(self) {
        NSDictionary *orderDic = [[NSUserDefaults standardUserDefaults] dictionaryForKey:KEY_TRANSACTION_RECEIPT];
        return orderDic == nil ? nil : [orderDic objectForKey:transactionId];
    }
}

//以transactionId为key，保存Receipt(如果已经存在，直接覆盖)
+ (BOOL) addReceipt:(NSString *)receipt
  withTransactionId:(NSString *)transactionId {
    if (receipt == nil) {
        return NO;
    }
    @synchronized(self) {
        NSDictionary *localData = [[NSUserDefaults standardUserDefaults] dictionaryForKey:KEY_TRANSACTION_RECEIPT];
        NSMutableDictionary *mutableLocalData = localData == nil? [[NSMutableDictionary alloc] init] : [NSMutableDictionary dictionaryWithDictionary:localData];
        [mutableLocalData setValue:receipt forKey:transactionId];
        [[NSUserDefaults standardUserDefaults] setObject:mutableLocalData forKey:KEY_TRANSACTION_RECEIPT];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return YES;
    }
}

//以transactionId为key，删除Receipt
+ (void) delReceiptByTransactionId:(NSString *)transactionId {
    @synchronized(self) {
        NSDictionary *localData = [[NSUserDefaults standardUserDefaults] dictionaryForKey:KEY_TRANSACTION_RECEIPT];
        if (localData != nil && transactionId != nil) {
            NSMutableDictionary *orderDic = [NSMutableDictionary dictionaryWithDictionary:localData];
            [orderDic removeObjectForKey:transactionId];
            [[NSUserDefaults standardUserDefaults] setObject:orderDic forKey:KEY_TRANSACTION_RECEIPT];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}


//以transactionId为key，获取uid
+ (NSString *) getUidByTransactionId:(NSString *)transactionId {
    @synchronized(self) {
        NSDictionary *orderDic = [[NSUserDefaults standardUserDefaults] dictionaryForKey:KEY_PRODUCT_UID];
        return orderDic == nil ? nil : [orderDic objectForKey:transactionId];
    }
}

//以transactionId为key，保存uid
+ (BOOL) addUid:(NSString *)uid
  withTransactionId:(NSString *)transactionId {
    if (uid == nil) {
        return NO;
    }
    @synchronized(self) {
        NSDictionary *localData = [[NSUserDefaults standardUserDefaults] dictionaryForKey:KEY_PRODUCT_UID];
        NSMutableDictionary *mutableLocalData = localData == nil? [[NSMutableDictionary alloc] init] : [NSMutableDictionary dictionaryWithDictionary:localData];
        [mutableLocalData setValue:uid forKey:transactionId];
        [[NSUserDefaults standardUserDefaults] setObject:mutableLocalData forKey:KEY_PRODUCT_UID];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return YES;
    }
}

//以transactionId为key，删除uid
+ (void) delUidByTransactionId:(NSString *)transactionId {
    @synchronized(self) {
        NSDictionary *localData = [[NSUserDefaults standardUserDefaults] dictionaryForKey:KEY_PRODUCT_UID];
        if (localData != nil && transactionId != nil) {
            NSMutableDictionary *orderDic = [NSMutableDictionary dictionaryWithDictionary:localData];
            [orderDic removeObjectForKey:transactionId];
            [[NSUserDefaults standardUserDefaults] setObject:orderDic forKey:KEY_PRODUCT_UID];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

+ (BOOL)setCheckItem:(AppleCheckItem *)item withTransactionId:(NSString *)transactionId {
    if (item.uid == nil || item.orderId == nil || item.receipt == nil) {
        return false;
    }
    @synchronized (self) {    
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:item];
        NSUserDefaults *userDefaults = [[NSUserDefaults alloc] initWithSuiteName:KEY_ITEM_TRANSACTION_RECEIPT];
        [userDefaults setObject:data forKey:transactionId];
        return YES;
    }
}

+ (void)delCheckItemByTransactionId:(NSString *)transactionId {
    @synchronized (self) {
        NSUserDefaults *userDefaults = [[NSUserDefaults alloc] initWithSuiteName:KEY_ITEM_TRANSACTION_RECEIPT];
        [userDefaults removeObjectForKey:transactionId];
    }
}

+ (AppleCheckItem *)getCheckItemByTransactionId:(NSString *)transactionId {
    @synchronized (self) {
        NSUserDefaults *userDefaults = [[NSUserDefaults alloc] initWithSuiteName:KEY_ITEM_TRANSACTION_RECEIPT];
        NSData *data = [userDefaults objectForKey:KEY_ITEM_TRANSACTION_RECEIPT];
        AppleCheckItem *item = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        return item;
    }
}

@end


@implementation AppleCheckItem
- (nullable instancetype)initWithCoder:(nonnull NSCoder *)coder {
    if (self = [super init]) {
        self.uid = [coder decodeObjectForKey:@"uid"];
        self.orderId = [coder decodeObjectForKey:@"orderId"];
        self.receipt = [coder decodeObjectForKey:@"receipt"];
    }
    return self;
}

- (void)encodeWithCoder:(nonnull NSCoder *)coder {
    [coder encodeObject:self.uid forKey:@"uid"];
    [coder encodeObject:self.orderId forKey:@"orderId"];
    [coder encodeObject:self.receipt forKey:@"receipt"];
}


@end
