//
//  ApplePay.h
//  Unity-iPhone
//
//  Created by wyht－ios－dev on 15/11/16.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "OpenSDK_PayProtocol.h"
#import "OpenSDK_SdkProtrol.h"
#import "OpenSDK_BaseClass.h"

@interface ApplePayOld : OpenSDK_BaseClass <OpenSDK_SdkProtrol, OpenSDK_PayProtocol, SKPaymentTransactionObserver, SKProductsRequestDelegate>

@end
