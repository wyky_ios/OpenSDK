//
//  ApplePayLocalData.h
//  Unity-iPhone
//
//  Created by wyht－ios－dev on 15/11/18.
//
//

#import <UIKit/UIKit.h>

@class AppleCheckItem;

@interface ApplePayLocalData : NSObject

//以transactionId为key，获取Receipt
+ (NSString *) getReceiptByTransactionId:(NSString *)transactionId;

//以transactionId为key，保存Receipt
+ (BOOL) addReceipt:(NSString *)receipt
      withTransactionId:(NSString *)transactionId;

//以transactionId为key，删除Receipt
+ (void) delReceiptByTransactionId:(NSString *)transactionId;


//以transactionId为key，获取uid
+ (NSString *) getUidByTransactionId:(NSString *)transactionId;

//以transactionId为key，保存uid
+ (BOOL) addUid:(NSString *)uid
  withTransactionId:(NSString *)transactionId;

//以transactionId为key，删除uid
+ (void) delUidByTransactionId:(NSString *)transactionId;

// 设置check对象
+ (BOOL)setCheckItem:(AppleCheckItem *)item withTransactionId:(NSString *)transactionId;

// 删除check对象
+ (void) delCheckItemByTransactionId:(NSString *)transactionId;

// 查找checkItem
+ (AppleCheckItem *)getCheckItemByTransactionId:(NSString *)transactionId;

@end

/// Check信息
@interface AppleCheckItem : NSObject <NSCoding>
@property(nonatomic, copy)NSString *uid;
@property(nonatomic, copy)NSString *orderId;
@property(nonatomic, copy)NSString *receipt;
@end
