//
//  OPKOrderItem.h
//  PaymentDemo
//
//  Created by ycgame on 2020/4/21.
//  Copyright © 2020 maqianzheng. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 订单对象
@interface OPKOrderItem : NSObject <NSCoding, NSCopying>
// 用户id
@property(nonatomic, copy)NSString *uid;
// 订单 id
@property(nonatomic, copy)NSString *orderId;
// 商品 id
@property(nonatomic, copy)NSString *productId;
/// 是否为订阅商品
@property(nonatomic)BOOL isSubscriptionProduct;
// appData 统计数据使用
@property(nonatomic, copy)NSString *appData;
// 统计使用
@property(nonatomic, copy)NSString *productAmount;
// 统计使用
@property(nonatomic, copy)NSString *payStatus;
// 统计使用
@property(nonatomic, copy)NSString *currency;
// 统计使用
@property(nonatomic, copy)NSString *payTime;

@end

NS_ASSUME_NONNULL_END
