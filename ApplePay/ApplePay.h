//
//  OPKApplePay.h
//
//  Created by ycgame on 2020/4/22.
//  Copyright © 2020 maqianzheng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenSDK_PayProtocol.h"
#import "OpenSDK_SdkProtrol.h"
#import "OpenSDK_BaseClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApplePay : OpenSDK_BaseClass <OpenSDK_SdkProtrol, OpenSDK_PayProtocol>

@end

NS_ASSUME_NONNULL_END
