//
//  OPKStoreManager.h
//  PaymentDemo
//
//  Created by ycgame on 2020/4/21.
//  Copyright © 2020 maqianzheng. All rights reserved.
//

@import StoreKit;
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OPKStoreManager : NSObject

+ (instancetype)sharedInstance;

/// 商品列表
@property(strong)NSMutableArray<SKProduct *> *products;

/// 获取商品
-(void)startProductRequestWithIdentifiers:(NSArray *)identifiers;

@end

NS_ASSUME_NONNULL_END
