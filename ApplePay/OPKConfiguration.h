//
//  OPKConfiguration.h
//  PaymentDemo
//
//  Created by ycgame on 2020/4/21.
//  Copyright © 2020 maqianzheng. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Notifications

extern NSString *const OPKProductRequestNotification;
extern NSString *const OPKConfirmTransactionNotification;
extern NSString *const OPKFailedTransactionNotification;

#pragma mark - Text
// 确认使用弹框
extern NSString *const OPKPromoCodeUseAlertMessage;
extern NSString *const OPKPromoCodeUseAlertNext;
extern NSString *const OPKPromoCodeUseAlertNow;

// 已使用弹框
extern NSString *const OPKPromoCodeReuseAlertMessage;
extern NSString *const OPKPromoCodeReuseAlertDiscard;
extern NSString *const OPKPromoCodeReuseAlertOk;

// 已过期弹框
extern NSString *const OPKPromoCodeExpiredAlertMessage;
extern NSString *const OPKPromoCodeExpiredAlertOk;

#pragma mark - 兑换码商品状态
/// 未开始
extern NSString *const OPKPromoProductStatusNotStarted;
/// 可使用
extern NSString *const OPKPromoProductStatusAvailable;
/// 已过期
extern NSString *const OPKPromoProductStatusExpired;
