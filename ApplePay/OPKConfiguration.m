//
//  OPKConfiguration.m
//  PaymentDemo
//
//  Created by ycgame on 2020/4/21.
//  Copyright © 2020 maqianzheng. All rights reserved.
//

#import "OPKConfiguration.h"

#pragma mark - Notifications

NSString *const OPKProductRequestNotification = @"OPKProductRequestNotification";
NSString *const OPKConfirmTransactionNotification = @"OPKConfirmTransactionNotification";
NSString *const OPKFailedTransactionNotification = @"OPKFailedTransactionNotification";


#pragma mark - Text

NSString *const OPKPromoCodeUseAlertMessage = @"您有来自App Store充值的活动礼包！同一游戏账号，在活动期间仅可参与兑换一次，确认为当前账号兑换礼包吗？";
NSString *const OPKPromoCodeUseAlertNext = @"下次兑换";
NSString *const OPKPromoCodeUseAlertNow = @"立即兑换";

NSString *const OPKPromoCodeReuseAlertMessage = @"您当前游戏账号已兑换过App Store的活动礼包，同一游戏账号，在活动期间仅可参与兑换一次，建议您更换游戏账号，重新登录游戏领取。";
NSString *const OPKPromoCodeReuseAlertDiscard = @"不再兑换";
NSString *const OPKPromoCodeReuseAlertOk = @"好的，已了解";

NSString *const OPKPromoCodeExpiredAlertMessage = @"您在App Store兑换的活动礼包已结束，当前礼包兑换失败，期待您参加下次活动，祝您游戏愉快。";
NSString *const OPKPromoCodeExpiredAlertOk = @"好的，了解了";


#pragma mark - 兑换码商品状态
NSString *const OPKPromoProductStatusNotStarted = @"0";
NSString *const OPKPromoProductStatusAvailable = @"1";
NSString *const OPKPromoProductStatusExpired = @"2";
