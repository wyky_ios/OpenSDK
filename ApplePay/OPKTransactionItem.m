//
//  OPKTransactionItem.m
//  PaymentDemo
//
//  Created by ycgame on 2020/4/21.
//  Copyright © 2020 maqianzheng. All rights reserved.
//

#import "OPKTransactionItem.h"

@implementation OPKTransactionItem

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)coder {
    if (self = [super init]) {
        self.orderItem = [coder decodeObjectForKey:@"orderItem"];
        self.transactionId = [coder decodeObjectForKey:@"transactionId"];
        self.originalTransaction = [coder decodeObjectForKey:@"originalTransaction"];
    }
    return self;
}

- (void)encodeWithCoder:(nonnull NSCoder *)coder {
    [coder encodeObject:self.orderItem forKey:@"orderItem"];
    [coder encodeObject:self.transactionId forKey:@"transactionId"];
    [coder encodeObject:self.originalTransaction forKey:@"originalTransaction"];
}

- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    OPKTransactionItem *item = [[OPKTransactionItem allocWithZone:zone] init];
    item.orderItem = self.orderItem;
    item.transactionId = self.transactionId;
    item.originalTransaction = self.originalTransaction;
    return item;
}

@end
