//
//  OPKLoadingView.m
//  Pods-Unity-iPhone
//
//  Created by ycgame on 2020/6/4.
//

#import "OPKLoadingView.h"

@interface OPKLoadingView()
@property(nonatomic, strong)UIView *contentView;
@property(nonatomic, strong)UIActivityIndicatorView *activityIndicatorView;
@end

@implementation OPKLoadingView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+ (OPKLoadingView *)sharedInstance {
    static dispatch_once_t onceToken;
    static OPKLoadingView * sharedInstance;

    dispatch_once(&onceToken, ^{
        UIWindow *window = [UIApplication sharedApplication].windows.firstObject;
        sharedInstance = [[OPKLoadingView alloc] initWithFrame:window.bounds];
    });
    return sharedInstance;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    
    _contentView = [[UIView alloc] init];
    _contentView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    _contentView.layer.cornerRadius = 8;
    [self addSubview:_contentView];
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_contentView addSubview:_activityIndicatorView];
    
    NSDictionary *views = @{@"contentView": _contentView,
                            @"activityIndicatorView": _activityIndicatorView};
    
    _contentView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *chConstrants = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[contentView(120.0)]" options:NSLayoutFormatAlignAllCenterX metrics:nil views:views];
    NSArray *cvConstrants = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[contentView(160.0)]" options:NSLayoutFormatAlignAllCenterY metrics:nil views:views];
    NSLayoutConstraint *cxConstranst = [NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
    NSLayoutConstraint *cyConstranst = [NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    [self addConstraint:cxConstranst];
    [self addConstraint:cyConstranst];
    [self addConstraints:chConstrants];
    [self addConstraints:cvConstrants];
    
    _activityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *ahConstrants = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[activityIndicatorView(60.0)]" options:NSLayoutFormatAlignAllCenterX metrics:nil views:views];
    NSArray *avConstrants = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[activityIndicatorView(60.0)]" options:NSLayoutFormatAlignAllCenterY metrics:nil views:views];
    NSLayoutConstraint *axConstranst = [NSLayoutConstraint constraintWithItem:_activityIndicatorView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_contentView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
    NSLayoutConstraint *ayConstranst = [NSLayoutConstraint constraintWithItem:_activityIndicatorView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_contentView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    [_contentView addConstraint:axConstranst];
    [_contentView addConstraint:ayConstranst];
    [_contentView addConstraints:ahConstrants];
    [_contentView addConstraints:avConstrants];
}

- (void)start {
    if ([self superview] != nil) { return; }
    UIWindow *window = [UIApplication sharedApplication].windows.firstObject;
    [window addSubview:self];
    [_activityIndicatorView startAnimating];
}

- (void)stop {
    [_activityIndicatorView stopAnimating];
    [self removeFromSuperview];
}

+ (void)start {
    dispatch_async(dispatch_get_main_queue(), ^{    
        [[OPKLoadingView sharedInstance] start];
    });
}

+ (void)stop {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[OPKLoadingView sharedInstance] stop];
    });
}

@end
