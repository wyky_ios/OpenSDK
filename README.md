# OpenSDK

#### 简介
|     日期    | Pods资源版本 |    备注   |
| ---------- | ---------- | --------- |
| 2021.06.21 | 0.3.2| 苹果支付增加兑换商品逻辑|
| 2021.06.21 | 0.3.1| 增加创建订单失败消息提示|
| 2021.6.20 | 0.3.0| 增加审核模式下默认登陆项|
| 2021.6.11 | 0.2.9| 第三方键盘优化|
| 2021.6.05 | 0.2.8| 去除敏感字符|
| 2021.05.27 | 0.2.7| 隐私政策优化|
| 2021.05.27 | 0.2.6| 增加游客登录|
| 2021.05.27 | 0.2.5| 登录,发送验证码,实名认证替换md5验签|
| 2021.04.09 | 0.2.3| 登录方式与UI修改 |
| 2021.03.14 | 0.2.2| 用户中心客服优化 |
| 2021.02.07 | 0.2.1| 实名认证问题修复 |
| 2021.01.27 | 0.2.0| UI显示适配高版本Unity |
| 2021.01.09 | 0.1.9| 登录方式优化 |
| 2020.12.12 | 0.1.8| 增加运营商一键登录 |
| 2020.12.06 | 0.1.7 | 增加用户中心，UI改版 |
| 2020.10.02 | 0.1.4 | 暗黑模式适配 |
| 2020.07.21 | 0.1.3 | 初始化逻辑优化，优化用户协议相关 |
| 2020.06.11 | 0.1.2 | Apple支付优化，完善订单支付异常处理逻辑 |
| 2020.03.31 | 0.1.1 | Apple登录，微信更新至1.8.6，Dark Mode文字看不到问题修改 |

OpenSDK渠道。包含多个子模块：

- 框架 YCOpenSDK/Framework
- [运营商一键登录](#登录配置：一键登录) YCOpenSDK/OneKeyLogin
- [苹果登录](#登录配置：苹果登录) YCOpenSDK/AppleLogin
- [微信登录](#登录配置：微信登录) YCOpenSDK/WechatLogin
- [QQ登录](#登录配置：QQ登录) YCOpenSDK/QQLogin
- [苹果支付](#支付配置:苹果支付ApplePay) YCOpenSDK/ApplePay
- [支付宝](#支付配置:支付宝AliPay) YCOpenSDK/AliPay
- [微信支付](#支付配置:微信支付WechatPay) YCOpenSDK/WechatPay
- [银联支付](#支付配置:银联支付UnionPay) YCOpenSDK/UnionPay
- [爱贝支付](#支付配置:爱贝支付AibeiPay) YCOpenSDK/AibeiPay
- [现在支付(H5微信支付)](#支付配置:现在支付(H5微信支付)PayNow) YCOpenSDK/PayNow

接入时，Framework是必须包含的。ApplePay也是必须的，支持苹果支付。

如果需要其他支付方式，根据需要自行添加对应的模块。

如果需要其他第三方登录方式，根据需要自行添加对应的模块。

#### 导入

1. 安装CocoaPods
2. 在项目根目录，输入`pod init`命令，创建Podfile
3. Podfile内容如下：
	```
	# Uncomment the next line to define a global platform for your project
	source 'https://gitee.com/wyky_ios/Spec.git'
	source 'https://github.com/CocoaPods/Specs.git'

	platform :ios, '8.0'

	target '你的项目名' do
		#指定OpenSDK资源、版本号、子模块(根据需要指定子模块)
		pod 'YCOpenSDK', :subspecs => ['Framework', 'ApplePay']
	end
	```
5. 输入`pod install`命令，安装相关依赖
6. 打开新生成的.xcworkspace，进行后续的开发和调试

#### Info.plist配置

1. 添加OpenSDKConfig
	```
	<key>OpenSDKConfig</key>
	<dict>
		<key>OpenSDK_AppID</key>
		<string>游戏在OpenSDK的AppId参数，必填</string>
		<key>OpenSDK_PubKey</key>
		<string>游戏在OpenSDK公钥参数，必填</string>
		<key>ChannelID</key>
		<string>游戏在OpenSDK的Channel参数，必填</string>
		<key>OpenSDK_Currency</key>
		<string>货币类型，一般填CNY</string>
		<key>OpenSDK_Logo</key>
		<string>游戏方自定义的logo图片(建议长宽比至少4:1)，可不填</string>
		<key>SupportLanguage</key>
		<string>zh-Hans</string>
	</dict>
	```

2. 指定自定义logo图片（可选）。为了效果美观，建议logo图片长宽比至少4:1（或5:1、6:1以上）。图片需要加入到项目中，并将图片路径配置到info.plist的**OpenSDKConfig**下
    ```
    <key>OpenSDKConfig</key>
    <dict>
        <key>OpenSDK_Logo</key>
        <string>logo图片名xxx.png</string>
    </dict>
    ```

#### 工程配置

1. 在Build Settings -> Header Search Paths中添加$(inherited)
2. 在Build Settings -> Other Linker Flags中添加$(inherited)
3. 在Build Settings -> Enable Bitcode设置成No

#### 登录配置：微信登录
1. Podfile中的subspecs，添加**WechatLogin**
2. 配置[Universal Links](https://gitee.com/wyky_ios/SuperSDK_Plugin/blob/master/UniversalLinks.md)
3. 工程中配置Associated Domains，如下图
![avatar](https://gitee.com/wyky_ios/SuperSDK_Plugin/blob/master/doc_static/asdom.png)
6. Info.plist中，添加微信参数。在`OpenSDKConfig`下，添加**WcAppId**
    ```
    <key>OpenSDKConfig</key>
    <dict>
        <key>WcAppId</key>
        <string>微信参数AppId</string>
        <key>OpenSDK_Universal_Links</key>
        <string>UniversalLinks</string>
    </dict>
    ```
7. 在"Info"标签栏的"URL type",添加**URL scheme**为你微信AppId，用于从微信返回游戏。如下图：
   ![avatar](https://gitee.com/wyky_ios/SuperSDK_Plugin/blob/master/doc_static/share.jpg)
9. Info.plist中，添加**LSApplicationQueriesSchemes**，用于检测手机上是否安装微信。
    ```
    <key>LSApplicationQueriesSchemes</key>
    <array>
        <string>weixin</string>
        <string>wechat</string>
         <string>weixinULAPI</string>
    </array>
    ```
    


#### 登录配置：QQ登录
1. Podfile中的subspecs，添加**QQLogin**
2. 配置[Universal Links](https://gitee.com/wyky_ios/SuperSDK_Plugin/blob/master/UniversalLinks.md)
3. 工程中配置Associated Domains，如下图
![avatar](https://gitee.com/wyky_ios/SuperSDK_Plugin/blob/master/doc_static/asdom.png)
6. Info.plist中，添加QQ参数。在`OpenSDKConfig`下，添加**QQAppId**
    ```
    <key>OpenSDKConfig</key>
    <dict>
        <key>QQAppId</key>
        <string>QQ参数AppId</string>
        <key>OpenSDK_Universal_Links</key>
        <string>UniversalLinks</string>
    </dict>
    ```
7. 在"Info"标签栏的"URL type",添加**URL scheme**为你QQAppId，用于从QQ返回游戏。如下图：
   ![avatar](/doc_static/qqShare.png)
9. Info.plist中，添加**LSApplicationQueriesSchemes**，用于检测手机上是否安装QQ。
    ```
    <key>LSApplicationQueriesSchemes</key>
    <array>
         <string>tim</string>
         <string>mqq</string>
         <string>mqqapi</string>
         <string>mqqbrowser</string>
         <string>mttbrowser</string>
         <string>mqqOpensdkSSoLogin</string>
         <string>mqqopensdkapiV2</string>
         <string>mqqopensdkapiV4</string>
         <string>mqzone</string>
         <string>mqzoneopensdk</string>
         <string>mqzoneopensdkapi</string>
         <string>mqzoneopensdkapi19</string>
         <string>mqzoneopensdkapiV2</string>
         <string>mqqapiwallet</string>
         <string>mqqopensdkfriend</string>
         <string>mqqopensdkavatar</string>
         <string>mqqopensdkminiapp</string>
         <string>mqqopensdkdataline</string>
         <string>mqqgamebindinggroup</string>
         <string>mqqopensdkgrouptribeshare</string>
         <string>tencentapi.qq.reqContent</string>
         <string>tencentapi.qzone.reqContent</string>
         <string>mqqthirdappgroup</string>
         <string>mqqopensdklaunchminiapp</string>
    </array>
    ```

#### 一键登录
1. Podfile中的subspecs，添加**OneKeyLogin**
2. Info.plist中，添加QQ参数。在`OpenSDKConfig`下，添加**JAppKey**
    ```
    <key>OpenSDKConfig</key>
    <dict>
        <key>JAppKey</key>
        <string>极光参数AppKey</string>
    </dict>
    ```
    
#### Apple登录
1. Podfile中的subspecs，添加**AppleLogin**
2.  工程中配置`Sign in with Apple`，如下图
![avatar](/doc_static/signApple.png)


#### 支付配置:苹果支付ApplePay

1. Podfile中的subspecs，添加**ApplePay**
2. 将游戏的bundleID和支付商品信息（productID和对应的金额）告知OpenSDK服务器端相关人员，在后台配置完成后方可进行苹果支付


#### 支付配置:支付宝AliPay

1. Podfile中的subspecs，添加**AliPay**
2. 点击"Info"选项卡，在"URL Types"选项中，点击"+"，在"URL Schemes"中输入"xxxxxxx"，用于支付宝跳回游戏。(注意：scheme的值游戏方自定义，建议跟bundleId有一定关联，要做到和其他app不重复，否则可能会导致支付宝返回的结果无法正确跳回app)
3. Info.plist中，在OpenSDKConfig下，创建**AliPayScheme**，值就是"Url Types"中配置的用于支付宝跳转回游戏的URL Schemes的值
	```
	<key>AliPayScheme</key>
	<string>支付宝跳转回游戏的URL Schemes的值</string>
	```


#### 支付配置:微信支付WechatPay

1. Podfile中的subspecs，添加**WechatPay**
2. Info.plist中，在OpenSDKConfig下，创建**WcXyzId**，值是微信支付的参数appId
    ```
    <key>WcXyzId</key>
    <string>微信appId</string>
    ```


#### 支付配置:银联支付UnionPay

1. Podfile中的subspecs，添加**UnionPay**


#### 支付配置:爱贝支付AibeiPay

1. Podfile中的subspecs，添加**AibeiPay**
2. Info.plist中，在OpenSDKConfig下，创建**MyPayH5Config**，配置爱贝支付的参数
    ```
    <key>MyPayH5Config</key>
    <dict>
    <key>AppID</key>
    <string>爱贝AppId</string>
    <key>AppPrivateKey</key>
    <string>爱贝私钥PrivateKey</string>
    <key>SupportUserProduct</key>
    <false/>
    </dict>
    ```
3. 注意：SupportUserProduct 默认设置为NO即可


#### 支付配置:现在支付(H5微信支付)PayNow

1. Podfile中的subspecs，添加**PayNow**


#### 作者

- jasontujun
- wangxudong
